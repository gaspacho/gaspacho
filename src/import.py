# _*_ coding: UTF8 _*_
# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


from optparse import OptionParser
from glob import glob
from os.path import join
import sys

from gaspacho import commit_database, initialize_database, \
        rollback_database, load

try:
    import json
except:
    #for python 2.5
    import simplejson as json

def parse_cmdline():
    parser = OptionParser()

    parser.add_option("-f", "--file", dest="file",
               default=None, help="import a specified gaspacho's file")
    parser.add_option("-a", "--all", dest="all", action="store_true",
               default=False, help="all json files in import/ will be imported")
    return parser.parse_args()

options, args = parse_cmdline()
if options.file != None:
    if options.all != False:
        print "Error, don't use -a -f option together"
        sys.exit(1)
    files = [options.file]
else:
    if options.all == False:
        print "Error, use -f or -a option, see help for more informations"
        sys.exit(1)
    files = glob(join('import', '*', '*', '*.gasp'))

initialize_database(create=True)

for fname in files:
    print "Import file: %s"%fname
    fh = file(fname, 'r')
    config = json.load(fh, encoding='utf-8')
    fh.close()
    try:
        load(config)
    except Exception, err:
        #import traceback
        #print traceback.print_exc()
        rollback_database()
        print("  error: "+ unicode(err))
    commit_database()

# vim: ts=4 sw=4 expandtab
