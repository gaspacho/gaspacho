#!/usr/bin/env python

import sys

sys.stdout.write('json: ')
try:
    import json
    JSON=True
except:
    JSON=False

if JSON == False:
    try:
        import simplejson
        JSON=True
    except:
        JSON=False
        print 'missing'
if JSON == True:
    print 'OK'

sys.stdout.write('Twisted Core: ')
try:
    from twisted.application import internet, service
    from twisted.internet import ssl #, reactor
    from twisted.python import components
    print 'OK'
except:
    print "missing"

sys.stdout.write('Twisted Web: ')
try:
    from twisted.web import server, resource, static, server
    from twisted.web.error import NoResource, Error
    print 'OK'
except:
    print "missing"

sys.stdout.write('Elixir: ')
try:
    from elixir import *
    print 'OK'
except:
    print 'missing'

sys.stdout.write('ConfigObj: ')
try:
    from configobj import ConfigObj
    print 'OK'
except:
    print 'missing'

sys.stdout.write('OptParse: ')
try:
    from optparse import OptionParser
    print 'OK'
except:
    print 'missing'

sys.stdout.write('pyPAM: ')
try:
    import PAM
    print 'OK'
except:
    print 'missing'

sys.stdout.write('formencode: ')
try:
    import formencode
    print 'OK'
except:
    print 'missing'
