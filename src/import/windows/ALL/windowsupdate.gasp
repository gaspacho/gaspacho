{
 "name": "windows/windows update", 
 "database": "0.0", 
 "version": "0.1", 
 "os": {
  "version": "ALL", 
  "name": "windows"
 }, 
 "categories": [
  {
   "name": [
    {
     "lang": "fr", 
     "label": "Windows"
    }
   ], 
   "tags": [
    {
     "computers": [
      {
       "variables": [
        {
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "AUOptions", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"
        }
       ], 
       "defaultvalue": "1", 
       "type": "enum", 
       "name": [
        {
         "lang": "fr", 
         "label": "Automatisation de l'installation des mises à jour"
        }
       ], 
       "options": [
        [
         "1", 
         "Désactiver les mises à jour"
        ], 
        [
         "2", 
         "Avertir en cas de nouvelles mises à jour sans les télécharger"
        ], 
        [
         "3", 
         "Télécharger automatiquement les mises à jour et prévenir"
        ], 
        [
         "4", 
         "Télécharger et installer automatiquement les mises à jour à l'heure définie (avec \"Jour d'installation\" et \"Heure d'installation\")"
        ], 
        [
         "5", 
         "La planification des mises à jour est configurée mais l'utilisateur final peut la configurer"
        ]
       ]
      }, 
      {
       "comment": "Règle activée = Considérer les MAJ mineures comme les autres MAJ<br />Règle desactivée = Installer silencieusement les MAJ mineures", 
       "variables": [
        {
         "value_on": "1", 
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "AutoInstallMinorUpdates", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Installation silencieuse des mises à jour mineures"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_on": "1", 
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "NoAutoRebootWithLoggedOnUsers", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Demander à l'utilisateur s'il veut redémarrer (NE PAS redémarrer brutalement)"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_on": "SUPPR", 
         "value_off": "1", 
         "type": "integer", 
         "name": "NoAutoUpdate", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Activer les mises à jour automatiques"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "ScheduledInstallDay", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"
        }
       ], 
       "defaultvalue": "1", 
       "type": "enum", 
       "name": [
        {
         "lang": "fr", 
         "label": "Jour d'installation des mises à jour automatiques"
        }
       ], 
       "options": [
        [
         "0", 
         "Tous les jours"
        ], 
        [
         "1", 
         "Dimanche"
        ], 
        [
         "2", 
         "Lundi"
        ], 
        [
         "3", 
         "Mardi"
        ], 
        [
         "4", 
         "Mercredi"
        ], 
        [
         "5", 
         "Jeudi"
        ], 
        [
         "6", 
         "Vendredi"
        ], 
        [
         "7", 
         "Samedi"
        ]
       ]
      }, 
      {
       "comment": "Heure comprise entre 0 < n < 23", 
       "name": [
        {
         "lang": "fr", 
         "label": "Heure d'installation des mises à jour automatiques"
        }
       ], 
       "variables": [
        {
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "ScheduledInstallTime", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"
        }
       ], 
       "options": {
        "max": 23, 
        "min": 0
       }, 
       "type": "integer", 
       "defaultvalue": "10"
      }, 
      {
       "comment": "Règle activée = Téléchargement depuis un serveur WSUS<br />Règle désactivée = Téléchargement depuis \"windowsupdate\"", 
       "variables": [
        {
         "value_on": "1", 
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "UseWUServer", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Activer le téléchargement des mises à jour depuis un serveur WSUS"
        }
       ]
      }
     ], 
     "name": [
      {
       "lang": "fr", 
       "label": "Configuration des mises à jour automatiques"
      }
     ], 
     "users": []
    }, 
    {
     "computers": [
      {
       "variables": [
        {
         "value_on": "1", 
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "AcceptTrustedPublisherCerts", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Le serveur WSUS distribue des mise à jour tierces"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_on": "1", 
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "ElevateNonAdmins", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Les utilisateurs NON-administrateurs peuvent paramétrer les MAJ automatiques (dangereux)"
        }
       ]
      }, 
      {
       "comment": "Ne fonctionne que si la règle \"Activer la gestion des groupes WSUS\" est activée.", 
       "variables": [
        {
         "value_off": "SUPPR", 
         "type": "unicode", 
         "name": "TargetGroup", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate"
        }
       ], 
       "defaultvalue": "TestServers", 
       "type": "unicode", 
       "name": [
        {
         "lang": "fr", 
         "label": "Nom du groupe dont la machine fait partie"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_on": "1", 
         "value_off": "SUPPR", 
         "type": "integer", 
         "name": "TargetGroupEnabled", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Activer la gestion des groupes WSUS"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_off": "SUPPR", 
         "type": "unicode", 
         "name": "WUServer", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate"
        }
       ], 
       "defaultvalue": "", 
       "type": "unicode", 
       "name": [
        {
         "lang": "fr", 
         "label": "URL HTTP(S) du serveur WSUS"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_off": "SUPPR", 
         "type": "unicode", 
         "name": "WUStatusServer", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate"
        }
       ], 
       "defaultvalue": "", 
       "type": "unicode", 
       "name": [
        {
         "lang": "fr", 
         "label": "URL HTTP(S) du serveur de status WSUS (doit être identique au serveur WSUS)"
        }
       ]
      }, 
      {
       "variables": [
        {
         "value_on": "SUPPR", 
         "value_off": "1", 
         "type": "integer", 
         "name": "DisableWindowsUpdateAccess", 
         "path": "reg://HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate"
        }
       ], 
       "type": "boolean", 
       "name": [
        {
         "lang": "fr", 
         "label": "Activer l'accès à Windows Update"
        }
       ]
      }
     ], 
     "name": [
      {
       "lang": "fr", 
       "label": "Configuration WSUS"
      }
     ], 
     "users": []
    }
   ]
  }
 ], 
 "software": {
  "version": "ALL", 
  "name": "windows update"
 }
}
