#! /bin/bash
### BEGIN INIT INFO
# Provides:          gaspacho
# Required-Start:    $local_fs $remote_fs
# Required-Stop:     $local_fs $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: gaspacho initscript
# Description:       stop/start Gaspacho
### END INIT INFO
#
# Author:	GnunuX <gnunux@gnunux.info>
#

set -e

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PYTHONPATH=/usr/share/
DESC="Gaspacho"
NAME=gaspacho
DAEMON=/usr/bin/twistd
DAEMON_ARGS="-noy /usr/local/share/gaspacho/website.tac"
PIDFILE=/var/run/gaspacho/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME
ERRORFILE=/var/log/gaspacho/error.log
DEFAULT="/etc/default/gaspacho"
DBFILE="/var/lib/gaspacho/gaspacho.sqlite"

export PYTHONPATH

. /lib/lsb/init-functions

[ -r $DEFAULT ] && . $DEFAULT

test -e $DBFILE || exit 0

# Gracefully exit if the package has been removed.
test -e $DAEMON || exit 0

if [ ! -d /var/run/gaspacho ];then
	mkdir -p /var/run/gaspacho
	chown nobody /var/run/gaspacho
fi

#
#	Function that starts the daemon/service.
#
d_start() {
	log_begin_msg "Starting $DESC: $NAME"
	if [ -f $PIDFILE ]
	then
		log_daemon_msg "$PIDFILE exists"
		log_end_msg 1
	else
		start-stop-daemon -b --start --exec $DAEMON -- $DAEMON_ARGS --pidfile $PIDFILE --logfile /var/log/gaspacho/$NAME.log 2>> $ERRORFILE
		log_end_msg $?
	fi
}

#
#	Function that stops the daemon/service.
#
d_stop() {
	log_begin_msg "Stopping $DESC: $NAME"
	if [ -f $PIDFILE ]
	then
		start-stop-daemon --stop --quiet -o --pidfile $PIDFILE
		sleep 1
	else
		log_failure_msg "$PIDFILE not found"
	fi
	if [ -f $PIDFILE ]
	then
		if ps h `cat $PIDFILE` > /dev/null
		then
			kill -9 `cat $PIDFILE`
		fi
		rm $PIDFILE
	fi

        # pour le restart
        log_end_msg 0
}

case "$1" in
  start)
	d_start
	;;
  stop)
	d_stop
	;;
  restart|force-reload)
	d_stop
	sleep 4
	d_start
	;;
  status)
	if ! [ -f $PIDFILE ]
	then echo "$DESC is stopped"
	     exit 3
	fi
	pid=`cat $PIDFILE`
	kill -0 $pid >/dev/null 2>&1
	if [ $? == 0 ]
	then echo "$DESC (pid $pid) is alive..."
	     exit 0
	fi
	echo "$DESC is stopped"
	exit 3
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload}" >&2
	exit 3
	;;
esac

exit 0
