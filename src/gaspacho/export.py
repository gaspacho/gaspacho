# Copyright (C) 2012-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gaspacho import get_categories, get_groups, get_choices
from gaspacho.config import (required_descr, required_os, required_category,
        required_names, required_tag, required_rule, optional_rule,
        required_variable, optional_variable, database_version, required_group,
        required_user, required_path, required_template, required_site,
        required_choice)
from gaspacho.platform import Platform

try:
    import json
except ImportError:
    #for python 2.5
    import simplejson as json

class Var:
    pass

def export_rules(filename):
    """
    Example of rule's exported gasp file:
    [{"name"      : "fedora-15/libreoffice-3.3",
     "version"   : "0.1",
     "database"  : "0.91",
     "os"        : {
      "name"      : "fedora",
      "version"   : "15"
     },
     "software"  : {
       "name"     : "libreoffice",
       "version"  : "3.3"
     },
     "packages"  : [{
        "name"    : "libreoffice"
     }, {
        "name"    : "libreoffice-i18n-fr",
        "lang"    :"fr"
     }],
     categories": [{
      "names"      : [{
           "lang"      : "fr",
           "label"     : "Reseau"
      }],
      "tags"      : [{
         "name"      : [{
          "lang"      : "fr",
          "label"     : "Configuration du proxy"
      }],
      "rules"     : [{
        "names"      : [{
         "lang"      : "fr",
         "label"     : "Activer le proxy"
        }],
        "type":     : 'boolean',
        "variables" : [{
           "value_on"  : "2",
           "value_off" : "SUPPR",
           "type"      : "integer",
           "name"      : "ooInetProxyType",
           "extension" : "xcu3",
           "info"      : "Inet/Settings",
           "path"      : "$HOME/.libreoffice/3/user/registrymodifications.xcu"
        }]
       }]
      }]
     }]
    }]"""
    exp_json = {}
    for category in get_categories():
        for tag in category.get_tags():
            for rule in tag.get_rules():
                for variable in rule.get_variables():
                    for import_ in variable.imports:
                        var = Var()
                        for key in required_variable:
                            try:
                                setattr(var, key, getattr(variable, key))
                            except AttributeError:
                                pass
                        for key in optional_variable:
                            try:
                                setattr(var, key, getattr(variable, key))
                            except AttributeError:
                                pass

                        level = variable.conflevel.name
                        if level.startswith('conf'):
                            level = level[4:]
                        var.level = level
                        platform = Platform.query.filter(Platform.imports.contains(import_),
                                        Platform.vars.contains(variable)).first()
                        var.extension = platform.path.extension
                        var.path = platform.path.name
                        var.info = platform.path.info
                        exp_json.setdefault(import_, {'categories': {},
                                'packages': platform.packages})['categories'
                                ].setdefault(category, {}).setdefault(tag, {}
                                ).setdefault(rule, []).append(var)
                        exp_json[import_]['os'] = {'name':
                                        platform.osversion.os.name,
                                        'version': platform.osversion.name}
                        exp_json[import_]['software'] = {'name':
                                        platform.softwareversion.software.name,
                                        'version': platform.softwareversion.name}

    ret = []
    for import_, categories in exp_json.items():
        imp = {'categories': []}
        imp['name'] = import_.name
        imp['version'] = import_.version
        imp['database'] = database_version
        imp['packages'] = categories['packages']
        imp['os'] = categories['os']
        imp['software'] = categories['software']
        for category, tags in categories['categories'].items():
            tcat = {'tags': [], 'names': []}
            for lang, label in category.name.items():
                tcat['names'].append({'lang': lang, 'label': label})
            for tag, rules in tags.items():
                ttag = {'rules': [], 'names': []}
                for lang, label in tag.name.items():
                    ttag['names'].append({'lang': lang, 'label': label})
                for rule, variables in rules.items():
                    trule = {'type': rule.type, 'variables': [], 'names': []}
                    for lang, label in rule.name.items():
                        trule['names'].append({'lang': lang, 'label': label})
                    for key in optional_rule:
                        try:
                            if key == 'defaultvalue':
                                value = rule.get_defaultvalue()
                            else:
                                value = getattr(rule, key)
                            if value == None or value == {}:
                                continue
                            trule[key] = value
                        except AttributeError:
                            pass
                    for variable in variables:
                        var = {}
                        for key in required_variable:
                            value = getattr(variable, key)
                            if value == None or value == {}:
                                continue
                            var[key] = value
                        for key in optional_variable:
                            try:
                                value = getattr(variable, key)
                                if value == None or value == {}:
                                    continue
                                var[key] = value
                            except AttributeError:
                                pass
                        trule['variables'].append(var)
                    ttag['rules'].append(trule)
                tcat['tags'].append(ttag)
            imp['categories'].append(tcat)
        ret.append(imp)
    fileh = file(filename, 'w')
    json.dump(ret, fileh, indent=1)
    fileh.close()

def export_groups(filename):
    def gen_group(group):
        print group.name
        tgroup = {'users': []}
        for key in required_group:
            tgroup[key] = getattr(group, key)
        tgroup['site'] = gen_site(group.site)
        for user in group.users:
            tgroup['users'].append(gen_user(user, group))
        return tgroup
    def gen_template(template):
        rtemplate = {'users': [], 'oses': [], 'softwares': [],
                    'choices': []}
        for user in template.users:
            rtemplate['users'].append(gen_user(user, template))
        for key in required_template:
            rtemplate[key] = getattr(template, key)
        for user in template.managers:
            rtemplate['managers'].append(gen_user(user, template))
        for os_ in template.oses:
            rtemplate['oses'].append(os_.name)
        for software in template.softs:
            rtemplate['softwares'].append(software.name)
        for choice in get_choices(template, heritage=False).values():
            rtemplate['choices'].append(gen_choice(choice))
        rtemplate['site'] = gen_site(template.site)
        return rtemplate
    def gen_user(user, group):
        ruser = {'choices': []}
        for key in required_user:
            ruser[key] = getattr(user, key)
        for choice in get_choices(group, user=user, heritage=False).values():
            ruser['choices'].append(gen_choice(choice))
        return ruser
    def gen_site(site):
        if site is None:
            return None
        rsite = {}
        for key in required_site:
            rsite[key] = getattr(site, key)
        return rsite
    def gen_path(path):
        rpath = {}
        for key in required_path:
            rpath[key] = getattr(path, key)
        return rpath
    def gen_choice(choice, user=None):
        rchoice = {}
        for key in required_choice:
            rchoice[key] = getattr(choice, key)
        rchoice['rule'] = {'rule': choice.rule.name,
                            'tag': choice.rule.tag.name,
                            'category': choice.rule.tag.category.name}
        platform = choice.platform
        if platform is not None:
            rchoice['platform'] = {'os': platform.osversion.os.name,
                    'osversion': platform.osversion.name,
                    'software': platform.softwareversion.software.name,
                    'softwareversion': platform.softwareversion.name,
                    'path': gen_path(platform.path)}
        return rchoice
    def get_children(grp):
        print "-"+grp.name
        rgroup = {'group': gen_group(grp), 'templates': [], 'groups': [],
                    'users': [], 'manager': [], 'oses': [],
                    'softwares': [], 'choices': []}
        for user in grp.users:
            rgroup['users'].append(gen_user(user, grp))
        for user in grp.managers:
            rgroup['managers'].append(gen_user(user, grp))
        for tmpl in grp.tmpls:
            rgroup['templates'].append(gen_template(tmpl))
        for os_ in grp.oses:
            rgroup['oses'].append(os_.name)
        for software in grp.softs:
            rgroup['softwares'].append(software.name)
        for choice in get_choices(grp, heritage=False).values():
            rgroup['choices'].append(gen_choice(choice))
        for group in grp.children:
            rgroup['groups'].append(get_children(group))
        return rgroup

    fileh = file(filename, 'w')
    groups = []
    for group in get_groups(None):
        groups.append(get_children(group))
    json.dump(groups, fileh, indent=1)
    fileh.close()

# vim: ts=4 sw=4 expandtab
