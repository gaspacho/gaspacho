# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gaspacho.group import Template, Group, User
from gaspacho.rule import Rule
from gaspacho.category import Category, Tag
from gaspacho.platform import Software, OS
from gaspacho.valid import valid
from gaspacho.log import trace #, logger

@trace
def ret_object(object_type):
    try:
        object_type = valid(object_type, 'unicode')
    except TypeError, e:
        raise Exception("Error in ret_object: ", str(e))
    if object_type == 'template':
        tobj = Template
    elif object_type == u'group':
        tobj = Group
    elif object_type == u'user':
        tobj = User
    elif object_type == u'category':
        tobj = Category
    elif object_type == u'software':
        tobj = Software
    elif object_type == u'rule':
        tobj = Rule
    elif object_type == u'os':
        tobj = OS
    elif object_type == u'tag':
        tobj = Tag
    else:
        raise TypeError('Unkown object type: %s' % object_type)
    return tobj

@trace
def is_object(obj, object_type):
    tobj = ret_object(object_type)
    try:
        valid(obj, tobj)
        return True
    except Exception:
        return False

@trace
def get_object_by_id(id, object_type):
    try:
        id = valid(id, 'integer')
    except TypeError, e:
        raise Exception("Error in get_object_by_id: ", str(e))
    tobj = ret_object(object_type)

    try:
        ret = tobj.query.filter_by(id=id).first()
    except Exception, e:
        raise Exception("Error in get_object_by_id: ", str(e))

    if ret == None:
        raise Exception("not a valid id")
    return ret

# vim: ts=4 sw=4 expandtab
