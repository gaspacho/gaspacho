class AlreadyInUseError(StandardError):
    pass

class AlreadyExistsError(StandardError):
    pass

class NoRelationError(StandardError):
    """
    if no relation between two objects
    """
    pass

class WrongObjError(StandardError):
    pass

class TranslationError(StandardError):
    pass

# vim: ts=4 sw=4 expandtab
