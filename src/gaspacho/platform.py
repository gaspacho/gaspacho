# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Create and manage OS, Software, OSVersion, SoftwareVersion, Path, Platform

OS: OS related with a Platform
Software: Software relatd with a Platform
OSVersion: One version of an OS
SoftwareVersion: One version of a SoftwareVersion
Path: The path file or tree of the Rule
Platform: Relation between OSVersion, SoftwareVersion and Path

                                  ,--- Group
Variable                          | ,- Template
   |                              | |
Platform -- SoftwareVersion -- Software
       '--- OSVersion -------- OS
"""
from elixir import (Entity, UnicodeText, Field, OneToMany, ManyToMany,
        ManyToOne, PickleType)
from gaspacho.valid import valid
from gaspacho.log import trace #, logger
from gaspacho.error import AlreadyExistsError

class OS(Entity):
    """Elixir class for OS

    :param name: common name for OS
    :param comment: description of the OS
    :param versions: relation with specific version
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    groups = ManyToMany('Group')
    tmpls = ManyToMany('Template')
    versions = OneToMany('OSVersion')
    #just for performance, don't touch this
    rules = ManyToMany('Rule')

#version
    @trace
    def add_version(self, name, comment=u''):
        try:
            name=valid(name, 'unicode')
            comment=valid(comment, 'unicode')
        except Exception, e:
            raise Exception('Error in add_version:' + str(e))
        if self.get_version(name) != None:
            raise AlreadyExistsError('OS Version with this name already exists')
        try:
            return OSVersion(name=name, comment=comment, os=self)
        except Exception, e:
            raise Exception('Error in add_version:' + str(e))

    @trace
    def get_version(self, name):
        try:
            name=valid(name, 'unicode')
            return OSVersion.query.filter_by(name=name, os=self).first()
        except Exception, e:
            raise Exception('Error in get_version:' + str(e))

    @trace
    def get_versions(self):
        try:
            return self.versions
        except Exception, e:
            raise Exception('Error in get_versions:' + str(e))

#name
    @trace
    def get_name(self):
        return self.name

#comment
    @trace
    def get_comment(self):
        return self.comment

    @trace
    def mod_comment(self, comment):
        try:
            comment = valid(comment, 'unicode')
            self.comment = comment
        except Exception, e:
            raise Exception("Error in mod_comment: " + str(e))

@trace
def add_os(name, comment=u''):
    try:
        name = valid(name, 'unicode')
        comment = valid(comment, 'unicode')
    except Exception, e:    
        raise Exception("Error in add_os: " + str(e))
    if get_os(name) != None:
        raise AlreadyExistsError('OS already exists')
    try:
        return OS(name=name, comment=comment)
    except Exception, e:    
        raise Exception("Error in add_os: " + str(e))

@trace
def get_oses():
    try:
        return OS.query.all()
    except Exception, e:
        raise Exception('Error in get_oses: ' + str(e))

@trace
def get_os(name):
    try:
        name = valid(name, 'unicode')
        return OS.query.filter_by(name=name).first()
    except Exception, e:
        raise Exception('Error in get_os: ' + str(e))

class Software(Entity):
    """Elixir class for Software

    :param name: common name for Software
    :param comment: description of the Software
    :param versions: relation with specific version
    :param groups/tmpls: relation with groups or templates
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    versions = OneToMany('SoftwareVersion')
    groups = ManyToMany('Group')
    tmpls = ManyToMany('Template')
    #just for performance, don't touch this
    rules = ManyToMany('Rule')

#software version    
    @trace
    def add_version(self, name, comment=u''):
        try:
            name=valid(name, 'unicode')
            comment=valid(comment, 'unicode')
        except Exception, e:
            raise Exception('Error in add_version:' + str(e))
        if self.get_version(name) != None:
            raise AlreadyExistsError('SoftwareVersion with this name already exists')
        try:
            return SoftwareVersion(name=name, comment=comment, software=self)
        except Exception, e:
            raise Exception('Error in add_version:' + str(e))

    @trace
    def get_version(self, name):
        try:
            name=valid(name, 'unicode')
            return SoftwareVersion.query.filter_by(name=name, software=self).first()
        except Exception, e:
            raise Exception('Error in get_version:' + str(e))

    @trace
    def get_versions(self):
        try:
            return self.versions
        except Exception, e:
            raise Exception('Error in get_versions:' + str(e))

#name
    @trace
    def get_name(self):
        return self.name

#comment
    @trace
    def get_comment(self):
        return self.comment

    @trace
    def mod_comment(self, comment):
        try:
            comment = valid(comment, 'unicode')
            self.comment = comment
        except Exception, e:
            raise Exception("Error in mod_comment: " + str(e))

@trace
def add_software(name, comment=u''):
    try:
        name = valid(name, 'unicode')
        comment = valid(comment, 'unicode')
    except Exception, e:    
        raise Exception("Error in add_software: " + str(e))
    if get_software(name) != None:
        raise AlreadyExistsError('Software already exists')
    try:
        return Software(name=name, comment=comment)
    except Exception, e:    
        raise Exception("Error in add_software: " + str(e))

@trace
def get_softwares():
    try:
        return Software.query.all()
    except Exception, e:
        raise Exception('Error in get_softwares: ' + str(e))

@trace
def get_software(name):
    try:
        name = valid(name, 'unicode')
        return Software.query.filter_by(name=name).first()
    except Exception, e:
        raise Exception('Error in get_software: ' + str(e))

class OSVersion(Entity):
    """Elixir class for OSVersion

    :param name: version of an OS
    :param comment: description of this OS version
    :param os: related OS
    :param platform: related Platform
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    os = ManyToOne('OS')
    plats = OneToMany('Platform')

#name
    @trace
    def get_name(self):
        return self.name

#comment
    @trace
    def get_comment(self):
        return self.comment

    @trace
    def mod_comment(self, comment):
        try:
            comment = valid(comment, 'unicode')
            self.comment = comment
        except Exception, e:
            raise Exception("Error in mod_comment: " + str(e))


class SoftwareVersion(Entity):
    """Elixir class for SoftwareVersion

    :param name: version of an Software
    :param comment: description of this Software version
    :param software: related Software
    :param platform: related Platform
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    software = ManyToOne('Software')
    plats = OneToMany('Platform')

#name
    @trace
    def get_name(self):
        return self.name

#comment
    @trace
    def get_comment(self):
        return self.comment

    @trace
    def mod_comment(self, comment):
        try:
            comment = valid(comment, 'unicode')
            self.comment = comment
        except Exception, e:
            raise Exception("Error in mod_comment: " + str(e))

class Path(Entity):
    """Elixir class for Path

    :param name: path name
    :param extension: extension (need associated plugin)
    :param info: information about the Path (option, section, ...)
    :param comment: description of the Path
    :param plats: related Platform
    """
    name = Field(UnicodeText)
    extension = Field(UnicodeText)
    info = Field(UnicodeText)
    comment = Field(UnicodeText)
    plats = OneToMany('Platform')

#name
    @trace
    def get_name(self):
        return self.name

    @trace
    def get_extension(self):
        return self.extension

    @trace
    def get_info(self):
        return self.info

#comment
    @trace
    def get_comment(self):
        return self.comment

    @trace
    def mod_comment(self, comment):
        try:
            comment = valid(comment, 'unicode')
            self.comment = comment
        except Exception, e:
            raise Exception("Error in mod_comment: " + str(e))

@trace
def add_path(name, extension, info=None, comment=u''):
    try:
        name = valid(name, 'unicode')
        extension = valid(extension, 'unicode')
        if info:
            info = valid(info, 'unicode')
        comment = valid(comment, 'unicode')
    except Exception, e:    
        raise Exception("Error in add_path: " + str(e))
    if get_path(name, extension, info):
        raise AlreadyExistsError('Path already exists')
    try:
        return Path(name=name, extension=extension, info=info, comment=comment)
    except Exception, e:    
        raise Exception("Error in add_path: " + str(e))

@trace
def get_path(name, extension, info=None):
    try:
        name = valid(name, 'unicode')
        extension = valid(extension, 'unicode')
        if info:
            info = valid(info, 'unicode')
        return Path.query.filter_by(name=name, extension=extension,
                        info=info).first()
    except Exception, e:
        raise Exception('Error in get_path: ' + str(e))

@trace
def get_paths():
    try:
        return Path.query.all()
    except Exception, e:
        raise Exception('Error in get_paths: ' + str(e))

class Platform(Entity):
    """Elixir class for Platform

    :param comment: description of the Platform
    :param osversion/softwareversion: related OS and Software version
    :param packages: name of package (for install software)
    :param path: related path
    :param vars: related variables
    :param choices: choice for a specific platform
    """
    comment = Field(UnicodeText)
    osversion = ManyToOne('OSVersion')
    packages = Field(PickleType)
    softwareversion = ManyToOne('SoftwareVersion')
    path = ManyToOne('Path')
    vars = ManyToMany('Variable')
    choices = OneToMany('Choice') 
    imports = ManyToMany('Import')

    @trace
    def get_softwareversion(self):
        return self.softwareversion

    @trace
    def get_osversion(self):
        return self.osversion

    @trace
    def get_path(self):
        return self.path

#comment
    @trace
    def get_comment(self):
        return self.comment

    @trace
    def mod_comment(self, comment):
        try:
            comment = valid(comment, 'unicode')
            self.comment = comment
        except Exception, e:
            raise Exception("Error in mod_comment: " + str(e))

#variable
    @trace
    def has_variable(self, name, conflevel, rule):
        try:
            name = valid(name, 'unicode')
            for var in self.vars:
                if name == var.name and conflevel == var.conflevel \
                        and rule == var.rule:
                    return True
            return False
        except Exception, e:
            raise Exception("Error in has_variable: "+ str(e))

@trace
def del_platform(platform):
    if platform.choices != []:
        raise AlreadyInUseError('platform has already choices')
    platform.softwareversion = None
    platform.osversion = None
    platform.path = None
    for variable in platform.vars:
        variable.platform = None
    platform.imports = []
    platform.delete()

@trace
def add_platform(path, osversion, softwareversion, packages, comment=u''):
    try:
        comment = valid(comment, 'unicode')
        valid(path, Path)
        valid(osversion, OSVersion)
        valid(softwareversion, SoftwareVersion)
        if comment == u'':
            comment = "%s %s - %s %s" % (osversion.os.name, osversion.name, softwareversion.software.name, softwareversion.name)
        if type(packages) != list:
            raise TypeError('Packages must be a list')
        for package in packages:
            if set(package) - set(['name', 'lang']):
                raise Exception('Unknown option in packages')
        if get_platform(path, osversion, softwareversion) != None:
            raise AlreadyExistsError('Platform already exists')
        return Platform(comment=comment, path=path, 
            osversion=osversion, softwareversion=softwareversion,
            packages=packages)
    except Exception, e:
        raise Exception("Error in add_platform: " + str(e))

@trace
def get_platform(path, osversion, softwareversion):
    try:
        valid(path, Path)
        valid(osversion, OSVersion)
        valid(softwareversion, SoftwareVersion)
        return Platform.query.filter_by(path=path, osversion=osversion, softwareversion=softwareversion).first()
    except Exception, e:
        raise Exception('Error in get_platform: ' + str(e))

@trace
def get_platforms():
    try:
        return Platform.query.all()
    except Exception, e:
        raise Exception('Error in get_platforms: ' + str(e))

# vim: ts=4 sw=4 expandtab
