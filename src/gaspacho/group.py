# Copyright (C) 2010-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Create and manage Group, Computer, Template, User

Group: relation between users and computers. Group could herited from an other
       Group
Computer: computers define by name or by ip
Template: same as Group by without herired and computers
User: user name or group of users

 ,--- Group ------- Computer
 |     | '------------,
 | ,- Template ------ User
 | |
Software
"""

from elixir import (Entity, Field, UnicodeText, Boolean, ManyToMany, ManyToOne,
                    OneToMany, Integer, PickleType)
from elixir.options import using_options
from copy import copy

from gaspacho.platform import Software, OS
from gaspacho.valid import valid
from gaspacho.database import commit_database
from gaspacho.config import multi_site, default_serv_lang
from gaspacho.log import trace #, logger
from gaspacho.error import AlreadyExistsError, AlreadyInUseError, \
        NoRelationError

class Site(Entity):
    """Elixir class for Site.

    * Site
    name: name of Site
    comment: description of the Site
    cn: common name (or id) of Site
    * Relation
    groups: list of groups in this Site
    templates: list of templates in this Site
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    cn = Field(UnicodeText)
    groups = OneToMany('Group')
    templates = OneToMany('Template')

@trace
def add_site(name, cn, comment=u''):
    """
    Add a new site
    """
    try:
        name = valid(name, 'unicode')
        cn = valid(cn, 'unicode')
        comment = valid(comment, 'unicode')
        if Site.query.filter_by(name=name).first() != None:
            raise AlreadyExistsError('site %s already exist' % name)
        if Site.query.filter_by(cn=cn).first() != None:
            raise AlreadyExistsError('site %s already exist' % name)
        return Site(name=name, cn=cn, comment=comment)
    except ValueError, err:
        raise ValueError("Error in add_site: " + unicode(err))

class Group(Entity):
    """Elixir class for Group

    :param name: group name
    :type name: `unicode`
    :param comment: group description
    :type comment: `unicode`
    :param level: to order Group, default is 999
    :type level: `integer`
    :param installpkg: need to install packages for this group
    :type installpkg: `boolean`
    :param lang: langage in this group (now just for pkg installation)
    :type lang: `unicode`
    :param computers: computers in relation with this Group
    :type computers: `list` of `Computer`
    :param users: users in relation with this Group
    :type users: `list` of `User`
    :param managers: people allow to manage Group (set Choices)
    :type managers: `list` of `User`
    :param parent: use for parent Group
    :type parent: `Group`
    :param children: use for children herited Group
    :type children: `list` of `Group`
    :param tmpls: use for herited templates
    :type tmpls: `list` of `Template`
    :param softs: Group only display Choice for this softwares
    :type softs: `list` of `Software`
    :param oses: Group only display Choice for this OS
    :type oses: `list` of `OS`
    :param site: Group in particular site
    :type site: `Site`
    :param choices: related choice
          Note: choice is a relation with group and user. There is only one
          choice available by relation group/user. There is several choices
          because there could have different relation with users.
    :type choices: `list` of `Choice`
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    level = Field(Integer, default=999)
    installpkg = Field(Boolean)
    lang = Field(UnicodeText)
    computers = Field(PickleType)
    users = ManyToMany('User', inverse='groups')
    managers = ManyToMany('User', inverse='gmanage')
    parent = ManyToOne('Group', inverse='children')
    children = OneToMany('Group')
    tmpls = ManyToMany('Template')
    softs = ManyToMany('Software')
    oses = ManyToMany('OS')
    site = ManyToOne('Site')
    choices = OneToMany('Choice')
    #for group by level
    using_options(order_by='level')

    #def __repr__(self):
    #    return "{'name': '%s', 'comment': '%s'}"  % (self.name, self.comment)

#name
    @trace
    def get_name(self):
        """Get group's name

        :return: name
        """
        return self.name

    @trace
    def mod_name(self, name):
        """Modify the group name
        """
        try:
            name = valid(name, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_name: " + unicode(err))
        if get_group(name=name, parent=self.parent):
            raise ValueError('Error in mod_name: group with name %s already'
                    ' exists' % name)
        self.name = name

#comment
    @trace
    def get_comment(self):
        """Get group's comment

        :return: comment
        """
        return self.comment

    @trace
    def mod_comment(self, comment):
        """Modify group's comment
        """
        try:
            comment = valid(comment, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_comment: " + unicode(err))
        self.comment = comment

#computer
    @trace
    def is_computer(self, name, type_):
        """Test if a computer already exist in a group

        :return: `boolean`
        """
        for tname, ttype in self.computers:
            if name == tname and ttype == type_:
                return True
        return False

    @trace
    def add_computer(self, name, type=u'ip'):
        """Add new computer
        """
        try:
            type = valid(type, 'computer')
            name = valid(name, 'unicode')
        except ValueError, err:
            raise ValueError("Error in add_computer: " + unicode(err))
        if self.is_computer(name, type):
            raise AlreadyExistsError('Computer already exists: %s' % name)
        tcomputers = copy(self.computers)
        tcomputers.append((name, type))
        self.computers = tcomputers

    @trace
    def del_computer(self, name, type=u'ip'):
        """Delete computer in a group
        """
        try:
            name = valid(name, 'unicode')
        except ValueError, err:
            raise ValueError("Error in del_computer: " + unicode(err))
        if not self.is_computer(name, type):
            raise Exception('Computer %s not already exists' % name)
        tcomputers = copy(self.computers)
        tcomputers.remove((name, type))
        self.computers = tcomputers

    @trace
    def get_computers(self):
        """
        Get all computers for a group
        """
        return self.computers

#user
    @trace
    def add_user(self, user):
        """
        Add relation to an user in a group
        """
        try:
            valid(user, User)
            if user in self.users:
                raise Exception('Relation between this group and this user '
                                'already exist')
            self.users.append(user)
        except ValueError, err:
            raise ValueError("Error in add_user: " + unicode(err))

    @trace
    def del_user(self, user, delete=True):
        """
        Delete relation between group and user with all related choices
        """
        try:
            valid(user, User)
            if user not in self.users:
                raise NoRelationError('No relation between this group and this user')
            if delete:
                #remove all choices for this group
                for choice in self.choices:
                    if choice.user == user:
                        choice.delete()
            self.users.remove(user)
        except ValueError, err:
            raise ValueError("Error in del_user: " + unicode(err))

    @trace
    def get_users(self):
        """
        Get all users of a group
        """
        return self.users

#manager
    @trace
    def add_manager(self, user):
        """
        Add a new manager in a group
        """
        try:
            valid(user, User)
            self.managers.append(user)
        except ValueError, err:
            raise ValueError("Error in add_manager: " + unicode(err))

    @trace
    def del_manager(self, user):
        """
        Delete relation between a group and a manager
        """
        try:
            valid(user, User)
            if user not in self.managers:
                raise NoRelationError('No relation between this group and this '
                                'manager')
            self.managers.remove(user)
        except ValueError, err:
            raise ValueError('Error in del_manager: ' + unicode(err))

    @trace
    def get_managers(self):
        """
        Get all managers in a group
        """
        return self.managers
#software
    @trace
    def add_software(self, software):
        """
        Add relation between software and a group
        """
        try:
            valid(software, Software)
            self.softs.append(software)
        except ValueError, err:
            raise ValueError("Error in add_software: " + unicode(err))

    @trace
    def del_software(self, software):
        """
        Delete relation between software and a group
        """
        try:
            valid(software, Software)
            if software not in self.softs:
                raise NoRelationError('No relation between this group and this '
                                'software')
            self.softs.remove(software)
        except ValueError, err:
            raise ValueError('Error in del_software: ' + unicode(err))

    @trace
    def del_softwares(self):
        """
        Delete all relations between software and a group
        """
        self.softs = []

    @trace
    def get_softwares(self):
        return self.softs

#os
    @trace
    def add_os(self, os):
        try:
            valid(os, OS)
            self.oses.append(os)
        except ValueError, err:
            raise ValueError("Error in add_os: " + unicode(err))

    @trace
    def del_os(self, os):
        try:
            valid(os, OS)
            if os not in self.oses:
                raise NoRelationError('No relation between this group and this OS')
            self.oses.remove(os)
        except ValueError, err:
            raise ValueError('Error in del_os: ' + unicode(err))

    @trace
    def del_oses(self):
        self.oses = []

    @trace
    def get_oses(self):
        return self.oses

#template
    @trace
    def add_template(self, template):
        try:
            valid(template, Template)
            self.tmpls.append(template)
        except ValueError, err:
            raise ValueError("Error in add_template: " + unicode(err))

    @trace
    def del_template(self, template):
        try:
            valid(template, Template)
            if template not in self.tmpls:
                raise NoRelationError('No relation between this group and this '
                                'template')
            self.tmpls.remove(template)
        except ValueError, err:
            raise ValueError("Error in del_template: " + unicode(err))

    @trace
    def get_templates(self):
        return self.tmpls

#installpkg
    @trace
    def get_installpkg(self):
        return self.installpkg

    @trace
    def mod_installpkg(self, installpkg):
        try:
            installpkg = valid(installpkg, 'boolean')
            self.installpkg = installpkg
        except ValueError, err:
            raise ValueError("Error in mod_installpkg: " + unicode(err))

@trace
def add_group(name, parent=None, installpkg=False, comment=u'', site=None):
    try:
        name = valid(name, 'unicode')
        comment = valid(comment, 'unicode')
        installpkg = valid(installpkg, 'boolean')
        if multi_site:
            if site == None:
                raise Exception('Need site in multi_site configuration')
        else:
            if site != None:
                raise Exception('Site must be None if multi_site is desactived')
        if get_group(name=name, parent=parent):
            raise AlreadyExistsError('group %s already exist' % name)
        #FIXME: lang only default_serv_lang
        if parent == None:
            ret = Group(name=name, comment=comment, computers=[],
                        installpkg=installpkg, lang=unicode(default_serv_lang))
        else:
            ret = Group(name=name, parent=parent, comment=comment,
                        computers=[], installpkg=installpkg,
                        lang=unicode(default_serv_lang))
    except ValueError, err:
        raise ValueError("Error in add_group: " + unicode(err))
    return ret

@trace
def del_group(group):
    """
    delete a group with all related choices, groups and users
    """
    try:
        valid(group, Group)
        #delete user in group but don't delete it's choices
        for user in group.users:
            group.del_user(user, delete=False)
        for child in group.children:
            del_group(child)
        #delete all choices
        for choice in group.choices:
            choice.delete()
        group.delete()

        #Need it.
        #Without it, some errors append with large heritage
        commit_database()
    except ValueError, err:
        raise ValueError('Error in del_group: ' + unicode(err))

@trace
def get_groups(parent=None):
    """
    get_groups
    parent: return children of this group
            None to return root groups
    """
    if parent == None:
        return Group.query.filter_by(parent=None).all()
    else:
        parent = valid(parent, Group)
        return parent.children

@trace
def get_group(name, parent=None):
    """
    get_group
    parent: this group is a child of this group
              None for a root group
    """
    name = valid(name, 'unicode')
    if parent:
        parent = valid(parent, Group)
    return Group.query.filter_by(name=name, parent=parent).first()

@trace
def get_all_groups(grp=None, cache=None):
    """Get a list of all groups
    """
    if cache is None:
        cache = {}
        for group in Group.query.all():
            cache.setdefault(group.parent, []).append(group)
    ret = []
    for group in cache.get(grp, {}):
        ret.append(group)
        ret.extend(get_all_groups(grp=group, cache=cache))
    return ret

class Template(Entity):
    """Elixir class for Template

    :param name: name of Template
    :type name: `unicode`
    :param comment: description of the Template
    :type comment: `unicode`
    :param managers: people allow to manage Template (set Choices)
    :type managers: `list` of `User`
    :param users: list of users in relation with this Template
    :type users: `list` of `User`
    :param softs: list of software in relation with this Template
    :type softs: `list` of `Software`
    :param oses: list of OS in relation with this Template
    :type oses: `list` of `OS`
    :param site: Template in particular site
    :type site: `Site`
    :param depends: some Groups could depend to Template
    :type depends: `list` of `Group`
    :param choices: related choice
        Note: choice is a relation with template and user. There is only one
              choice available by relation template/user. There is several
              choices because there could have different relation with users.
    :type choices: `list` of `Choice`
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    managers = ManyToMany('User', inverse='tmanage')
    users = ManyToMany('User', inverse='tmpls')
    softs = ManyToMany('Software')
    oses = ManyToMany('OS')
    site = ManyToOne('Site')
    depends = ManyToMany('Group')
    choices = OneToMany('Choice')

#name
    @trace
    def get_name(self):
        """Get current template name
        """
        return self.name

    @trace
    def mod_name(self, name):
        """Modify template name

        :param name: new template name
        :type name: `unicode`
        """
        try:
            name = valid(name, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_name: " + unicode(err))
        if get_template(name=name):
            raise AlreadyExistsError('Template with name %s already exists' % name)
        self.name = name

#comment
    @trace
    def get_comment(self):
        """Get current template comment
        """
        return self.comment

    @trace
    def mod_comment(self, comment):
        """Modify template comment

        :param comment: new template comment
        :type comment: `unicode`
        """
        try:
            comment = valid(comment, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_comment: " + unicode(err))
        self.comment = comment

#user
    @trace
    def add_user(self, user):
        """Add new relation with specified user

        :param user: new user
        :type user: `User`
        """
        try:
            valid(user, User)
        except ValueError, err:
            raise ValueError("Error in add_user: " + unicode(err))
        self.users.append(user)

    @trace
    def del_user(self, user, delete=True):
        """Delete related user

        :param user: relate user to delete
        :type user: `User`
        :param delete: delete choice too
        :type delete: `boolean`
        """
        try:
            valid(user, User)
        except ValueError, err:
            raise ValueError('Error in del_user: ' + unicode(err))
        if user not in self.users:
            raise NoRelationError('No relation between this group and this user')
        #remove all choices for this group
        if delete:
            for choice in self.choices:
                if choice.user == user:
                    choice.delete()
        self.users.remove(user)

    @trace
    def get_users(self):
        """Get all users of a template
        """
        return self.users

#software
    @trace
    def add_software(self, software):
        """Add new relation with software

        :param software: software to add
        :type software: `Software`
        """
        try:
            valid(software, Software)
        except ValueError, err:
            raise ValueError("Error in add_software: " + unicode(err))
        self.softs.append(software)

    @trace
    def del_software(self, software):
        """Delete relation with a specified software

        :param software: software to delete
        :type software: `Software`
        """
        try:
            valid(software, Software)
        except ValueError, err:
            raise ValueError('Error in del_software: ' + unicode(err))
        if software not in self.softs:
            raise NoRelationError('No relation between this group and this '
                            'software')
        self.softs.remove(software)

    @trace
    def del_softwares(self):
        """Delete all relation with softwares
        """
        self.softs = []

    @trace
    def get_softwares(self):
        """Get all softwares in relation with a specified template

        :return: `list` of `Software`
        """
        return self.softs
#os
    @trace
    def add_os(self, os):
        """Add new OS relation

        :param os: new OS
        :type os: `OS`
        """
        try:
            valid(os, OS)
        except ValueError, err:
            raise ValueError("Error in add_os: " + unicode(err))
        self.oses.append(os)

    @trace
    def del_os(self, os):
        """Delete OS

        :param os: relation to delete with this OS
        :type os: `OS`
        """
        try:
            valid(os, OS)
        except ValueError, err:
            raise ValueError('Error in del_os: ' + unicode(err))
        if os not in self.oses:
            raise NoRelationError('No relation between this template and this OS')
        self.oses.remove(os)

    @trace
    def del_oses(self):
        """Delete all OS in relation with this Template
        """
        self.oses = []

    @trace
    def get_oses(self):
        """Get all OS related

        :return: `list` of `OS`
        """
        return self.oses
#manager
    @trace
    def add_manager(self, user):
        """Add new manager

        :param user: user manager
        :type user: `User`
        """
        try:
            valid(user, User)
        except ValueError, err:
            raise ValueError("Error in add_manager: " + unicode(err))
        self.managers.append(user)

    @trace
    def del_manager(self, user):
        """Delete relation with template manager. That not delete user.

        :param user: user manager
        :type user: `User`
        """
        try:
            valid(user, User)
        except ValueError, err:
            raise ValueError('Error in del_manager: ' + unicode(err))
        if user not in self.managers:
            raise NoRelationError('No relation between this template and this '
                            'manager')
        self.managers.remove(user)

@trace
def add_template(name, comment=u''):
    """Add new template

    :param name: template name
    :type name: `unicode`
    :param comment: template comment
    :type comment: `unicode`
    :return: new `Template`
    """
    try:
        name = valid(name, 'unicode')
        comment = valid(comment, 'unicode')
    except ValueError, err:
        raise ValueError("Error in add_template: " + unicode(err))
    if get_template(name):
        raise AlreadyExistsError('Template %s already exists' % name)
    return Template(name=name, comment=comment)

@trace
def del_template(template):
    """Delete a template with all related choices

    :param template: template to delete
    :type template: `Template`
    """
    try:
        valid(template, Template)
    except ValueError, err:
        raise ValueError("Error in del_template: " + unicode(err))
    if template.depends != []:
        raise AlreadyInUseError('template already link to a group')
    #delete user in template but don't delete it's choices
    for user in template.users:
        template.del_user(user, delete=False)
    for user in template.managers:
        template.del_manager(user)
    #delete all choices
    for choice in template.choices:
        choice.delete()
    template.delete()

@trace
def get_templates(check_depend=False):
    """Get all templates

    :param check_depend: only template with depends
    :type check_depend: `boolean`
    :return: `list` of `Template`
    """
    if check_depend:
        return Template.query.filter(Template.depends.any()).all()
    return Template.query.all()

@trace
def get_template(name):
    """Get a template by name

    :param name: template name:
    :type name: `unicode`
    :return: `Template`
    """
    try:
        name = valid(name, 'unicode')
    except ValueError, err:
        raise ValueError("Error in get_template: " + unicode(err))
    return Template.query.filter_by(name=name).first()

class User(Entity):
    """Elixir class for User

    :param name: name of the User
    :type name: `unicode`
    :param type: type of the User (user or usergroup)
    :type type: `unicode`
    :param comment: description of User
    :type comment: `unicode`
    :param groups: related Groups
    :type groups: `list` of `Group`
    :param tmpls: related Templates
    :type tmpls: `list` of `Template`
    :param gmanage: User manage those Groups
    :type gmanage: `list` of `User`
    :param tmanage: User manage those Templates
    :type tmanage: `list` of Template`
    :param choices: related Choices
    :type choices: `list` of `Choice`
    """
    name = Field(UnicodeText)
    type = Field(UnicodeText)
    comment = Field(UnicodeText)
    groups = ManyToMany('Group')
    tmpls = ManyToMany('Template')
    gmanage = ManyToMany('Group')
    tmanage = ManyToMany('Template')
    choices = ManyToMany('Choice')

#name
    @trace
    def get_name(self):
        """Get user name
        """
        return self.name

    @trace
    def mod_name(self, name):
        """Modify user name

        :param name: new user name
        :type name: `unicode`
        """
        try:
            name = valid(name, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_name: " + unicode(err))
        if get_user(name=name, type=self.type) != None:
            raise AlreadyExistsError('User with name %s already exists' % name)
        self.name = name
#comment
    @trace
    def get_comment(self):
        """Get user comment
        """
        return self.comment

    @trace
    def mod_comment(self, comment):
        """Modify user comment

        :param comment: new comment
        :type comment: `unicode`
        """
        try:
            comment = valid(comment, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_comment: " + unicode(err))
        self.comment = comment

    #def __repr__(self):
    #    return '{name: "%s", type: "%s", comment: "%s"}' % (self.name,
    #            self.type, self.comment)

@trace
def add_user(name, type=u'user', comment=u''):
    """Add new user

    :param name: user name
    :type name: `unicode`
    :param type: user type
    :type name: `unicode`
    :param comment: user comment
    :type comment: `unicode`
    :return: new `User`
    """
    try:
        name = valid(name, 'unicode')
        type = valid(type, 'user')
        comment = valid(comment, 'unicode')
    except ValueError, err:
        raise ValueError("Error in add_user: " + unicode(err))
    if User.query.filter_by(name=name, type=type).first() != None:
        raise AlreadyExistsError('User already exists')
    return User(name=name, type=type, comment=comment)

@trace
def del_user(user):
    """Delete user

    :param user: user to delete
    :type user: `User`
    """
    try:
        valid(user, User)
    except ValueError, err:
        raise ValueError('Error in del_user:', unicode(err))
    if user.groups != []:
        raise AlreadyInUseError('User already used in a group')
    if user.gmanage != [] or user.tmanage != []:
        raise AlreadyInUseError('User is manager for a group or template')
    user.delete()

@trace
def get_users():
    """Get all users

    :return: `list` of `User`
    """
    return User.query.all()

@trace
def get_user(name, type=u'user'):
    """Get user

    :param name: user name
    :type name: `unicode`
    :param type: type group
    :type type: `unicode`
    :return: `User`
    """
    try:
        name = valid(name, 'unicode')
        type = valid(type, 'user')
    except ValueError, err:
        raise ValueError('Error in get_user: ' + unicode(err))
    return User.query.filter_by(name=name, type=type).first()

# vim: ts=4 sw=4 expandtab
