# Copyright (C) 2010-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""
Apply rules, this means generate file downloaded by agent
"""

from os.path import join, isdir, dirname
from os import makedirs
from shutil import rmtree
from copy import copy, deepcopy

from gaspacho import get_platforms
from gaspacho.choice import Choice
from gaspacho.platform import Path
from gaspacho.rule import get_rules, Variable, Rule
from gaspacho.group import get_all_groups, get_templates, Template, User
from gaspacho.config import store_dir_apply, default_separator
from gaspacho.conflevel import ConfLevel
from gaspacho.log import trace

try:
    import json
except ImportError:
    #for python 2.5
    import simplejson as json

global cache_db
cache_db = {}


class AllUser:
    """Fake User object to add u'all' type
    """
    type = u'all'
    name = u'all'


def gen_cache_db():
    global cache_db
    cache_db = {'conflevel': {}, 'name': {}, 'platform': {}, 'template': {},
                'choice': {}, 'path': {}, 'os': {}, 'software': {},
                'group': {}, 'user': {}, 'variable': {}, 'path_name': {},
                'rule': {}}

    paths = {}
    for path in Path.query.all():
        paths[path] = path
    for platform in get_platforms():
        cache_db['platform'][platform] = platform
        cache_db['path'][platform] = paths[platform.path]
    cache_db['group'] = get_all_groups()
    tgroups = copy(cache_db['group'])
    tgroups.extend(Template.query.all())
    groups = {None: None}
    for group in tgroups:
        groups[group] = group
    for choice in Choice.query.all():
        grp = groups[choice.group]
        if grp is None:
            grp = groups[choice.template]
        cache_db['choice'].setdefault((grp, choice.user), []
                                      ).append(choice)
    for conflevel in ConfLevel.query.all():
        cache_db['conflevel'][conflevel.name] = conflevel
    for rule in Rule.query.all():
        cache_db['rule'][rule] = rule
    for variable in Variable.query.all():
        cache_db['variable'].setdefault(cache_db['rule'][variable.rule],
                                        []).append(variable)
    cache_db['user'] = User.query.all()
    cache_db['user'].append(AllUser())


class Apply:
    @trace
    def __init__(self, group, gasp_cache=None, create_file=False):
        """
        gasp
           |- computer
           |    '- all
           |       '- os_version
           |              '- extension
           |                     '- path
           |                          '- variable
           |- user -- all
           |       |   |- os_version
           |       |   |      '- extension
           |       |   |             '- path
           |       |   |                  '- variable
           |       '- user1 ...
           '- context -- ...
        packages: [package1, package2, ...]
        path
           |- user -- all
           |       |   |- os_version = filename
           |       |   '- ...
           |       |- user1 ...
           |       '- ...
           |- context -- ...
           '- computer -- all - os_version = filemane
                                            '- ...

        """
        self.gasp = {u'user': {u'all': {u'all': {}}},
                     u'context': {u'all': {u'all': {}}},
                     u'computer': {u'all': {u'all': {}}}}
        self.create_file = create_file
        self.group = group
        if gasp_cache is None:
            gasp_cache = {}
        self.gen_cache(gasp_cache)
        self.platforms = self._get_apply_platforms()

    def gen_cache(self, cache):
        def _construct_cache():
            self.gasp[level].setdefault(usertype, {}).setdefault(
                user, {}).setdefault(platform, {}).setdefault(
                    extension, {}).setdefault(path, {}).setdefault(
                        info, {})[key] = variable
        #should be only for template
        if cache == {}:
            return
        #copy parent's gasp (if None copy load_defaults)
        self.gasp = deepcopy(cache['group'][self.group.parent])
        #add template configurations
        for template in self.group.tmpls:
            for level, usertypes in cache['template'][template].items():
                for usertype, users in usertypes.items():
                    if level == u'computer' and usertype != u'all':
                        continue
                    for user, platforms in users.items():
                        for platform, extensions in platforms.items():
                            for extension, paths in extensions.items():
                                for path, infos in paths.items():
                                    for info, keys in infos.items():
                                        for key, variable in keys.items():
                                            _construct_cache()

    @trace
    def get_gasp_value(self, variable, choice, rule):
        """
        Get value from choice or in variable
        """
        tvalue = choice.value
        tchoice = choice.state
        von = variable.value_on
        voff = variable.value_off
        rtype = rule.type
        if rtype == u'multi':
            mseparator = rule.options.get(u'separator', default_separator)
            tmvalue = tvalue.split(mseparator)
            i = 0
            mvalue = {}
            for val in rule.options[u'values']:
                mvalue[val[u'name']] = tmvalue[i]
                i += 1
        if tchoice == u'on':
            if von is None:
                value = tvalue
            else:
                if rtype == u'multi':
                    tvon = []
                    for val in von.split(mseparator):
                        tvon.append(mvalue.get(val, val))
                    tvon = unicode(mseparator.join(tvon))
                    value = tvon
                else:
                    value = von
        elif tchoice == u'off':
            value = voff
        elif tchoice == u'free':
            return None
        else:
            raise Exception('unknown choice: %s' % tchoice)
        return value

    @trace
    def get_gasp_packages(self, platform):
        """
        Get packages name in platform for a langage
        """
        packages = []
        lang = self.group.lang
        for package in platform.packages:
            #if lang is not set in package or only selected lang
            if package.get('lang', lang) == lang:
                if package['name'] not in packages:
                    packages.append(package['name'])
        return packages

    @trace
    def get_plugins_apply(self, user):
        """
        Get plugins values for a group, user, platform and level
        """
        global cache_db
        if user is None:
            usertype = u'all'
            username = u'all'
        else:
            usertype = user.type
            username = user.name
        tree_group = (self.group, user)
        for choice in cache_db['choice'].get(tree_group, {}):
            rule = cache_db['rule'][choice.rule]
            for variable in cache_db['variable'][rule]:
                value = self.get_gasp_value(variable, choice, rule)
                if variable.conflevel == cache_db['conflevel']['confcontext']:
                    level = u'context'
                elif variable.conflevel == cache_db['conflevel']['confuser']:
                    level = u'user'
                elif variable.conflevel == cache_db['conflevel']['confcomputer']:
                    level = u'computer'
                for platform in variable.plats:
                    platform = cache_db['platform'][platform]
                    name = build_choice_name(platform)
                    path = cache_db['path'][platform].name
                    extension = cache_db['path'][platform].extension
                    info = cache_db['path'][platform].info
                    key = variable.name
                    self.gasp[level].setdefault(usertype, {}).setdefault(
                        username, {}).setdefault(name, {}).setdefault(
                            extension, {}).setdefault(path, {}).setdefault(
                                info, {})
                    #if value == IGNORE, rule must not be set
                    if value in [None, u'IGNORE'] and \
                        key in self.gasp[level][usertype][username][name][
                            extension][path][info]:
                        del(self.gasp[level][usertype][username][name][
                            extension][path][info][key])
                    else:
                        self.gasp[level][usertype][username][name][extension][
                            path][info][key] = \
                            gen_variable(variable, info, value)

    @trace
    def build_choice_dir(self, platform):
        """
        Build choice dir name
        """
        if platform in cache_db['path_name']:
            path = cache_db['path_name'][platform]
        else:
            osversion = platform.get_osversion()
            osversionname = osversion.name
            osname = osversion.os.name
            path = join(osname, osversionname)
            cache_db['path_name'][platform] = path
        return path

    @trace
    def _get_apply_platforms(self):
        """
        Calculate platforms
        """
        platforms = cache_db['platform'].keys()
        if self.group.oses != []:
            for platform in platforms:
                if platform in cache_db['os']:
                    os = cache_db['os'][platform]
                else:
                    os = platform.osversion.os
                    cache_db['os'][platform] = os
                if os not in self.group.oses:
                    platforms.remove(platform)
        if self.group.softs != []:
            for platform in platforms:
                if platform in cache_db['software']:
                    software = cache_db['software'][platform]
                else:
                    software = platform.softwareversion.software
                    cache_db['software'][platform] = software
                if software not in self.group.softs:
                    platforms.remove(platform)
        return platforms

    @trace
    def get_gasp(self):
        """
        """
        def _construct_gasp():
            self.gasp[level].setdefault(user.type, {}).setdefault(
                user.name, {}).setdefault(platform, {}).setdefault(
                    extension, {}).setdefault(path, {}).setdefault(
                        info, {})[key] = variable
        self.get_plugins_apply(None)
        for user in self.group.users:
            for level in self.gasp.keys():
                if level == u'computer' and user is not None:
                    continue
                for platform, extensions in self.gasp[level][u'all'][
                        u'all'].items():
                    for extension, paths in extensions.items():
                        for path, infos in paths.items():
                            for info, keys in infos.items():
                                for key, variable in keys.items():
                                    _construct_gasp()
            self.get_plugins_apply(user)
        return self.gasp

    @trace
    def gen_output(self, extensions):
        output = {}
        for extension, paths in extensions.items():
            for path, infos in paths.items():
                for info, keys in infos.items():
                    for key, variable in keys.items():
                        output.setdefault(extension, {}).setdefault(
                            path, []).append(variable)
        return output

    @trace
    def write_files(self):
        """
        :return: return file's content (mostly for debugging)
        """
        output = {u'path': {}, u'gasp': {}}
        paths = {u'user': {u'all': {u'all': {}}},
                 u'context': {u'all': {u'all': {}}},
                 u'computer': {}}
        packages = {}
        for platform in self.platforms:
            name = build_choice_name(platform)
            base_dir = self.build_choice_dir(platform)
            paths[u'computer'][name] = join(base_dir, 'computer.gasp')
            for level in [u'user', u'context']:
                paths[level][u'all'][u'all'][name] = join(base_dir, level,
                                                          'ALL.gasp')
                for user in self.group.users:
                    filename = join(base_dir, level, user.type, '%s.gasp'
                                    % user.name)
                    paths[level].setdefault(user.type, {}).setdefault(
                        user.name, {})[name] = filename
            if hasattr(self.group, 'installpkg') and \
                    self.group.installpkg:
                packages[name] = self.get_gasp_packages(platform)
            else:
                packages[name] = []
        for level, usertypes in self.gasp.items():
            for usertype, users in usertypes.items():
                for user, platforms in users.items():
                    for platform, extensions in platforms.items():
                        try:
                            if level == u'computer':
                                filename = join(store_dir_apply,
                                                self.group.name,
                                                paths[level][platform])
                                paths[level][platform] = filename
                                out = {'plugins': self.gen_output(extensions),
                                       'packages': packages[platform]}
                            else:
                                out = self.gen_output(extensions)
                                filename = join(store_dir_apply,
                                                self.group.name,
                                                paths[level][usertype][user][
                                                    platform])
                                paths[level][usertype][user][platform] = filename
                        except KeyError:
                            continue
                        if self.create_file:
                            _dir = dirname(filename)
                            if not isdir(_dir):
                                makedirs(_dir)
                            fileh = file(filename, 'w')
                            json.dump(out, fileh, indent=1)
                            fileh.close()
                        else:
                            output['gasp'].setdefault(level, {}).setdefault(
                                usertype, {}).setdefault(user, {})[
                                    platform] = out
        output[u'path'] = paths
        return output


@trace
def build_choice_name(platform):
    """
    Build choice name
    """
    global cache_db
    if platform not in cache_db['name']:
        osversion = platform.get_osversion()
        cache_db['name'][platform] = "%s_%s" % (osversion.os.name,
                                     osversion.name)
    return cache_db['name'][platform]


@trace
def gen_variable(variable, info, value):
    tvariable = {'key': variable.name, 'type': variable.type}
    if value is not None:
        tvariable['value'] = value
    if info is not None:
        tvariable['info'] = info
    return tvariable


@trace
def get_templates_gasp():
    gasp = {}
    for template in get_templates(check_depend=True):
        gasp_apply = Apply(template)
        gasp[template] = gasp_apply.get_gasp()
    return gasp


@trace
def load_defaults():
    """
    Parse all rule with default state != 'free' and load default values
    """
    gcache = {u'group': {None: {u'user': {u'all': {u'all': {}}},
              u'context': {u'all': {u'all': {}}}, u'computer':
              {u'all': {u'all': {}}}}}, u'template': {}, u'ret': {}}
    for platform in cache_db['platform'].keys():
        name = build_choice_name(platform)
        gcache[u'group'][None][u'computer'][u'all'][u'all'][name] = {}
        for level in [u'user', u'context']:
            for user in cache_db['user']:
                gcache[u'group'][None][level].setdefault(
                    user.type, {}).setdefault(user.name, {})[name] = {}
    context = cache_db['conflevel']['confcontext']
    user = cache_db['conflevel']['confuser']
    computer = cache_db['conflevel']['confcomputer']
    #load default
    for rule in get_rules(defaultstate_not_free=True):
        value = rule.get_defaultvalue()
        for var in cache_db['variable'][rule]:
            if var.conflevel == context:
                level = u'context'
            elif var.conflevel == user:
                level = u'user'
            elif var.conflevel == computer:
                level = u'computer'
            for platform in var.plats:
                platform = cache_db['platform'][platform]
                name = build_choice_name(platform)
                path = cache_db['path'][platform].name
                extension = cache_db['path'][platform].extension
                info = cache_db['path'][platform].info
                key = var.name
                gcache[u'group'][None][level][u'all'][u'all'][name].setdefault(
                    extension, {}).setdefault(path, {}).setdefault(
                        info, {})[key] = gen_variable(var, info, value)
    return gcache


@trace
def apply_choices(create_file=True):
    """
    Apply choices
    create_file: use it only for debug or test
    """
    global cache_db
    gen_cache_db()
    ret = {}
    if create_file and isdir(store_dir_apply):
        rmtree(store_dir_apply)
    gcache = load_defaults()
    gcache['template'] = get_templates_gasp()
    for group in cache_db['group']:
        if group in gcache:
            raise Exception('group already in cache!')
        gasp_apply = Apply(group, gcache, create_file)
        gasp = gasp_apply.get_gasp()
        gcache['group'][group] = gasp
        computer = [(name, _type) for name, _type in group.get_computers()]
        #if no computer, don't generate group
        if computer == []:
            continue
        output = gasp_apply.write_files()
        gcache['ret'][group] = output['gasp']
        output[u'path'][u'group'] = computer
        ret[group.name] = output[u'path']
    if create_file:
        if not isdir(store_dir_apply):
            makedirs(store_dir_apply)
        filename = join(store_dir_apply, 'gaspacho.json')
        fileh = file(filename, 'w')
        json.dump(ret, fileh, indent=1)
        fileh.close()
    else:
        return (ret, gcache['ret'])

# vim: ts=4 sw=4 expandtab
