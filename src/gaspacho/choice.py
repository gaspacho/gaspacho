# Copyright (C) 2010-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Create and manage Choices.

A choice is response for a rule with specific group/template and, optionaly,
user.

                       ,-- Group
Rule ------- Choice ---'-- Template
                '--------- User

Example:

::

    from gaspacho import add_category, add_group
    category = add_category()
    category.add_translation('Network', 'en')
    tag = category.add_tag()
    tag.add_translation('Proxy', 'en')
    rule = tag.add_rule('unicode')
    rule.add_translation('Enable proxy', 'en')
    group = add_group('grp')
    choice = rule.set_choice(group, 'on', 'value')
    choice.set_value('new value')
    choice.set_state('off')
    rule.del_choice(group)
"""

from elixir import Entity, Field, UnicodeText, ManyToOne
from gaspacho.valid import valid, valid_value
from gaspacho.config import default_separator
from gaspacho.log import trace #, logger

class Choice(Entity):
    """Elixir class for Choice.

    A Choice is for a rule associated to a Group or template (and optionaly
    to a User). If rule needs value, value must not be None.

    Choice must not be associated to a Group and a Template together.

    :param state: should be "on", "off" or "free". For herited choice, just
           remove it.
    :type state: `unicode`
    :param value: response for the choice if rule need choice a state is 'on'
    :type value: `unicode`
    :param rule: associated Rule
    :type rule: `Rule`
    :param group: associated Group
    :type group: `Group`
    :param template: associated Template
    :type template: `Template`
    :param user: related object for the decision
    :type rule: `Rule`
    :param platform: if choice is only for a specific platform
    :type platform: `Platform`
    """
    state = Field(UnicodeText)
    value = Field(UnicodeText)
    rule = ManyToOne('Rule')
    group = ManyToOne('Group')
    template = ManyToOne('Template')
    user = ManyToOne('User')
    platform = ManyToOne('Platform')

#state
    @trace
    def set_state(self, state):
        """Set choice state (on, off or free)
        """
        try:
            state = valid(state, 'state')
        except TypeError, err:
            raise Exception("Error in set_state: ", str(err))
        try:
            self.state = state
        except Exception, err:
            raise Exception("Error in set_state: ", str(err))

    @trace
    def get_state(self):
        """Get choice state
        """
        return self.state

#value
    @trace
    def set_value(self, value):
        """Set choice value.

        Rule with type boolean could not have value (set value to None).
        If Rule is list or multi, value must be a list.
        If Rule is enum, value must be in unicode. This value must be in
        options.

        :param value: new value for choice
        """
        self.value = valid_value(value, self.rule.type, self.rule.options,
                self.rule._get_separator())

    @trace
    def get_value(self):
        """Get choice value
        """
        if self.rule.type in [u'list', u'multi']:
            return valid(self.value.split(self.rule._get_separator()),
                         self.rule.type,
                         self.rule.options)

        #valid to change type
        return valid(self.value, self.rule.type, self.rule.options)

# vim: ts=4 sw=4 expandtab
