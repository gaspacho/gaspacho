# Copyright (C) 2013 Team Gaspacho (see README fr all contributors)
# _*_ coding: UTF8 _*_
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gaspacho import (get_categories, get_choices, get_confuser,
                      get_confcontext, get_oses, get_softwares)
from tiramisu.option import OptionDescription, BoolOption, UnicodeOption, \
    IntOption, IPOption
from tiramisu.config import Config, MetaConfig
from tiramisu.setting import owners
from gaspacho.config import default_serv_lang
from gaspacho.util import get_object_by_id

global cache_optdesc
cache_optdesc = None

type_option_convert = {BoolOption: bool, IntOption: int}
type_option = {u'boolean': BoolOption, u'unicode': UnicodeOption,
               u'integer': IntOption, u'ip': IPOption,
               u'enum': UnicodeOption, u'list': UnicodeOption}


def gen_rule_id(rule):
    return 'r' + str(rule)


def gen_tag_id(tag):
    return 't' + str(tag)


def gen_category_id(category):
    return 'c' + str(category)


def gen_multi_id(rule, id_):
    if id_ > 10:
        id_ = '0' + id_
    elif id_ > 100:
        raise Exception('id_ must not be greater than 100')
    return 'm' + str(rule) + str(id_)


def gen_os_id(os_):
    return 'o' + str(os_)


def gen_soft_id(soft):
    return 's' + str(soft)


def convert_id(obj_id):
    return int(obj_id[1:])


def _gen_option_str(type_, id_, name, value, state, props, requires=None):
    option = type_option[type_](id_, name, value, properties=props,
                                requires=requires)
    option.impl_set_information('state', state)
    return option


def _gen_option(rule, user_level):
    name = rule.get_name(default_serv_lang)
    props = []
    if not user_level:
        props.append('computerlevel')
    for os_ in rule.oses:
        props.append(gen_os_id(os_.id))
    for soft in rule.softs:
        props.append(gen_soft_id(soft.id))
    if not rule.display:
        props.append('hidden')
    props = tuple(props)
    defaultvalue = rule.get_defaultvalue()
    defaultstate = rule.defaultstate
    if rule.type == u'multi':
        #add boolean to activate/desactivate
        opt = _gen_option_str(u'boolean', gen_rule_id(rule.id), name,
                              None, defaultstate, props)
        rules_multi = [opt]
        requires = ({'option': opt, 'expected': False, 'action': 'hidden'},)
        for num, value in enumerate(rule.options['values']):
            defaultval = defaultvalue[num]
            rules_multi.append(_gen_option_str(value['type'],
                                               gen_multi_id(rule.id, num),
                                               value['name'], defaultval,
                                               defaultstate, props, requires))
        option = OptionDescription(gen_rule_id(rule.id),
                                   rule.get_name(default_serv_lang),
                                   rules_multi)
    else:
        option = _gen_option_str(rule.type, gen_rule_id(rule.id), name,
                                 defaultvalue, defaultstate, props)

    comment = rule.get_comment(default_serv_lang)
    option.impl_set_information('help', comment)
    return option


def _gen_optdesc():
    categories = []
    confuser = get_confuser()
    confcontext = get_confcontext()
    for category in get_categories():
        tags = []
        for tag in category.get_tags():
            rules = []
            for rule in tag.get_rules():
                if confuser in rule.conflevels or \
                        confcontext in rule.conflevels:
                    user_level = True
                else:
                    user_level = False
                #comment = rule.get_comment(default_serv_lang)
                option = _gen_option(rule, user_level)
                rules.append(option)
            #tag.comment
            #FIXME family
            tags.append(OptionDescription(gen_tag_id(tag.id),
                                          tag.get_name(default_serv_lang),
                                          rules))
        #category.comment
        categories.append(OptionDescription(gen_category_id(category.id),
                          category.get_name(default_serv_lang), tags))

    return OptionDescription('rootconfig', "", categories)


def _disabled_oses(oses, config):
    for os_ in get_oses():
        if os_ not in oses:
            config.cfgimpl_get_settings().append(gen_os_id(os_))


def _disabled_softwares(softs, config):
    for soft in get_softwares():
        if soft not in softs:
            config.cfgimpl_get_settings().append(gen_soft_id(soft))


def load_config(grp, user):
    #FIXME multi : chargement de la valeur !
    global cache_optdesc
    if cache_optdesc is None:
        cache_optdesc = _gen_optdesc()
    if 'gaspacho' not in dir(owners):
        owners.addowner('gaspacho')
        owners.addowner('herited')
    config = Config(cache_optdesc)
    config.read_write()
    if user is not None:
        config.cfgimpl_get_settings().append('computerlevel')
    if grp.oses != []:
        _disabled_oses(grp.oses, config)
    if grp.softs != []:
        _disabled_softwares(grp.softs, config)
    config.impl_set_information('states', {})
    config.cfgimpl_get_settings().setowner(owners.gaspacho)
    choices = get_choices(grp, user)
    hconfig = MetaConfig([config])
    hconfig.cfgimpl_get_settings().setowner(owners.herited)
    for rule in choices.keys():
        tag = rule.get_tag()
        category = tag.get_category()
        #FIXME juste pour option ?
        path = '{0}.{1}'.format(gen_category_id(category.id), gen_tag_id(tag.id))
        subconfig = getattr(config, path)
        rule_id = gen_rule_id(rule.id)
        option = subconfig.unwrap_from_path(rule_id)

        choice, hchoice = choices[rule]
        if hchoice is not None:
            if hchoice.get_value() is not None:
                setattr(hconfig, path + '.' + rule_id, hchoice.get_value())
            hconfig.cfgimpl_get_settings()[option].append(hchoice.get_state())
        if choice is not None:
            if choice.get_value() is not None:
                setattr(subconfig, rule_id, str(choice.get_value()))
            config.cfgimpl_get_settings()[option].append(hchoice.get_state())
    hconfig.impl_set_information('grp', grp)
    hconfig.impl_set_information('user', user)
    return hconfig


def t_get_categories(hconfig):
    """Get category's names and, if category_name is set, all variables of
    current category_name.

    :param id_: session id
    :type id_: str
    returns: return a list of categories like:
                [{'name': 'cat1, 'help': 'help1', 'mode': 'basic'}, ...]
    """
    ret = []
    for grp, ownconfig in hconfig.iter_groups():
        option = ownconfig.cfgimpl_get_description()
        name = option.impl_getdoc()
        help_ = option.impl_get_information('help', '')
        category = {'id': convert_id(grp), 'name': name, 'help': help_}
        has_value = False
        for var in ownconfig.iter_all():
            path = '{0}.{1}'.format(grp, var[0])
            option = hconfig.unwrap_from_path(path)
            if isinstance(option, OptionDescription):
                for slave in getattr(hconfig, path):
                    has_value = True
                    break
                if has_value:
                    break
            else:
                has_value = True
                break
        if has_value:
            ret.append(category)
    return ret


def t_get_tags(hconfig, categoryid):
    data = []
    for grp, children in getattr(hconfig, gen_category_id(categoryid)).iter_groups():
        data.append({'id': convert_id(grp),
                     'name': hconfig.unwrap_from_path(gen_category_id(categoryid)
                                                      + '.' + grp
                                                      ).impl_getdoc()})
    return data


def _get_config(hconfig):
    #only one config so always at index 0
    return hconfig.cfgimpl_get_children()[0]


def _remove_state(config, option):
    for state in [u'free', u'on', u'off']:
        setting = config.cfgimpl_get_settings()
        if state in setting[option]:
            setting[option].remove(state)


def _get_state(config, option):
    for state in [u'free', u'on', u'off']:
        if state in config.cfgimpl_get_settings()[option]:
            return state
    return u'free'


def t_get_rules(hconfig, categoryid, tagid):
    data = []
    config = _get_config(hconfig)
    path = '{0}.{1}'.format(gen_category_id(categoryid), gen_tag_id(tagid))
    for rule, value in getattr(hconfig, path):
        opt_path = path + '.' + rule
        option = hconfig.unwrap_from_path(opt_path)
        data.append({'id': convert_id(rule), 'name': option.impl_getdoc(),
                     'hvalue': getattr(hconfig, opt_path),
                     'value': value,
                     'hstate': _get_state(hconfig, option),
                     'state': _get_state(config, option)})
    return data


def t_get_config(group, user=None):  # TODO: Rewrite using tiramisu syntax
    config = load_config(group, user)
    categories = []
    for category in t_get_categories(config):
        tags = []
        for tag in t_get_tags(config, category['id']):
            rules = []
            for rule in t_get_rules(config, category['id'], tag['id']):
                rules.append(rule)
            tags.append(dict(tag.items() + {'rules': rules}.items()))
        categories.append(dict(category.items() + {'tags': tags}.items()))
    return categories


def t_set_choice(hconfig, categoryid, tagid, ruleid, state, value):
    path = gen_category_id(categoryid) + '.' + gen_tag_id(tagid) + '.' + \
        gen_rule_id(ruleid)
    config = _get_config(hconfig)
    setattr(config, path, value)
    option = config.unwrap_from_path(path)
    _remove_state(config, option)
    config.cfgimpl_get_settings()[option].append(state)


def t_del_choice(hconfig, categoryid, tagid, ruleid):
    path = gen_category_id(categoryid) + '.' + gen_tag_id(tagid) + '.' + \
        gen_rule_id(ruleid)
    config = _get_config(hconfig)
    option = config.unwrap_from_path(path)
    _remove_state(config, option)
    delattr(config, path)


def _set_choices(rule, config, grp, user):
    tag = rule.get_tag()
    tagid = tag.id
    categoryid = tag.get_category().id

    path = gen_category_id(categoryid) + '.' + gen_tag_id(tagid) + '.' + \
        gen_rule_id(rule)
    option = config.unwrap_from_path(path)
    if config.getowner(path) != owners.gaspacho:
        rule.del_choice(grp, user)
    else:
        state = _get_state(config, option)
        value = getattr(config, path)
        rule.set_choice(grp, state, value, user)


def t_save_config(hconfig):
    grp = hconfig.impl_get_information('grp')
    user = hconfig.impl_get_information('user')
    choices = get_choices(grp, user)
    config = _get_config(hconfig)
    #FIXME voir si on peut faire mieux
    for option in config.cfgimpl_get_values()._values.keys():
        rule = get_object_by_id(convert_id(option._name), 'rule')
        if rule not in choices.keys():
            _set_choices(rule, config, grp, user)
    for rule in choices.keys():
        _set_choices(rule, config, grp, user)

# vim: ts=4 sw=4 expandtab
