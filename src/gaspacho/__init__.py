# Copyright (C) 2010-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

'''
Gaspacho
'''

from gaspacho.database import (initialize_database, commit_database,
                               rollback_database, close_database)
from gaspacho.category import get_category, get_categories
from gaspacho.platform import (get_platforms, get_platform, get_softwares,
                               get_software, get_oses, get_os, get_path)
from gaspacho.rule import get_choices, get_rules
from gaspacho.group import (add_group, del_group, get_group, get_groups,
                            get_all_groups, add_template, del_template,
                            get_templates, get_template, add_user, del_user,
                            get_users, get_user)
from gaspacho.conflevel import get_confuser, get_confcomputer, get_confcontext
from gaspacho.apply import apply_choices
from gaspacho.totiramisu import (load_config, t_get_config, t_get_categories, t_get_tags,
                                 t_get_rules, t_set_choice, t_del_choice,
                                 t_save_config)
from importfile import load

__version__ = '1.0'

__all__ = ['add_group', 'add_template', 'add_user', 'apply_choices',
           'close_database', 'commit_database', 'del_group', 'del_template',
           'del_user', 'get_all_groups', 'get_categories', 'get_category',
           'get_choices', 'get_confcomputer', 'get_confuser', 'get_confcontext',
           'get_group', 'get_groups', 'get_template', 'get_templates', 'get_os',
           'get_oses', 'get_platforms', 'get_software', 'get_softwares', 'get_rules',
           'get_user', 'get_users', 'initialize_database', 'load',
           'rollback_database', 'load_config', 't_get_config', 'get_path', 'get_platform',
           't_get_categories', 't_get_tags', 't_get_rules', 't_set_choice',
           't_del_choice', 't_save_config']
