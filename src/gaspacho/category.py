# Copyright (C) 2010-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Create and manage Category, Tag and ConfLevel

Each rule is store in a specified category and tag. Category and tag is only
use for rank rule. Category and tag are only label (translate in different
langage). There are not used when apply choice.

You should not create Category, tag or rule manually. Prefer write a Gasp file
with category/tag/rule description.

You should not create ConfLevel too. ConfLevels are integre in Gaspacho's
structure. You can't easily add new one. ConfLevel is create when get it.

Category: organise rule in subdivision
Tag: subcategories
ConfLevel: when variable are set (computer, user or context)

ConfLevel    Category
    |            |
    |           Tag
    |    Rule ---'

Examples:

Add new category/tag:

::

    from gaspacho import get_confuser, add_category
    conflevel = get_confuser()
    category = add_category()
    category.add_translation('Network', 'en')
    tag = category.add_tag()
    tag.add_translation('Proxy', 'en')

Get category/tag:

::

    from gaspacho import get_category
    category = get_category('Network', 'en')
    tag = category.get_tag('Proxy', 'en')
    rule = tag.get_rule('Enable proxy', 'en')

Delete category/tag:

::

    from gaspacho import get_categories, del_category
    from category in get_categories():
        from tag in category.get_tags():
            category.del_tag(tag)
        del_category(category)
"""

from elixir import (Entity, Field, UnicodeText, ManyToOne, OneToMany,
                    ManyToMany, PickleType)

from gaspacho.valid import valid
from gaspacho.rule import Rule, del_rule, TranslateObj
from gaspacho.log import trace
from gaspacho.error import AlreadyInUseError, AlreadyExistsError, \
    NoRelationError, TranslationError


class Category(Entity, TranslateObj):
    """Elixir class for Categories

    :param name: category translated name
    :type name: `pickle`
    :param comment: description of the category
    :type comment: `unicode`
    :param tags: each category have one or many tags
    :type tags: `list`
    """
    #names = Field(PickleType)
    name = Field(PickleType)
    comment = Field(UnicodeText)
    tags = OneToMany('Tag')

    @trace
    def _get_by_name(self, name, lang):
        """Get element from parent object to avoid name conflict

        :param name: category name
        :type name: `unicode`
        :param lang: langage name
        :type lang: `unicode`
        :return: Category with name/lang, None otherwise
        """
        return get_category(name=name, lang=lang)

#tag
    @trace
    def get_tag(self, name, lang):
        """Get tag for a category

        :param name: category name
        :type name: `unicode`
        :param lang: langage name
        :type lang: `unicode`
        :return: Tag if exists, None otherwise
        """
        try:
            name = valid(name, 'unicode')
            lang = valid(lang, 'unicode')
            #search all object set
            objs = Tag.query.filter_by(category=self).all()
            if objs is not None:
                for obj in objs:
                    #name is an object with lang as key
                    #lname = obj.names.get(lang, None)
                    lname = obj.name.get(lang, None)
                    #if label name is equal to name
                    if lname == name:
                        return obj
            return None
        except ValueError, err:
            raise ValueError("Error in get_tag: " + unicode(err))

    @trace
    def get_tags(self):
        """Get all tags for a selected category

        :return: list of all Tag
        """
        return Tag.query.filter_by(category=self).all()

    @trace
    def add_tag(self, comment=u''):
        """Use this function to add Tag object in database

        :param comment: may have comment
        :type comment: `unicode`
        :return: new tag
        """
        #if self.names == {}:
        if self.name == {}:
            raise TranslationError('Error in add_tag: need translation before add tag')
        comment = valid(comment, 'unicode')
        #return Tag(comment=comment, category=self, names={})
        return Tag(comment=comment, category=self, name={})

    @trace
    def del_tag(self, tag):
        """Delete a specified Tag

        You should not delete a tag use by rules. This method is safer way to delete tag.

        :param tag: tag to delete
        :type tag: `Tag`
        :raise: AlreadyInUseError
        """
        valid(tag, Tag)
        if tag.get_rules() != []:
            raise AlreadyInUseError('Error in del_tag: remove associated rules before')
        if tag.get_category() != self:
            raise NoRelationError('Error in del_tag: tag not associated with this category')
        tag.category = None
        tag.delete()

#comment
    @trace
    def get_comment(self):
        """Get category comment

        :return: comment
        """
        return self.comment

    @trace
    def mod_comment(self, comment):
        """Modify category comment
        """
        try:
            comment = valid(comment, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_comment: " + unicode(err))
        self.comment = comment


@trace
def add_category(comment=u''):
    """Use this function to add Category object in database.

    :param comment: may have comment
    :type comment: `unicode`
    :return: new category
    """
    try:
        comment = valid(comment, 'unicode')
    except ValueError, err:
        raise ValueError("Error in add_category: " + unicode(err))
    #return Category(comment=comment, names={})
    return Category(comment=comment, name={})


@trace
def get_category(name, lang):
    """Get Category by it's name for a specified lang.

    :param name: category name
    :type name: `unicode`
    :param lang: langage name
    :type lang: `unicode`
    :return: Category if exists, None otherwise
    """
    try:
        name = valid(name, 'unicode')
        lang = valid(lang, 'unicode')
    except ValueError, err:
        raise ValueError("Error in get_category: " + unicode(err))
    #search all object set
    objs = Category.query.all()
    if objs is not None:
        for obj in objs:
            #name is an object with lang as key
            #lname = obj.names.get(lang, None)
            lname = obj.name.get(lang, None)
            #if label name is equal to name
            if lname == name:
                return obj
    return None


@trace
def get_categories(grp=None, user_level=False, display=False):
    """Get all categories with (optionaly) grp or conflevel.

    :param grp: limit categories use in specified group (if oses or softwares
                associate to this group)
    :type grp: `Group`
    :param user_level: only categories with user and context level
    :type user_level: `unicode`
    :param display: only categorie with display status
    :type display: `boolean`
    :return: list of `Category`
    """
    from gaspacho import get_confuser, get_confcontext
    confuser = get_confuser()
    confcontext = get_confcontext()

    #if no group specified or group without software and os
    if grp is None or (grp.softs == [] and grp.oses == []):
        #get all categories
        categories = Category.query.all()
        if user_level is False:
            #all level
            if not display:
                return categories
            else:
                ret = []
                for category in categories:
                    for tag in category.tags:
                        if Rule.query.filter_by(tag=tag, display=True).all():
                            ret.append(category)
                            break
                return ret
        else:
            #else search if at least one Rule in confuser or confcontext
            ret = []
            for category in categories:
                is_break = False
                for tag in category.tags:
                    if display:
                        rules = Rule.query.filter_by(tag=tag,
                                                     display=True).all()
                    else:
                        rules = Rule.query.filter_by(tag=tag).all()
                    for rule in rules:
                        if confuser in rule.conflevels or \
                                confcontext in rule.conflevels:
                            ret.append(category)
                            is_break = True
                            break
                    if is_break:
                        is_break = False
                        break
            return ret
    else:
        #if group is specified and has softwares/oses
        retsoftwares = []
        retoses = []
        #search categories for those softwares/oses
        softwares = grp.softs
        oses = grp.oses
        #search categories for softwares
        if softwares != []:
            for software in softwares:
                for rule in software.rules:
                    category = rule.tag.category
                    if user_level is False or confuser in rule.conflevels or \
                            confcontext in rule.conflevels:
                        retsoftwares.append(category)
            #remove duplicated categories
            ret = set(retsoftwares)
        if oses != []:
            for os_ in oses:
                for rule in os_.rules:
                    category = rule.tag.category
                    if user_level is False or confuser in rule.conflevels or \
                            confcontext in rule.conflevels:
                        retoses.append(category)
            #remove duplicated oses
            ret = set(retoses)
        if softwares != [] and oses != []:
            #if softwares and oses specified, only return same categories
            ret = set(retsoftwares) & set(retoses)
        #return a list, not a set
        return list(ret)


@trace
def del_category(category):
    """Delete specified category

    This method is safer way to delete rule.

    :param category: category to delete
    :type category: `Category`
    :raise: AlreadyInUseError
    """
    valid(category, Category)
    if category.get_tags() != []:
        raise AlreadyInUseError('Remove associated tag before')
    try:
        category.delete()
    except Exception, err:
        raise Exception("Error in del_category: " + unicode(err))


class Tag(Entity, TranslateObj):
    """
    Elixir class for Tags

    name: name of the tag
    comment: description of the tag
    category: each tags have in one and only one category
    rules: each rules are dispached in tags
    """
    #names = Field(PickleType)
    name = Field(PickleType)
    comment = Field(UnicodeText)
    category = ManyToOne('Category')
    rules = OneToMany('Rule')

    @trace
    def _get_by_name(self, name, lang):
        """Get element from parent object to avoid name conflict

        :param name: category name
        :type name: `unicode`
        :param lang: langage name
        :type lang: `unicode`
        :return: Category with name/lang, None otherwise
        """
        return self.category.get_tag(name, lang)

#category
    @trace
    def get_category(self):
        """Get category associate to this tag.
        :return: `Category`
        """
        return self.category

#rule
    @trace
    def add_rule(self, type, defaultvalue=None, display=True,
                 defaultstate=u'free', options=None):
        """Use this function to add Rule object in database.

        :param type:
        :type type: `unicode`
        :param defaultvalue: default value
        :type defaultvalue: `unicode`
        :param display: display or not rule in interface
        :type display: `boolean`
        :param defaultstate: default state of the rule, default is free
        :type defaultstate: `unicode`
        :param options: options dictionary if needed
        :type options: `dict`
        :return: new rule
        """
        try:
            type = valid(type, 'type')
            display = valid(display, 'boolean')
            if options is None:
                options = {}

            if type == u'enum':
                if options == {}:
                    raise ValueError('if type is enum, options must not be None')
                options = valid(options, 'opt_enum')
            else:
                #defaultvalue must be None if boolean (value_on is apply)
                #defaultvalue should validate with the type but must be
                #unicode in database
                if type != u'boolean':
                    if type == u'integer':
                        ret = set(options.keys()) - set(['min', 'max'])
                        if list(ret) != []:
                            raise ValueError('Options must be min or max for '
                                             'integer')
                        for value in options.values():
                            valid(value, 'integer')
                    elif type == u'list':
                        ret = set(options.keys()) - set(['separator'])
                        if list(ret) != []:
                            raise ValueError('Options must be only separator '
                                             'for list')
                    elif type == u'unicode':
                        if options != {}:
                            raise ValueError('if type is unicode, options must '
                                             'be None')
                    elif type == u'multi':
                        keys_needed = ['values']
                        keys_optional = ['separator']
                        keys_optional.extend(keys_needed)
                        if list(set(keys_needed) -
                                set(options.keys())) != []:
                            raise ValueError('Multi must have attribute %s (%s)' %
                                             (', '.join(keys_needed),
                                              ', '.join(options.keys())))
                        if list(set(options.keys()) -
                                set(keys_optional)) != []:
                            raise ValueError('Multi can only have attributes %s (%s)' %
                                             (', '.join(keys_optional),
                                             ', '.join(options.keys())))
                else:
                    if options != {}:
                        raise ValueError('if type is boolean, options must be '
                                         'None')
                defaultstate = valid(defaultstate, 'unicode')
                if defaultstate not in [u'on', u'off', u'free']:
                    raise ValueError("defaultstate must be 'on', 'off' or "
                                     "'free'")
            rule = Rule(type=type, display=display, name={},
                        defaultstate=defaultstate, options=options, tag=self,
                        comments={})
            rule.set_defaultvalue(defaultvalue)
        except Exception, err:
            #import traceback
            #traceback.print_exc()
            raise Exception("Error in add_rule: " + unicode(err))
        return rule

    @trace
    def get_rules(self, softwares=[], oses=[], user_level=False, display=False):
        """Get all rules for a tag and optionnaly for a conflevel.

        :param display: if True filter by attribut display
        :type display: `boolean`
        :return: list of rules
        """

        from gaspacho import get_confuser, get_confcontext
        confuser = get_confuser()
        confcontext = get_confcontext()

        if display:
            rules = Rule.query.filter_by(tag=self, display=True).all()
        else:
            rules = Rule.query.filter_by(tag=self).all()
        if user_level:
            trules = []
            for rule in rules:
                if confuser in rule.conflevels or \
                        confcontext in rule.conflevels:
                    trules.append(rule)
            rules = trules

        if softwares == [] and oses == []:
            return rules

        #if softwares or oses specified
        trules = []
        for rule in rules:
            added = False
            if softwares != []:
                for software in rule.softs:
                    if software in softwares:
                        added = True
                        trules.append(rule)
                        break
            if added is False and oses != []:
                for os_ in rule.oses:
                    if os_ in oses:
                        trules.append(rule)
                        break
        return trules

    @trace
    def get_rule(self, name, lang):
        """Get a rule by name/lang.

        :param name: rule name
        :type name: `unicode`
        :param lang: langage name
        :type lang: `unicode`
        :return: return Rule, None otherwise
        """
        try:
            name = valid(name, 'unicode')
            lang = valid(lang, 'unicode')
        except ValueError, err:
            raise ValueError('Error in get_rule:', unicode(err))
        #search all label with rule set
        rules = Rule.query.filter_by(tag=self).all()
        if rules is not None:
            for rule in rules:
                #name is an object with lang as key
                #lname = rule.names.get(lang, None)
                lname = rule.name.get(lang, None)
                #if label name is equal to name
                if lname == name:
                    return rule
        return None

    @trace
    def del_rule(self, rule):
        """Del a rule for a Tag

        This method is safer way to delete rule.

        :param rule: rule to delete
        :type rule: `Rule`
        :raise: NoRelationError
        """
        if rule.get_tag() != self:
            raise NoRelationError('Rule not associated with this tag')
        del_rule(rule)

#comment
    @trace
    def get_comment(self):
        """Get comment for a Tag

        :return: comment
        """
        return self.comment

    @trace
    def mod_comment(self, comment):
        """Modify comment for a Tag

        :return: comment
        """
        try:
            comment = valid(comment, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_comment: " + unicode(err))
        self.comment = comment


class ConfLevel(Entity):
    """Elixir class for ConfLevel

    :param name: name of the conflevel, only confcomputer, confusers and confcontext
          are allowed
    :type name: `unicode`
    :param comment: description of the conflevel
    :type comment: `unicode`
    :param rules: list of Rule associated, don't touch this
    :type rules: list of `Rule`
    :param variables: list of Variable associated, don't touch this
    :type variables: list of `Variable`
    """
    name = Field(UnicodeText)
    comment = Field(UnicodeText)
    rules = ManyToMany('Rule')
    variables = OneToMany('Variable')

    @trace
    def get_name(self):
        """Get name of a ConfLevel

        :return: name
        """
        return self.name


@trace
def add_conflevel(name, comment=u''):
    """Use this function to add ConfLevel object in database

    :param name: conflevel name
    :type name: `unicode`
    :param comment: conflevel comment
    :type comment: `unicode`
    :raise: AlreadyExistsError
    :return: new ConfLevel
    """
    try:
        name = valid(name, 'unicode')
        comment = valid(comment, 'unicode')
    except ValueError, err:
        raise ValueError("Error in add_conflevel: " + unicode(err))
    #FIXME if conflevel already exists ?
    if ConfLevel.query.filter_by(name=name).first() is not None:
        raise AlreadyExistsError('The Conflevel %s already exists' % name)
    return ConfLevel(name=name, comment=comment)

# vim: ts=4 sw=4 expandtab
