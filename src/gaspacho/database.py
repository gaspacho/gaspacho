# Copyright (C) 2010-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from elixir import *
from sqlalchemy.exc import OperationalError

from gaspacho.config import database
from gaspacho.log import trace #, logger

#Initialize Elixir Database
@trace
def initialize_database(create=False, debug=False, database=database):
    metadata.bind = database
    if debug:
        metadata.bind.echo = True

    setup_all()

	#Create table if needed
    if create:
        try:
            create_all()
        except OperationalError, e:
            print "Unable to create database"
            raise Exception(str(e))
        except Exception, e:
            raise Exception(str(e))

#Commit and reload change in database
@trace
def commit_database():
    session.commit()
    session.flush()

@trace
def rollback_database():
    session.rollback()
#    session.clear()

@trace
def close_database():
    session.close()
#    cleanup_all()

# vim: ts=4 sw=4 expandtab
