# Copyright (C) 2009-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Validate

Common method to validate value, type, ...

Example:

    >>> from gaspacho.valid import normalize_unicode, valid
    >>> normalize_unicode('text')
    u'text'
    >>> normalize_unicode(u'text')
    u'text'
    >>> valid('text', 'unicode')
    u'texte'
    >>> valid(1, 'unicode')
    u'1'
    >>> valid(1, 'integer')
    1
    >>> valid(u'1', 'integer')
    1
    >>> try:
    ...    valid('text', 'integer')
    ... except:
    ...    print 'not valid'
    ...
    not valid
"""

from formencode import validators, Invalid
from gaspacho.log import trace
from gaspacho.config import default_separator

NORMALIZE_TYPE = (u'', u'SUPPR', u'SUPPRALL', u'IGNORE')


@trace
def normalize_unicode(value):
    """Test input for string or unicode.
    If string convert to unicode.

    :param value: value to test
    :type value: `unicode` or `str`
    :raise: `ValueError` if not unicode
    :return: `unicode`
    """
    if not type(value) == unicode:
        if type(value) == str:
            value = unicode(value, "utf-8")
        else:
            raise ValueError("unsupported encoding")
    return value


def valid_value(value, typ, options, separator):
    """Value validation

    :param value: value to test
    :param typ: type of the value
    :type typ: `unicode`
    :param options: if typ is integer, can specified min or max value
    :type options: `None` or `Dict`
    :param separator: separator's character for enum, list, ...
    :type separator: `unicode`
    :raise: ValueError
    :return: value, possibly convert
    """
    #no value only if type is boolean
    if typ == 'boolean':
        if value is not None:
            raise ValueError("Must no value for this rule's type %s" % typ)
    else:

        if value is None:
            raise ValueError("Need value for this rule's type %s" % typ)
        #validate value
        valid(value, typ, options)
        #for list or multi, join
        if typ == u'list':
            value = separator.join(value)
        if typ == u'multi':
            value = separator.join([str(val) for val in value])
        #value is in unicode type in database
        value = valid(value, 'unicode')
    return value


def valid(value, typ, options=None):
    """Validation

    :param value: value to test
    :param typ: type of the value
    :type typ: `unicode`
    :param options: if typ is integer, can specified min or max value
    :type options: `None` or `Dict`
    :raise: ValueError
    :return: value, possibly convert
    """
    if typ == 'list' or typ == 'multi':
        if type(value) == list:
            separator = options.get('separator', default_separator)
            if typ == u'multi':
                values = []
            for index, val in enumerate(value):
                if typ == u'list' and type(val) not in [str, unicode]:
                    raise ValueError('Value in list must be a string')
                if typ == u'multi':
                    if len(options['values']) != len(value):
                        raise ValueError('Value for multi must have same len has '
                                         'option')
                    values.append(valid(val, options['values'][index]['type'],
                                        options['values'][index].get('options', None)))
                try:
                    if separator in val:
                        raise ValueError('Separator must not be in defaultvalue')
                except TypeError:
                    #if not iterable
                    pass
            if typ == u'multi':
                value = values
            return value
        else:
            raise ValueError('%s should be a list' % typ)
    if typ in ['unicode', 'type', 'state', 'user', 'computer', 'vtype']:
        validator = validators.String()
    elif typ == 'boolean':
        validator = validators.StringBoolean()
    elif typ == 'integer':
        if options is not None:
            min_ = options.get('min', None)
            if min_ is not None:
                min_ = int(min_)
            max_ = options.get('max', None)
            if max_ is not None:
                max_ = int(max_)
        else:
            min_ = None
            max_ = None
        validator = validators.Int(min=min_, max=max_, not_empty=True)
    #FIXME
    elif typ == 'ip':
        validator = validators.IPAddress()
    elif typ == 'opt_enum':
        if type(value) == list:
            try:
                for val in value:
                    assert len(val) == 2
            except:
                raise ValueError('Options must be list of list for opt_enum')
            return value
        else:
            raise ValueError('opt_enum should be a list')
    elif typ == 'enum':
        #valid if value is in options
        for option in options:
            if value == option[0]:
                return value
        raise ValueError('Value must be in options')
    else:
        if type(value) == typ:
            return value
        else:
            raise ValueError('Unknown type (%s) for %s' % (typ, value))
    try:
        val = validator.to_python(value)
    except Invalid, err:
        raise ValueError(err)
    # If string or ip, convert to unicode
    if typ in ['unicode', 'ip', 'type', 'user', 'computer', 'state', 'vtype']:
        if val is None:
            val = ''
        val = normalize_unicode(val)
    if typ == 'type':
        if val not in [u'boolean', u'unicode', u'integer', u'ip', u'enum',
                       u'multi', u'list']:
            raise ValueError('%s is an unsupported type' % str(val))
    if typ == 'vtype':
        if val not in [u'boolean', u'unicode', u'integer', u'ip', u'enum']:
            raise ValueError('%s is an unsupported type' % str(val))
    if typ == 'state':
        if val not in [u'off', u'on', u'free']:
            raise ValueError('%s is an unsupported state' % str(val))
    if typ == 'computer':
        if val not in [u'ip', u'dns']:
            raise ValueError('%s is an unsupported computer type' % str(val))
    if typ == 'user':
        if val not in [u'user', u'usergroup']:
            raise ValueError('%s is an unsupported user type' % str(val))
    return val

# vim: ts=4 sw=4 expandtab
