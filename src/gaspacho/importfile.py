# Copyright (C) 2010-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from elixir import Entity, Field, UnicodeText, ManyToMany
from copy import copy

from gaspacho import add_group, get_template, add_template, get_user, add_user
from gaspacho.category import get_category, add_category, del_category
from gaspacho.platform import (add_platform, add_path, add_os, add_software,
        get_os, get_software, get_platform, get_path, del_platform)
from gaspacho.config import (database_version, required_descr, required_os,
        required_category, required_names, required_tag, required_rule,
        optional_rule, required_variable, optional_variable)
from gaspacho import get_confuser, get_confcomputer, get_confcontext
from gaspacho.log import trace #, logger
from gaspacho.error import AlreadyInUseError, AlreadyExistsError
from gaspacho.rule import del_variable, del_rule
from gaspacho.platform import Platform

class Import(Entity):
    """
    Elixir class for Import
    This table all information about importation file (name, version, ...)
    """
    name = Field(UnicodeText)
    version = Field(UnicodeText)
    variables = ManyToMany('Variable')
    platforms = ManyToMany('Platform')

    @trace
    def add_variable(self, variable, platform):
        self.variables.append(variable)
        self.platforms.append(platform)

    @trace
    def get_rules(self):
        rules = []
        for variable in self.variables:
            rules.append(variable.rule)
        return set(rules)

    @trace
    def del_platforms(self):
        for len_plat in range(0, len(self.platforms)):
            platform = self.platforms[0]
            remove_plat = True
            for variable in platform.vars:
                if variable not in self.variables:
                    remove_plat = False
                    continue
            if remove_plat:
                del_platform(platform)

    @trace
    def del_variables(self):
        for len_var in range(0, len(self.variables)):
            variable = self.variables[0]
            platform = Platform.query.filter(Platform.imports.contains(self),
                                        Platform.vars.contains(variable)).first()
            platform.vars.remove(variable)
            self.variables.pop(0)
            try:
                del_variable(variable)
            except AlreadyInUseError, err:
                pass

@trace
def load(datas):
    desc = GaspDescr(datas)
    desc.process()

class GaspDescr:
    @trace
    def __init__(self, datas):
        """{"name"      : "fedora-15/libreoffice-3.3",
         "version"   : "0.1",
         "database"  : "0.91",
         "os"        : {
          "name"      : "fedora",
          "version"   : "15"
         },
         "software"  : {
           "name"     : "libreoffice",
           "version"  : "3.3"
         },
         "packages"  : [{
            "name"    : "libreoffice"
         }, {
            "name"    : "libreoffice-i18n-fr",
            "lang"    :"fr"
         }],
         "categories" : ...
        """
        self.datas = datas
        self._validate()
        # setting the datas available by attributes
        for key, value in datas.items():
            if key == u'categories':
                continue
            if type(key) == unicode:
                setattr(self, key, value)
            elif type(key) == dict:
                # defining a sub object that stores the os and software datas
                # self.software = object()
                obj = setattr(self, key, object())
                # self.software.name = "fedora"
                for k, v in value:
                    setattr(getattr(self, obj), k, v)
        self.categories = []
        for category in self.datas['categories']:
            self.categories.append(GaspCategory(category))
        
    @trace
    def _validate(self):
        #check gaspfile version
        database = self.datas.get('database', None)
        if database == None:
            raise Exception('no database version, this not a Gasp file')
        if self.datas['database'] != database_version:
            raise Exception('database version need to be %s not %s' % \
                            (database_version, self.datas['database']))

#FIXME: unused?
        keys_descr = set(required_descr)
        keys = set(required_os)
        try:
            type(self.datas) == dict
            assert 'categories' in self.datas
            set(self.datas.keys()) == keys
            type(self.datas['os']) == dict
            type(self.datas['software']) == dict
            set(self.datas['os']) == keys
            set(self.datas['software']) == keys
            type(self.datas['packages']) == dict
        except Exception, e:
             raise TypeError("incorrect data values in datas: %s" % e)

    @trace
    def _get_os_version(self):
        os = get_os(self.os['name'])
        if os == None:
            os = add_os(name=self.os['name'])

        osv = os.get_version(self.os['version'])
        if osv == None:
            osv = os.add_version(self.os['version'])

        return osv

    @trace
    def _get_software_version(self):
        software = get_software(self.software['name'])
        if software == None:
            software = add_software(name=self.software['name'])

        swv = software.get_version(self.software['version'])
        if swv == None:
            swv = software.add_version(self.software['version'])

        return swv

    @trace
    def process(self):
        #check if gaspfile already imported
        import_obj = Import.query.filter_by(name=self.name).first()
        if import_obj != None:
            if import_obj.version == self.version:
                raise AlreadyExistsError("This gasp's file is already imported")
            elif import_obj.version > self.version:
                raise AlreadyExistsError("A more recent version of this gasp's is already "
                        "imported")
            else:
                rules = import_obj.get_rules()
                #remove older platforms and variables
                import_obj.del_variables()
                import_obj.del_platforms()
                #import new variable
                self._process(import_obj)
                #remove unused rule, tag and category
                for rule in rules:
                    try:
                        tag = rule.tag
                        category = tag.category
                        del_rule(rule)
                        category.del_tag(tag)
                        del_category(category)
                    except AlreadyInUseError:
                        pass
                import_obj.version = unicode(self.version)
        else:
            import_obj = Import(name=self.name, version=self.version)
            self._process(import_obj)


    def _process(self, import_obj):
        #get os, software et package of this import file's
        #only one os or software is allow per file
        osv = self._get_os_version()
        swv = self._get_software_version()
        packages = self.datas['packages']
        for category in self.categories:
            category.process(osv, swv, packages, import_obj)

class GaspCategory:
    @trace
    def __init__(self, datas):
        """"categories": [{
              "name"      : [{
                   "lang"      : "fr",
                   "label"     : "Reseau"
              }],
              "tags"      : ...
        """
        self.datas = datas
        self._validate()
        self.name = self.datas['names']
        #for name in name:
        #    for k, v in name.items():
        #        self.name.append((k, v))
        self.tags = []
        for tag in self.datas['tags']:
            self.tags.append(GaspTag(tag))
    
    @trace
    def _validate(self):
        try:
            type(self.datas) == dict, "datas not a dict"
            assert set(required_category) == set(self.datas.keys()), \
                    'must have attributes %s (%s)' % \
                        (', '.join(required_category),
                        ', '.join(self.datas.keys()))
            for name in self.datas['names']:
                type(name) == dict, 'name must be a dict'
                assert set(required_names) == set(name.keys()), \
                        'names must have attributes %s (%s)' % \
                        (', '.join(required_names),
                    ', '.join(name.keys()))

        except Exception, e:
            raise TypeError("incorrect datas structure: %s" % e)

    @trace
    def process(self, osv, swv, packages, import_obj):
        #FIXME: lang par defaut!
        category = get_category(name=self.name[0][u'label'], lang=self.name[0][u'lang'])
        if category == None:
            category = add_category()
            for catname in self.name:
                category.add_translation(lang=catname[u'lang'], name=catname[u'label'])
        for tag in self.tags:
            tag.process(category, osv, swv, packages, import_obj)

class GaspTag:
    @trace
    def __init__(self, datas):
        """ "tags"      : [{
               "name"      : [{
                "lang"      : "fr",
                "label"     : "Configuration du proxy"
             }],              
             "rules" : ...
        """
        self.datas = datas
        self._validate()
        self.name = self.datas['names']
        self.rules = []
        for rule in self.datas['rules']:
            self.rules.append(GaspRule(rule))
    
    @trace
    def _validate(self):
        try:
            assert type(self.datas) == dict, 'datas must be a dict'
            assert set(required_tag) == set(self.datas.keys()), \
                        'must have attributes %s (%s)'% (', '.join(required_tag),
                        ', '.join(self.datas.keys()))
            assert type(self.datas['names']) == list, 'name must be a list'
            for name in self.datas['names']:
                assert type(name) == dict, 'name must be a list of dict'
                assert set(required_names) == set(name.keys()), \
                    'names must have attributes %s' % ', '.join(required_names)
        except Exception, e:
            raise TypeError("GaspTag: %s" % e)

    @trace
    def process(self, category, osv, swv, packages, import_obj):
        #FIXME: lang par defaut!
        tag = category.get_tag(name=self.name[0][u'label'], lang=self.name[0][u'lang'])
        if tag == None:
            tag = category.add_tag()
            for tagname in self.name:
                tag.add_translation(lang=tagname[u'lang'], name=tagname[u'label'])

        for rule in self.rules:
            rule.process(tag, osv, swv, packages, import_obj)

class GaspRule:
    @trace
    def __init__(self, datas):
        """ "rules"     : [{
              "name"      : [{
               "lang"      : "fr",
               "label"     : "Activer le proxy"
              }],
              "type":     : 'boolean',
              "variables" : ...
             }]
        """
        self.datas = datas
        self._validate()
        self.name = []
        self.defaultvalue = None
        self.defaultstate = u'free'
        self.options = {}
        self.display = True
        self.comments = {}
        for key, value in self.datas.items():
            if key == u'variables':
                continue
            if type(key) == unicode:
                setattr(self, key, value)
            elif type(key) == dict:
                # defining a sub object that stores the os and software datas
                # self.software = object()
                obj = setattr(self, key, object())
                # self.software.name = "fedora"
                for k, v in value:
                    setattr(getattr(self, obj), k, v)

        self.variables = []
        for variable in self.datas['variables']:
            self.variables.append(GaspVariable(variable))
    
    @trace
    def _validate(self):
        keys_optional = copy(optional_rule)
        keys_optional.extend(required_rule)
        try:
            assert type(self.datas) == dict, 'datas must be a dict (%s)' % \
                        type(self.datas)
            assert set(required_rule) - set(self.datas.keys()) == set([]), \
                        'must have attributes %s (%s)' % (
                        ', '.join(required_rule), ', '.join(self.datas.keys()))
            assert set(self.datas.keys()) - set(keys_optional) == set([]), \
                        'allowed attributes %s (%s)' % (
                        ', '.join(keys_optional), ', '.join(self.datas.keys()))
            for name in self.datas['names']:
                type(name) == dict
                assert set(required_names) == set(name.keys())
        except Exception, e:
            raise TypeError("GaspRule: %s" % e)

    @trace
    def process(self, tag, osv, swv, packages, import_obj):
        #FIXME: should be default
        rule = tag.get_rule(name=self.names[0][u'label'],
                            lang=self.names[0][u'lang'])
        if rule == None:
            rule = tag.add_rule(type=self.type, defaultvalue=self.defaultvalue,
                    defaultstate=self.defaultstate, display=self.display,
                    options=self.options)
            for rulename in self.names:
                rule.add_translation(name=rulename[u'label'],
                            lang=rulename[u'lang'])
            for rulecomment in self.comments:
                rule.add_comment(name=rulecomment[u'label'],
                            lang=rulecomment[u'lang'])
        else:
            if rule.options != self.options or \
                    rule.get_defaultvalue() != self.defaultvalue or \
                    rule.type != self.type:
                raise Exception('Found an old rule but options, type or defaultvalue are different\nfor rule %s'
                                    % rule.name)

        for variable in self.variables:
            variable.process(rule, osv, swv, packages, import_obj)

class GaspVariable:
    @trace
    def __init__(self, datas):
        """ :datas: "variables" : [{
               "value_on"  : "2",
               "value_off" : "SUPPR",
               "type"      : "integer",
               "name"      : "ooInetProxyType",
               "extension" : "xcu3",
               "info"      : "Inet/Settings",
               "path"      : "$HOME/.libreoffice/3/user/registrymodifications.xcu"
            }]
        """
        self.datas = datas
        self._validate()
        self.value_on = None
        self.info = None
        self.comment = ''
        for key, value in self.datas.items():
            if key != 'level':
                setattr(self, key, value)
        if self.datas['level'] == 'user':
            self.conflevel = get_confuser()
        elif self.datas['level'] == 'context':
            self.conflevel = get_confcontext()
        else:
            self.conflevel = get_confcomputer()

    @trace
    def _validate(self):
        keys_level = ['user', 'computer', 'context']
        keys_optional = copy(optional_variable)
        keys_optional.extend(required_variable)
        try:
            assert type(self.datas) == dict, 'datas must be a dict'
            assert set(required_variable) - set(self.datas.keys()) == set([]), \
                        'must have attributes %s (%s)' % (
                        ', '.join(required_variable), ', '.join(self.datas.keys()))
            assert set(self.datas.keys()) - set(keys_optional) == set([]), \
                        'allowed attributes %s (%s)' % (
                        ', '.join(keys_optional), ', '.join(self.datas.keys()))
            assert self.datas['level'] in keys_level, \
                        'level must be %s (%s)' % (' or '.join(keys_level),
                        ', '.join(self.datas['level']))
        except Exception, e:
            name = self.datas.get('name', '')
            raise TypeError("GaspVariable: %s (%s)" % (e, name))

    @trace
    def process(self, rule, osv, swv, packages, import_obj):
        variable = rule.get_variable(name=self.name,
                conflevel=self.conflevel)
        if variable == None:
            variable = rule.add_variable(name=self.name,
                    conflevel=self.conflevel, type=self.type, 
                    value_on=self.value_on, 
                    value_off=self.value_off)
        else:
            if variable.type != self.type or \
                    variable.value_on != self.value_on or \
                    variable.value_off != self.value_off:
                raise Exception('Found an old variable but type, value_on or value_off are different\nfor variable %s'% self.name)

        path = get_path(name=self.path, info=self.info, extension=self.extension)
        if path == None:
            path = add_path(name=self.path, info=self.info, extension=self.extension,
                            comment=self.comment)
        platform = get_platform(osversion=osv, softwareversion=swv, path=path)
        if platform == None:
            platform = add_platform(osversion=osv, softwareversion=swv, path=path, packages=packages)
        else:
            if platform.packages != packages:
                raise Exception('Different package name: %s - %s' % (
                        platform.packages, packages))
        if platform not in variable.get_platforms():
            variable.add_platform(platform)
        import_obj.add_variable(variable, platform)

def load_groups(datas):
    def import_group(igroup, parent=None):
        #FIXME: no site support
        group = add_group(igroup['name'], parent=parent,
                    installpkg=igroup['installpkg'],
                    comment=igroup['comment'])
        group.level = igroup['level']
        group.lang = igroup['lang']
        for cname, ctype in igroup['computers']:
            group.add_computer(cname, ctype)
        for user in igroup['users']:
            import_user(group, user)
        return group

    def import_template(itemplate):
        #FIXME: no site support
        tmpl = get_template(itemplate['name'])
        if tmpl == None:
            tmpl = add_template(itemplate['name'], itemplate['comment'])
            for user in itemplate['users']:
                import_user(tmpl, user)
            for choice in itemplate['choices']:
                import_choice(None, tmpl, choice)
            for os_ in itemplate['oses']:
                tmpl.add_os(get_os(os_))
            for software in itemplate['softwares']:
                tmpl.add_software(get_software(software))
        return tmpl
        
    def import_user(group, iuser):
        user = get_user(iuser['name'], iuser['type'])
        if user == None:
            user = add_user(iuser['name'], iuser['type'], iuser['comment'])
        group.add_user(user)
        for choice in iuser['choices']:
            import_choice(user, group, choice)
        return user

    def import_choice(user, group, ichoice):
        lang = ichoice['rule']['category'].keys()[0]
        category = get_category(ichoice['rule']['category'][lang], lang)
        lang = ichoice['rule']['tag'].keys()[0]
        tag = category.get_tag(ichoice['rule']['tag'][lang], lang)
        lang = ichoice['rule']['rule'].keys()[0]
        rule = tag.get_rule(ichoice['rule']['rule'][lang], lang)
        #FIXME platform
        rule.set_choice(group, ichoice['state'], ichoice['value'], user)

    def import_groups(igroups, parent):
        parent = import_group(igroups['group'], parent)
        for itemplate in igroups['templates']:
            parent.add_template(import_template(itemplate))
        for igroup in igroups['groups']:
            import_groups(igroup, parent)
        for choice in igroups['choices']:
            import_choice(None, parent, choice)
    for data in datas:
        import_groups(data, None)

# vim: ts=4 sw=4 expandtab
