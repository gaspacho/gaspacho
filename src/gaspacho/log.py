# Copyright (C) 2012-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
from gaspacho.config import log_level, log_file

def trace(func):
    """This is a decorator which can be used to trace functions
    """
    def newFunc(*args, **kwargs):
        logger.debug("-> entering %s(%s, %s)"% (func.__name__, str(args), str(kwargs)))
#        print "-> entering %s(%s, %s)"% (func.__name__, str(args), str(kwargs))
        return func(*args, **kwargs)
    newFunc.__name__ = func.__name__
    newFunc.__doc__ = func.__doc__
    newFunc.__dict__.update(func.__dict__)
    return newFunc

def get_logger(loggername):
    """logging facilites
    """
    LEVELS = {'debug': logging.DEBUG,
              'info': logging.INFO,
              'warning': logging.WARNING,
              'error': logging.ERROR,
              'critical': logging.CRITICAL}

    if not log_level in LEVELS:
        print "%s is not a valid log_level"%log_level
        exit(1)
    format = logging.Formatter('%(asctime)s (%(process)d) %(levelname)s "%(funcName)s" %(message)s')
    gasp_logger = logging.getLogger(loggername)
    gasp_logger.setLevel(LEVELS[log_level])
    handler = logging.FileHandler(log_file)
    handler.setFormatter(format)
    gasp_logger.addHandler(handler)
    return gasp_logger # logging.getLogger(loggername)

logger = get_logger('gaspacho-agent')

# vim: ts=4 sw=4 expandtab
