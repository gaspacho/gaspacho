# Copyright (C) 2009-2013 Team Gaspacho (see README fr all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
ConfLevel is used by agent to know in with environment choice are apply.
ConfComputer is before the user log on
ConfUser is just after log on with administrator privilege
ConfContext is just avec ConfUser in the user's context
"""

from gaspacho.category import ConfLevel, add_conflevel
from gaspacho.database import commit_database
from gaspacho.log import trace #, logger

@trace
def get_confuser():
    """
    Get the user conflevel
    """
    need_commit = False
    confuser = ConfLevel.query.filter_by(name=u'confuser').first()
    if confuser == None:
        confuser = add_conflevel(name=u'confuser',
                                 comment=u"User's configuration")
        need_commit = True
    if need_commit:
        commit_database()
    return confuser

@trace
def get_confcomputer():
    """
    Get the computer conflevel
    """
    need_commit = False
    confcomputer = ConfLevel.query.filter_by(name=u'confcomputer').first()
    if confcomputer == None:
        confcomputer = add_conflevel(name=u'confcomputer',
                comment=u"Computer's configuration")
        need_commit = True
    if need_commit:
        commit_database()
    return confcomputer

@trace
def get_confcontext():
    """
    Get the context conflevel
    """
    need_commit = False
    conf = ConfLevel.query.filter_by(name=u'confcontext').first()
    if conf == None:
        conf = add_conflevel(name=u'confcontext',
                comment=u"User's context configuration")
        need_commit = True
    if need_commit:
        commit_database()
    return conf

# vim: ts=4 sw=4 expandtab
