# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Create and manage Rule and Variable

Rules are store in tag. Inside rule you should store variables. When users
set a choice state to 'on' or 'off', variables are set in application
configuration.

If rule needs value, this value is only used if choice state is 'on'.

Rule contains information display in user interface with default value.
Variable contains variable for speficied file, register, ...

Tag
  |
Rule
  |
Variable

Examples:

Add new rule and variable:

::

    from gaspacho import get_confuser, add_category
    confuser = get_confuser()
    category = add_category()
    category.add_translation('Network', 'en')
    tag = category.add_tag()
    tag.add_translation('Proxy', 'en')
    rule = tag.add_rule('boolean')
    rule.add_translation('Enable proxy', 'en')
    variable = rule.add_variable('enable.proxy', 'unicode', value_on='on',
                    value_off='off', conflevel=confuser)

Get rule and variable:

::

    from gaspacho import get_confuser, get_category
    confuser = get_confuser()
    category = get_category('Network', 'en')
    tag = category.get_tag('Proxy', 'en')
    rule = tag.get_rule('Enable proxy', 'en')
    variable = rule.get_variable('enable.proxy', conflevel)

Delete category/tag/rule:

::

    from gaspacho import get_categories, del_category
    from category in get_categories():
        from tag in category.get_tags():
            from rule in tag.get_rules():
                from variable in rule.get_variables()
                    rule.del_variable(variable)
                tag.del_rule(rule)
            category.del_tag(tag)
        del_category(category)

"""
from elixir import (Field, Boolean, UnicodeText, OneToMany, ManyToMany,
                    ManyToOne, PickleType, Entity)
from sqlalchemy import not_
from copy import copy

from gaspacho.valid import valid, valid_value, NORMALIZE_TYPE
from gaspacho.platform import Platform
from gaspacho.config import ordered_lang, default_separator
from gaspacho.choice import Choice
from gaspacho.group import User, Group, Template
from gaspacho.log import trace  # , logger
from gaspacho.error import AlreadyInUseError, AlreadyExistsError, \
    NoRelationError, WrongObjError


class TranslateObj():
    @trace
    def add_translation(self, name, lang):
        """Add a translation for a Tag.

        :param name: object's label for a specified langage
        :type name: `unicode`
        :param lang: langage name
        :type lang: `unicode`
        :raise: AlreadyExistsError
        """
        name = valid(name, 'unicode')
        lang = valid(lang, 'unicode')
        #if self.names.has_key(lang):
        if lang in self.name:
            raise AlreadyExistsError('This object has a translation for %s' % lang)
        if self._get_by_name(name, lang) is not None:
            raise AlreadyExistsError('Translation %s in %s already exists' %
                                     (name, lang))
        #tname = copy(self.names)
        tname = copy(self.name)
        tname[lang] = name
        #self.names = tname
        self.name = tname

    @trace
    def get_name(self, lang):
        """Get object name for a specified langage.

        :param lang: langage name
        :type lang: `unicode`
        :return: name for specified langage, None otherwise
        """
        try:
            lang = valid(lang, 'unicode')
        except ValueError, err:
            raise ValueError("Error in get_name: " + unicode(err))
        #if self.names.has_key(lang):
        #    return self.names[lang]
        if lang in self.name:
            return self.name[lang]
        for tlang in ordered_lang:
            #if self.names.has_key(tlang):
            #    return self.names[tlang]
            if tlang in self.name:
                return self.name[tlang]
        return None
        #return u'No translation in %s'%lang

    @trace
    def mod_translation(self, name, lang):
        """Modify a translation of a Tag

        :param name: object's label for a specified langage
        :type name: `unicode`
        :param lang: langage name
        :type lang: `unicode`
        :raise
        """
        try:
            name = valid(name, 'unicode')
            lang = valid(lang, 'unicode')
        except ValueError, err:
            raise ValueError('Error in mod_translation:', err)
        #if not self.names.has_key(lang):
        if lang not in self.name:
            raise ValueError('Error in mod_translation: this object has not a'
                             ' translation for %s' % lang)
        if self._get_by_name(name, lang) is not None:
            raise AlreadyExistsError('Error in mod_translation: the object %s '
                                     'in %s already exists' % (name, lang))
        #self.names[lang] = name
        self.name[lang] = name


class Rule(Entity, TranslateObj):
    """Elixir class for Rule

    :param name: label of the Rule
    :type name: `unicode`
    :param type: type of the name (see below)
    :type type: `unicode`
    :param defaultvalue: default value
    :type defaultvalue: `unicode`
    :param options: options for this rule (see below)
    :type options: `dict`
    :param comments: description of this Rule
    :type comments: `pickle`
    :param vars: related Variable
    :type vars: `list` of `Variable`
    :param tag: related Tag
    :type tag: `list` of `Tag`
    :param choices: related Choices
    :type choices: `list` of `Choice`
    :param display: if display is False, this rule is not display in interface
    :type display: `boolean`
    :param defaultstate: default state must be "on", "off" or "free" (by default)
    :type defaultstate: `unicode`

    Type:
    unicode: string
    boolean: boolean
    integer: integer
    ip: IP
    list: list of string
    enum: list of predeterminate choice
    multi: several field for a rule

    Options:
    enum: list of available choice list (ie: [['1', 'first], '2', 'second']])
    integer: specify min and max value(ie: {'min': 0, 'max': 23})
    list: specify the separator character (ie: {'separator': ';'}), default: -
    multi: description of all values, ie:
           {'values': [{'name': 'PROXY_HOST', 'type': 'unicode'},
                       {'name': 'PROXY_PORT', 'type': 'integer',
                        'options': {'min': 0, 'max': 65535}}],
           'separator': ':'}
    """
    #names = Field(PickleType)
    name = Field(PickleType)
    type = Field(UnicodeText)
    defaultvalue = Field(UnicodeText)
    defaultstate = Field(UnicodeText)
    display = Field(Boolean)
    options = Field(PickleType)
    comments = Field(PickleType)
    vars = OneToMany('Variable')
    tag = ManyToOne('Tag')
    choices = OneToMany('Choice')
    #just for performance, don't touch this
    conflevels = ManyToMany('ConfLevel')
    oses = ManyToMany('OS')
    softs = ManyToMany('Software')

    @trace
    def _get_by_name(self, name, lang):
        """Get element from parent object to avoid name conflict

        :param name: category name
        :type name: `unicode`
        :param lang: langage name
        :type lang: `unicode`
        :return: Rule with name/lang, None otherwise
        """
        return self.tag.get_rule(name, lang)

    @trace
    def _get_separator(self):
        """Get separator from option or default_separator

        :return: separator
        """
        try:
            return self.options.get('separator', default_separator)
        except:
            pass
        return None

#tag
    @trace
    def get_tag(self):
        """Get related tag

        :return: associated `Tag`
        """
        return self.tag

#defaultvalue
    @trace
    def get_defaultvalue(self):
        """Get the defaultvalue

        :return: defaultvalue
        """
        defaultvalue = self.defaultvalue
        if (self.type == u'list' or self.type == u'multi') \
                and defaultvalue is not None:
            defaultvalue = valid(defaultvalue.split(self._get_separator()),
                                 self.type, self.options)
        return defaultvalue

    @trace
    def set_defaultvalue(self, defaultvalue):
        """Set default value

        :param defaultvalue: default value to set
        :type defaultvalue: `unicode`
        """
        self.defaultvalue = valid_value(defaultvalue, self.type, self.options,
                                        self._get_separator())

#variable
    @trace
    def add_variable(self, name, type, value_off, conflevel, value_on=None,
                     comment=u''):
        """Add a variable

        :param name: variable name
        :type name: `unicode`
        :param type: variable type
        :type type: `unicode`
        :param value_off: variable value if state is 'off'
        :type value_off: `unicode`
        :param conflevel: variable conflevel
        :type conflevel: `ConfLevel`
        :param value_on: variable value if no value and state is 'on'
        :type value_on: `unicode`
        :param comment: comment for this variable
        :type comment: `unicode`
        :return: `Variable`
        """
        try:
            name = valid(name, 'unicode')
            type = valid(type, 'vtype')
            if self.get_variable(name=name, conflevel=conflevel):
                raise AlreadyExistsError('Variable with name %s and conflevel %s already exists' % (name, conflevel))
            #if type of the rule is not boolean, value_on could be None or not
            if value_on is not None:
                #value_on have to validate, but must be unicode in the database
#FIXME: Invalid => ValueError et aucun type error non plus !
#FIXME on ne connait pas la variable, rien ...
                need_valid = True
                if self.type == 'multi':
                    for var in self.options['values']:
                        if var['name'] == value_on:
                            need_valid = False
                            break
                if need_valid:
                    valid(value_on, type, self.options)
                value_on = valid(value_on, 'unicode')
            elif self.type == u'boolean':
                #if type of the rule is boolean, value_on must not be None
                raise ValueError('value_on must not be None for %s' % name)
            #value_off have to validate, but must be unicode in the database
            if value_off not in NORMALIZE_TYPE:
                valid(value_off, type)
            value_off = valid(value_off, 'unicode', self.options)
            comment = valid(comment, 'unicode')
        except ValueError, err:
            raise ValueError("Error in add_variable: " + unicode(err))

        variable = Variable(name=name, type=type, rule=self,
                            value_on=value_on, value_off=value_off,
                            conflevel=conflevel, comment=comment)
        if conflevel not in self.conflevels:
            self.conflevels.append(conflevel)

        return variable

    @trace
    def get_variable(self, name, conflevel):
        """Get variable by name or conflevel

        :return: Variable if exists, None otherwise
        """
        try:
            name = valid(name, 'unicode')
            return Variable.query.filter_by(name=name, conflevel=conflevel,
                                            rule=self).first()
        except Exception, err:
            raise Exception("Error in get_variable: " + unicode(err))

    @trace
    def get_variables(self):
        """Get all variables associated to this Rule

        :return: `list` of `Variable`
        """
        return self.vars

    @trace
    def del_variable(self, variable):
        """Delete a variable

        :param variable: variable to delete
        :type variable: `Variable`
        """
        if variable.get_rule() != self:
            raise NoRelationError('Variable not associated with this rule')
        del_variable(variable)

#comment
    @trace
    def add_comment(self, name, lang):
        """Add a new comment

        :param name: comment
        :type name: `unicode`
        :param lang: langage comment
        :type lang: `unicode`
        """
        try:
            name = valid(name, 'unicode')
            lang = valid(lang, 'unicode')
        except ValueError, err:
            raise ValueError("Error in add_comment: " + unicode(err))
        tcomment = copy(self.comments)
        tcomment[lang] = name
        self.comments = tcomment

    @trace
    def get_comment(self, lang):
        """Get a comment from specified langage

        :param lang: langage comment
        :type lang: `unicode`
        """
        try:
            lang = valid(lang, 'unicode')
        except ValueError, err:
            raise ValueError("Error in get_comment: " + unicode(err))
        if self.comments == {}:
            return u''
        if lang in self.comments:
            return self.comments[lang]
        for tlang in ordered_lang:
            if tlang in self.comments:
                return self.comments[tlang]
        return None
        #u'No translation in %s'%lang

#choice
    @trace
    def get_choice(self, group, user=None):
        """Get choice for a rule/group and optionaly an user.

        :param group: choice for a specified group
        :type group: `Group`
        :param user: choice for a specified user (None for All)
        :type user: `User`
        :return: Choice if exists, None otherwise
        """
        #tree_group = get_tree_group(group, user)
        #non_herited = True
        if not isinstance(group, Template) and not isinstance(group, Group):
            raise WrongObjError('Group in get_choice must be Template or Group')
        if user is not None:
            valid(user, User)
        return get_choices(group, user, rule=self)

    @trace
    def set_choice(self, group, state, value=None, user=None, platform=None):
        """Set a choice to the database.

        :param group: a choice is set for a group
        :type group: `Group`
        :param state: state of the choice (must be 'on', 'off' or 'free')
        :type state: `unicode`
        :param value: if choice must have value
        :type value: `unicode`
        :param user: a choice could be set for an user
        :type user: `User`
        :param platform: a choice could be set for a platform
        :type platform: `Platform`
        :return: the new choice
        """
        state = valid(state, 'state')
        if isinstance(group, Template):
            tgroup = None
            ttemplate = group
        elif isinstance(group, Group):
            tgroup = group
            ttemplate = None
        else:
            #raise TypeError('Error in set_choice: group need to be a Group or a Template for rule %s' % self.names)
            raise TypeError('Error in set_choice: group need to be a Group or '
                            'a Template for rule %s' % self.name)
        if user is not None:
            if not isinstance(user, User):
                raise TypeError('Error in set_choice: not a valid User')
            if user not in group.get_users():
                raise NoRelationError("User {0} not linked to group {1}".format(
                                      user.name, group.name))
            from gaspacho.conflevel import get_confuser, get_confcontext
            confuser = get_confuser()
            confcontext = get_confcontext()
            if confuser not in self.conflevels and confcontext not in \
                    self.conflevels:
                raise NoRelationError("Choice associate to an User but Rule"
                                      " don't have conflevel variable")
        if platform is not None:
            platform = valid(platform, Platform)
        if group.softs != [] and self.softs != []:
            if list(set(group.softs) & set(self.softs)) == []:
                raise NoRelationError('Group link to software not listed in this rule')

        choice = Choice.query.filter_by(rule=self, group=tgroup,
                                        template=ttemplate, user=user,
                                        platform=platform).first()
        if choice is None:
            #if no choice is already set to this combination
            if isinstance(group, Template):
                choice = Choice(rule=self, template=group, user=user,
                                platform=platform)
            else:
                choice = Choice(rule=self, group=group, user=user,
                                platform=platform)

        choice.set_state(state)
        choice.set_value(value)
        return choice

    @trace
    def del_choice(self, group, user=None, platform=None):
        """Delete a specified choice by rule/group/user/platform

        :param group: a choice is set for a group
        :type group: `Group`
        :param user: a choice could be set for an user
        :type user: `User`
        :param platform: a choice could be set for a platform
        :type platform: `Platform`
        """
        if user is not None:
            if not isinstance(user, User):
                raise TypeError('Error in del_choice: not a valid User')
        if platform is not None:
            valid(platform, Platform)

        if isinstance(group, Template):
            choice = Choice.query.filter_by(rule=self, template=group,
                                            user=user, platform=platform
                                            ).first()
        elif isinstance(group, Group):
            choice = Choice.query.filter_by(rule=self, group=group, user=user,
                                            platform=platform).first()
        else:
            raise TypeError('Error in del_choice: group need to be a Group or a Template for rule %s' % self)
        if choice is not None:
            choice.delete()

#platform
    @trace
    def get_platforms(self):
        """Get platforms for all related variables

        :return: `list` of `Platform`
        """
        try:
            ret = []
            for variable in self.vars:
                ret.extend(variable.plats)
            #return list of uniq platforms
            return list(set(ret))
        except Exception, err:
            raise Exception('Error in get_platforms: ' + unicode(err))


@trace
def del_rule(rule):
    """Delete a rule

    :param rule: rule to delete
    :type rule: `Rule`
    """
    try:
        valid(rule, Rule)
    except Exception, err:
        raise Exception("Error in del_rule: " + unicode(err))
    if rule.get_variables() != []:
        raise AlreadyInUseError('Remove associated variables before')
    #remove all choices related to this rule
    for choice in rule.choices:
        choice.delete()
    rule.tag = None
    rule.delete()


class Variable(Entity):
    """Elixir class for Variable

    :param name: name of the Variable for key
    :type name: `unicode`
    :param type: type of the Variable
    :type type: `unicode`
    :param value_on: value of the key if Rule state is "on"
    :type value_on: `unicode`
    :param value_off: value of the key if Rule state is "off"
    :type value_off: `unicode`
    :param comment: description of this variable
    :type comment: `unicode`
    :param rule: related Rule
    :type rule: `Rule`
    :param plats: related Platforms
    :type plats: `Platform`
    """
    name = Field(UnicodeText)
    type = Field(UnicodeText)
    value_on = Field(UnicodeText)
    value_off = Field(UnicodeText)
    comment = Field(UnicodeText)
    rule = ManyToOne('Rule')
    plats = ManyToMany('Platform')
    conflevel = ManyToOne('ConfLevel')
    imports = ManyToMany('Import')

#comment
    @trace
    def get_comment(self):
        """Get the variable comment.

        :return: comment.
        """
        return self.comment

    @trace
    def mod_comment(self, comment):
        """Modify comment.

        :param comment: new comment for this variable
        :type comment: `unicode`
        """
        try:
            comment = valid(comment, 'unicode')
        except ValueError, err:
            raise ValueError("Error in mod_comment: " + unicode(err))
        self.comment = comment

#rule
    @trace
    def get_rule(self):
        """Get the variable rule.

        :return: `Rule`
        """
        return self.rule

#platform
    @trace
    def add_platform(self, platform):
        """Add relation to a platform for this variable.

        :param platform: related platform
        :type platform: `Platform`
        """
        platform = valid(platform, Platform)
        if platform.has_variable(name=self.name, conflevel=self.conflevel,
                                 rule=self.rule):
            raise AlreadyExistsError('Variable name %s with conflevel %s '
                                     'already exists for this platform'
                                     % (self.name, self.conflevel))
        self.plats.append(platform)
        #for performance
        os = platform.osversion.os
        software = platform.softwareversion.software
        if not os in self.rule.oses:
            self.rule.oses.append(os)
        if not software in self.rule.softs:
            self.rule.softs.append(software)

    @trace
    def get_platforms(self):
        """Get all platforms related to this variable.

        :return: `list` of `Platform`
        """
        return self.plats

    #def del_platform(self, platform):
    #    #FIXME
    #    #remove os and software in rules but only if not in an other platform
    #    pass


@trace
def get_variables():
    """Get all variables.

    :return: `Variable`
    """
    return Variable.query.all()


@trace
def _load_choices(choices, user_level, tree_group, platforms, force_level):
    """Parse specified choices and sort it by groups.

    :param choices: all possible choices
    :type choices: `list` of `Choice`
    :param user_level: all choice for user and context level
    :type user_level: `boolean`
    :param tree_group: see `get_tree_group` for more information
    :type tree_group: `list` of `tuple`
    :param platforms: can filter by platforms
    :type platforms: `list` of `Platform`
    :param force_level: can filter by conflevel
    :type force_level: `ConfLevel`
    :return: `dict` like {(group1, user1): {rule1: choice, rule2: choice},
    (group1, user2): {rule3: choice}}
    """
    from gaspacho.conflevel import get_confuser, get_confcontext
    confuser = get_confuser()
    confcontext = get_confcontext()
    tchoices = {}
    for choice in choices:
        rule = choice.rule
        #if user level : only confuser et confcontext
        if (force_level is not None and force_level in rule.conflevels) or \
           (force_level is None and (user_level is True and
           (confuser in rule.conflevels or confcontext in rule.conflevels) or
                user_level is False)):
            #test if choice is group or template
            if choice.group:
                grp = choice.group
            else:
                grp = choice.template
            #if grp is in tree_group
            if (grp, choice.user) in tree_group:
                #remove choce for an other platform
                if platforms:
                    if choice.platform and choice.platform not in platforms:
                        continue
                    is_platform = False
                    set_platforms = set(platforms)
                    for variable in rule.get_variables():
                        if set_platforms & set(variable.get_platforms()):
                            is_platform = True
                            break
                    if not is_platform:
                        continue
                tchoices.setdefault((grp, choice.user), {})[rule] = choice
    return tchoices


@trace
def del_variable(variable):
    """Delete variable.

    :param variable: variable to delete
    :type variable: `Variable`
    """
    valid(variable, Variable)
    if variable.imports != []:
        raise AlreadyInUseError('Remove related import file before')
    if variable.get_platforms() != []:
        raise AlreadyInUseError('Remove associated platforms before')
    variable.rule = None
    variable.delete()


class DefaultChoice:
    """This class is only use to simulate a choice if default choice
    is not override.

    :param type: type of the default choice
    :type type: `unicode`
    :param state: default state (on, off or free)
    :type state: `unicode`
    :param value: default value if rule need value
    :type value: `unicode`
    """
    @trace
    def __init__(self, type, state, value, options):
        self.type = type
        self.state = state
        self.value = value
        self.options = options

    @trace
    def get_value(self):
        """Get DefaultChoice value
        """
        return valid(self.value, self.type, self.options)

    @trace
    def get_state(self):
        """Get DefaultChoice state
        """
        return self.state


@trace
def get_default_choice(rule):
    """Generate DefaultChoice for a specified rule

    :param rule: The rule
    :type rule: `Rule`
    :return: DefaultChoice or None if no default
    """
    #get default value
    if rule.defaultstate != u'free':
        return DefaultChoice(rule.type, rule.defaultstate,
                             rule.get_defaultvalue(), rule.options)


@trace
def get_rules(defaultstate_not_free=False):
    """Get all rules

    :param defaultstate_not_free: True to get only rules with default state
    :type defaultstate_not_free: `boolean`
    :return: `list` of `Rule`
    """
    if not defaultstate_not_free:
        return Rule.query.all()
    return Rule.query.filter(not_(Rule.defaultstate.contains(u'free'))).all()


@trace
def get_choices(group, user=None, rule=None, platforms=None,
                force_level=None, heritage=True):
    """Get choices for a group/rule/user. By default search choice with
    heritage.

    :param group: choice for this group
    :type group: `Group`
    :param user: can filter by user
    :type user: `User`
    :param rule: can filter by rule
    :type rule: `Rule`
    :param platforms: can filter by platforms
    :type platforms: `list` of `Platform`
    :param force_level: can filter by conflevel
    :type force_level: `ConfLevel`
    :param heritage: check choice and, if True, search herited value
                     (or default is no herited value)
    :type heritage: `boolean`
    :return: if heritage (choice, herited), choice otherwise
    """

    if user:
        valid(user, User)
        if user not in group.users:
            raise NoRelationError("get_choices: user %s not in group %s" % (
                                  user.name, group.name))

    if rule is not None:
        valid(rule, Rule)
        if heritage:
            choices = Choice.query.filter_by(rule=rule).all()
        else:
            if isinstance(group, Group):
                choices = Choice.query.filter_by(rule=rule, group=group,
                                                 user=user).first()
            elif isinstance(group, Template):
                choices = Choice.query.filter_by(rule=rule, template=group,
                                                 user=user).first()
            else:
                raise NoRelationError('get_choices: %s is not a group' % group)
            if choices is None:
                choices = []
            else:
                choices = [choices]
    else:
        if heritage:
            choices = Choice.query.all()
        else:
            if isinstance(group, Group):
                choices = Choice.query.filter_by(group=group, user=user).all()
            elif isinstance(group, Template):
                choices = Choice.query.filter_by(template=group, user=user).all()
            else:
                raise WrongObjError('get_choices: %s is not a group' % group)

    if heritage:
        #load all choices for better performance
        tree_group = get_tree_group(group, user)
    else:
        tree_group = [(group, user)]

    #True if user, False if user is None
    user_level = not user is None
    tchoices = _load_choices(choices, user_level, tree_group, platforms,
                             force_level=force_level)
    #parse loaded choices to get choice
    choices = {}
    non_herited = True
    for group_user in tree_group:
        if group_user in tchoices:
            for trule, choice in tchoices[group_user].items():
                if not heritage:
                    choices[trule] = choice
                elif non_herited:
                    choices[trule] = (choice, None)
                elif trule in choices:
                    #only if no herited choice
                    if not choices[trule][1]:
                        choices[trule] = (choices[trule][0], choice)
                else:
                    #if no rule, choice is not set
                    choices[trule] = (None, choice)
        non_herited = False

    #get default value
    if heritage:
        if rule is not None:
            rules = [rule]
        else:
            rules = get_rules(defaultstate_not_free=True)

        for trule in rules:
            #this test is only here if rule is not None and free
            if rule is not None and trule.defaultstate != u'free':
                choice = get_default_choice(rule)
                if trule not in choices:
                    choices[trule] = (None, choice)
                elif not choices[trule][1]:
                    choices[trule] = (choices[trule][0], choice)

    if rule:
        if choices == {}:
            if heritage:
                return (None, None)
            else:
                return None
        return choices[rule]
    else:
        return choices


@trace
def get_tree_group(grp, user=None):
    """Build tree dependencies for group (Group + User)
    Examples of inherit:
    default (None, user1, user2)
     '--group1 (None, user1, user2)
         |--group2 (None, user1, user2)
         |
        template1 (None, user1, user2)
    - example 1: grp=group2, user=user1:
      build [(group2, user1), (group2, None), (group1, user1), (group1, None)
      (template1, user1), (tempate1, None), (default, user1), (default, None)]
    - example 2: grp=group2:
      [(group2, None), (group1, None), (template1, None), (default, None)]
    - example 3: grp=default:
      [(default, None)]

    :param group: choice for this group
    :type group: `Group`
    :param user: can filter by user
    :type user: `User`
    :return: `list` of `tuple`
    """
    ret = []

    if user is not None and user in grp.get_users():
        ret.append((grp, user))
    ret.append((grp, None))
    #if not template
    if isinstance(grp, Group):
        for template in grp.tmpls:
            if user is not None:
                ret.append((template, user))
            ret.append((template, None))
        grp2 = grp.parent
        if grp2 is not None:
            ret.extend(get_tree_group(grp2, user))
    return ret

# vim: ts=4 sw=4 expandtab
