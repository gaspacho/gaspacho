# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
from py.test import raises
from sys import exit

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'rule.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import get_category, initialize_database, commit_database,\
    add_group, get_group, close_database
from gaspacho.category import add_category
initialize_database(create=True, database=database)
from gaspacho.conflevel import get_confcomputer, get_confuser


def init_rule():
    try:
        category1 = add_category()
        category1.add_translation(lang='en', name='category1')
        tag1 = category1.add_tag()
        tag1.add_translation(lang='en', name='tag1')
        commit_database()
        add_group('grp1')
    except:
        print "Error when creation of group, category or tag, run test_category.py before"
        exit(1)

init_rule()

##############################################################################
# rule                                                                       #
##############################################################################


def test_add_rule_boolean():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    raises(Exception, "tag1.add_rule(type='boolean', defaultvalue='value')")
    raises(Exception,
           "tag1.add_rule(type='boolean', options={'separator': ';'})")
    tag1.add_rule(type='boolean')


def test_add_rule_integer():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    raises(Exception, "tag1.add_rule(type='integer')")
    raises(Exception, "tag1.add_rule(type='integer', defaultvalue='value')")
    raises(Exception, "tag1.add_rule(type='integer', defaultvalue='1', options={'separator': ';'})")
    raises(Exception, "tag1.add_rule(type='integer', defaultvalue='1', options={'min': 'value'})")
    raises(Exception, "tag1.add_rule(type='integer', defaultvalue='1', options={'min': '1', 'separator': ';'})")
    tag1.add_rule(type='integer', defaultvalue='1')
    tag1.add_rule(type='integer', defaultvalue='1', options={'min': '1'})
    tag1.add_rule(type='integer', defaultvalue='1', options={'min': '1', 'max': '2'})


def test_add_rule_enum():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    raises(Exception, "tag1.add_rule(type='enum')")
    raises(Exception, "tag1.add_rule(type='enum', defaultvalue='1')")
    raises(Exception, "tag1.add_rule(type='enum', options=[['1', 'first option'], ['2', 'second option']])")
    raises(Exception, "tag1.add_rule(type='enum', defaultvalue='1', options={'separator': ';'})")
    raises(Exception, "tag1.add_rule(type='enum', defaultvalue='value', options=[['1', 'first option'], ['2', 'second option']])")
    raises(Exception, "tag1.add_rule(type='enum', defaultvalue='3', options=[['1', 'first option'], ['2', 'second option']])")
    tag1.add_rule(type='enum', defaultvalue='1', options=[['1', 'first option'], ['2', 'second option']])
    tag1.add_rule(type='enum', defaultvalue='2', options=[['1', 'first option'], ['2', 'second option']])


def test_add_rule_list():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    raises(Exception, "tag1.add_rule(type='list')")
    raises(Exception, "tag1.add_rule(type='list', options=[['1', 'first option'], ['2', 'second option']])")
    raises(Exception, "tag1.add_rule(type='list', options={'min': 2})")
    tag1.add_rule(type='list', defaultvalue=['value'])
    tag1.add_rule(type='list', defaultvalue=['value'],
                  options={'separator': ';'})


def test_add_rule_unicode():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    raises(Exception, "tag1.add_rule(type='unicode')")
    raises(Exception, "tag1.add_rule(type='unicode', options=[['1', 'first option'], ['2', 'second option']])")
    raises(Exception, "tag1.add_rule(type='unicode', options={'min': 2})")
    tag1.add_rule(type='unicode', defaultvalue='1')
    tag1.add_rule(type='unicode', defaultvalue='value')


def test_add_rule_multi():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    options = {"separator": ":", "values": [{"type": "unicode", "name": "PROXY_HOST"}, {"type": "integer", "name": "PROXY_PORT", "options": {"max": 65535, "min": 0}}]}
    tag1.add_rule(type='multi', defaultvalue=["", "3128"], options=options)


def test_add_rule_defaultstate():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    raises(Exception, "tag1.add_rule(type='boolean', defaultstate='error')")
    tag1.add_rule(type='boolean', defaultstate='free')
    tag1.add_rule(type='boolean', defaultstate='on')
    tag1.add_rule(type='boolean', defaultstate='off')


def test_get_rule():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='rule1')
    rule2 = tag1.get_rule(lang='en', name='rule1')
    assert rule1 == rule2


def test_get_unknown_rule():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='unknown_rule')
    assert rule1 is None


def test_get_rules():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    assert [] != tag1.get_rules()


def test_del_rule():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    for rule in tag1.get_rules():
        tag1.del_rule(rule)
    assert [] == tag1.get_rules()


def test_get_rules_2():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    assert [] == tag1.get_rules()
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='rule1')
    assert [rule1] == tag1.get_rules()
    rule3 = tag1.add_rule(type='integer', defaultvalue='1')
    rule3.add_translation(lang='en', name='rule3')
    assert [rule1, rule3] == tag1.get_rules()


def test_get_name_rule():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    assert "rule1" == rule1.get_name('en')


def test_add_comment_rule():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    rule1.add_comment('comment rule1', 'en')


def test_get_comment_rule():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    assert 'comment rule1' == rule1.get_comment('en')


def test_get_defaulvalue_rule():
    category1 = get_category('category1', 'en')
    grp1 = get_group('grp1')
    assert grp1 is not None
    tag1 = category1.get_tag('tag1', 'en')
    rule = tag1.add_rule(type='list', defaultstate='on',
                         defaultvalue=['one', 'two'])
    choice, hchoice = rule.get_choice(grp1)
    assert choice is None
    assert hchoice is not None
    assert set(hchoice.get_value()) == set(['one', 'two'])


##############################################################################
# variable                                                                   #
##############################################################################
def test_add_variable_boolean():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    raises(Exception, "rule1.add_variable('variable1', type=u'boolean', value_on='yes', value_off='no')")
    raises(Exception, "rule1.add_variable('variable1', type=u'boolean', value_on='yes', conflevel=confuser)")
    raises(Exception, "rule1.add_variable('variable1', type=u'boolean', value_off='no', conflevel=confuser)")
    rule1.add_variable('variable1', type=u'boolean', value_on='yes',
                       value_off='no', conflevel=confuser)


def test_add_variable_unicode():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule_boolean = tag1.get_rule(lang='en', name='rule1')
    rule_boolean.add_variable('variable2', type='unicode', value_on='yes',
                              value_off='no', conflevel=confuser)
    raises(Exception, "rule_boolean.add_variable('variable3', type='unicode', value_off='no', conflevel=confuser)")
    raises(Exception, "rule_boolean.add_variable('variable3', type='unicode', value_on='yes', conflevel=confuser)")
    raises(Exception, "rule_boolean.add_variable('variable3', type='unicode', value_on='yes', value_off='no')")
    rule_integer = tag1.get_rule(lang='en', name='rule3')
    rule_integer.add_variable('variable3', type='unicode', value_on='yes',
                              value_off='no', conflevel=confuser)
    rule_integer.add_variable('variable4', type='unicode', value_off='no',
                              conflevel=confuser)


def test_add_variable_integer():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    rule1.add_variable('variable5', type='integer', value_on='0',
                       value_off='1', conflevel=confuser)
    raises(Exception, "rule1.add_variable('variable6', type='integer', value_on='0', value_off='value', conflevel=confuser)")


def test_add_variable_integer_value_unicode():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    raises(Exception, "rule1.add_variable('variable11', type='integer', value_on='unicode', value_off='0', conflevel=confuser)")
    raises(Exception, "rule1.add_variable('variable11', type='integer', value_on='0', value_off='unicode', conflevel=confuser)")


def test_add_variable_same_name():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    rule1.add_variable('variable_same', type='unicode', value_on='yes',
                       value_off='no', conflevel=confuser)
    raises(Exception, "rule1.add_variable('variable_same', type='unicode', value_on='yes', value_off='no', conflevel=confuser)")


def test_add_variable_same_name_diff_level():
    confcomputer = get_confcomputer()
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    rule1.add_variable('variable_same2', type='unicode', value_on='yes',
                       value_off='no', conflevel=confuser)
    rule1.add_variable('variable_same2', type='unicode', value_on='yes',
                       value_off='no', conflevel=confcomputer)


def test_add_variable_same_name_diff_rule():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    rule3 = tag1.get_rule(lang='en', name='rule3')
    rule1.add_variable('variable_same3', type='unicode', value_on='yes',
                       value_off='no', conflevel=confuser)
    rule3.add_variable('variable_same3', type='unicode', value_on='yes',
                       value_off='no', conflevel=confuser)


def test_normalize_type_variable():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    rule1.add_variable('variable6', type='unicode', value_on=u'',
                       value_off='no', conflevel=confuser)
    rule1.add_variable('variable7', type='unicode', value_on=u'SUPPR',
                       value_off='no', conflevel=confuser)
    rule1.add_variable('variable8', type='unicode', value_on=u'SUPPRALL',
                       value_off='no', conflevel=confuser)
    rule1.add_variable('variable9', type='unicode', value_on=u'IGNORE',
                       value_off='no', conflevel=confuser)


def test_get_variables():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule4 = tag1.add_rule(type='boolean')
    rule4.add_translation(lang='en', name='rule4')
    assert [] == rule4.get_variables()
    variable1 = rule4.add_variable('variable1', type='unicode', value_on=u'',
                                   value_off='no', conflevel=confuser,
                                   comment='comment10')
    assert [variable1] == rule4.get_variables()
    variable2 = rule4.add_variable('variable2', type='unicode', value_on=u'',
                                   value_off='no', conflevel=confuser,
                                   comment='comment10')
    assert set([variable1, variable2]) == set(rule4.get_variables())


def test_get_variable():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule4 = tag1.add_rule(type='boolean')
    variable3 = rule4.add_variable('variable3', type='unicode', value_on=u'',
                                   value_off='no', conflevel=confuser,
                                   comment='comment10')
    variable3_2 = rule4.get_variable('variable3', confuser)
    assert variable3 == variable3_2


def test_get_unknown_variable():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule4 = tag1.add_rule(type='boolean')
    variable = rule4.get_variable('unknown_variable', confuser)
    assert variable is None


def test_comment_variable():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    rule1.add_variable('variable10', type='unicode', value_on=u'',
                       value_off='no', conflevel=confuser, comment='comment10')


def test_get_comment_variable():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    variable10 = rule1.get_variable('variable10', confuser)
    assert 'comment10' == variable10.get_comment()


def test_get_comment_variable_2():
    confuser = get_confuser()
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule1 = tag1.get_rule(lang='en', name='rule1')
    variable10 = rule1.get_variable('variable10', confuser)
    assert 'comment10' == variable10.get_comment()
    variable10.mod_comment('comment new 10')
    assert 'comment new 10' == variable10.get_comment()


def test_del_rule_variable():
    category1 = get_category('category1', 'en')
    tag1 = category1.get_tag('tag1', 'en')
    rule = tag1.get_rules()[0]
    assert rule.vars != []
    raises(Exception, 'tag1.del_rule(rule)')


def test_close_database():
    commit_database()
    close_database()
