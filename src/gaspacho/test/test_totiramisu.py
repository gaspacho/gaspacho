# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
#from py.test import raises
from sys import exit

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'platform.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import initialize_database, add_user, add_template, add_group
from gaspacho.platform import add_os, add_software, add_path, add_platform
from gaspacho.category import add_category
from gaspacho.conflevel import get_confcomputer, get_confuser, get_confcontext
from gaspacho.totiramisu import _gen_optdesc, load_config

initialize_database(create=True, database=database)

##############################################################################
#                                                                            #
##############################################################################

try:
    os1 = add_os('os1')
    osversion1 = os1.add_version('osversion11')
    osversion2 = os1.add_version('osversion12')
    os2 = add_os('os2')
    software1 = add_software('software1')
    software2 = add_software('software2')
    softwareversion1 = software1.add_version('softwareversion1')
    softwareversion2 = software2.add_version('softwareversion2')
    path1 = add_path('path1', 'moz')
    path2 = add_path('path2', 'moz', 'info')
    platform1 = add_platform(path1, osversion1, softwareversion1, [{'name': 'application'}])
    platform2 = add_platform(path1, osversion2, softwareversion2, [{'name': 'application2'}])
except:
    import traceback
    traceback.print_exc()
    print "unable to create a platform"
    exit(1)

try:
    user1 = add_user('user1')
except:
    print "unable to create an user"
    exit(1)

try:
    """
    grp1 - grp2
    tmpl1 - grp3
    grp4
    grp5
    grp6
    """
    tmpl1 = add_template('tmpl1')
    tmpl1.add_user(user1)
    grp1 = add_group('grp1')
    grp1.add_user(user1)
    grp1.add_computer('*', u'dns')
    grp2 = add_group('grp2', parent=grp1)
    grp2.add_user(user1)
    grp2.add_computer('a*', u'dns')
    grp2.add_computer('192.168.1.*', 'ip')
    grp3 = add_group('grp3')
    grp3.add_user(user1)
    grp3.add_computer('b*', 'dns')
    grp3.add_template(tmpl1)
    grp4 = add_group('grp4')
    grp4.add_computer('c*', 'dns')
    grp5 = add_group('grp5')
    grp5.add_computer('c*', 'dns')
    grp5.add_software(software1)
    grp6 = add_group('grp6')
except:
    import traceback
    traceback.print_exc()
    print "unable to create a group"
    exit(1)

try:
    confuser = get_confuser()
    confcomputer = get_confcomputer()
    confcontext = get_confcontext()
    #rules
    category1 = add_category()
    category1.add_translation(lang='en', name='category1')
    tag1 = category1.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    #boolean
    rule1 = tag1.add_rule(type='boolean')
    rule2 = tag1.add_rule(type='boolean')
    rule3 = tag1.add_rule(type='unicode', defaultvalue='oui', display=False, defaultstate='on')
    rule4 = tag1.add_rule(type='boolean')
    options = {"separator": ":", "values": [{"type": "unicode", "name": "PROXY_HOST"}, {"type": "integer", "name": "PROXY_PORT", "options": {"max": 65535, "min": 0}}]}
    rule5 = tag1.add_rule(type='multi', defaultvalue=["", 3128], options=options)

    variable1 = rule1.add_variable('variable_confuser', type='unicode',
                                   value_on='yes', value_off='no', conflevel=confuser)
    variable1.add_platform(platform1)
    variable2 = rule2.add_variable('variable_confcomputer', type=u'boolean',
                                   value_on='yes', value_off='no', conflevel=confcomputer)
    variable2.add_platform(platform1)
    variable2.add_platform(platform2)
    variable3 = rule3.add_variable('variable3_confuser', type='unicode',
                                   value_off='no', conflevel=confuser)
    variable3.add_platform(platform1)
    variable4 = rule4.add_variable('variable4_confuser', type='unicode',
                                   value_on='yes', value_off='no', conflevel=confuser)
    variable4.add_platform(platform2)
    variable5 = rule5.add_variable('variable5_confuser', type='unicode',
                                   value_on='PROXY_HOST', value_off='IGNORE',
                                   conflevel=confuser)
except:
    import traceback
    traceback.print_exc()
    print "unable to add rule"
    exit(1)


def add_choice_1():
    if (None, None) == rule1.get_choice(grp1):
        rule1.set_choice(grp1, 'on')


def add_choice_2():
    if (None, None) == rule2.get_choice(grp1):
        rule2.set_choice(grp1, 'on')


def add_tmpl_1():
    if (None, None) == rule1.get_choice(tmpl1):
        rule1.set_choice(tmpl1, 'off')


def add_choice_3():
    if None == rule1.get_choice(grp3)[0]:
        rule1.set_choice(grp3, 'on')


def add_choice_4():
    if None == rule1.get_choice(grp2)[0]:
        rule1.set_choice(grp2, 'off')


def test_to_tiramisu():
    descr = _gen_optdesc()
    descr.impl_build_cache()
    assert descr._cache_paths[1] == ('c1', 'c1.t1', 'c1.t1.r1',
                                     'c1.t1.r2', 'c1.t1.r3', 'c1.t1.r4',
                                     'c1.t1.r5', 'c1.t1.r5.r5', 'c1.t1.r5.m50',
                                     'c1.t1.r5.m51')
    assert descr.c1.t1.r1._opt_type == 'bool'
    assert descr.c1.t1.r3._opt_type == 'unicode'
    assert descr.c1.t1.r3.impl_getdefault() == u'oui'
    assert descr.c1.t1.r3.impl_get_information('state') == u'on'

#unicode est hidden

#    options = Field(PickleType)
#    comments = Field(PickleType)
#    choices = OneToMany('Choice')
#    #just for performance, don't touch this
#    conflevels = ManyToMany('ConfLevel')
#    oses = ManyToMany('OS')
#    softs = ManyToMany('Software')


def test_to_tiramisu_config():
    config = load_config(grp1, None)
    assert config.c1.t1.r1 is None
    config.c1.t1.r1 = False
    assert config.c1.t1.r1 is False
    assert config.c1.t1.r3 == u'oui'
    config.c1.t1.r3 = u'no'
    assert config.c1.t1.r3 == u'no'


def test_to_tiramisu_hidden():
    descr = _gen_optdesc()
    assert 'hidden' not in descr.c1.t1.r1._properties
    assert 'hidden' not in descr.c1.t1.r2._properties
    assert 'hidden' in descr.c1.t1.r3._properties
    assert 'hidden' not in descr.c1.t1.r4._properties
