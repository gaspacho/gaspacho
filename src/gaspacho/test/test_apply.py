# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
from py.test import raises
from sys import exit

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'platform.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import *
from gaspacho.platform import add_os, add_software, add_path, add_platform
from gaspacho.category import add_category
from gaspacho.conflevel import get_confcomputer, get_confuser, get_confcontext
from gaspacho.apply import apply_choices, get_templates_gasp, Apply, gen_cache_db

initialize_database(create=True, database=database)

##############################################################################
#                                                                            #
##############################################################################

try:
    os1 = add_os('os1')
    osversion1 = os1.add_version('osversion11')
    osversion2 = os1.add_version('osversion12')
    os2 = add_os('os2')
    software1 = add_software('software1')
    software2 = add_software('software2')
    softwareversion1 = software1.add_version('softwareversion1')
    softwareversion2 = software2.add_version('softwareversion2')
    path1 = add_path('path1', 'moz')
    path2 = add_path('path2', 'moz', 'info')
    platform1 = add_platform(path1, osversion1, softwareversion1, [{'name': 'application'}])
    platform2 = add_platform(path1, osversion2, softwareversion2, [{'name': 'application2'}])
except:
    import traceback
    traceback.print_exc()
    print "unable to create a platform"
    exit(1)

try:
    user1 = add_user('user1')
except:
    print "unable to create an user"
    exit(1)

try:
    """
    grp1 - grp2
    tmpl1 - grp3
    grp4
    grp5
    grp6
    """
    tmpl1 = add_template('tmpl1')
    tmpl1.add_user(user1)
    grp1 = add_group('grp1')
    grp1.add_user(user1)
    grp1.add_computer('*', u'dns')
    grp2 = add_group('grp2', parent=grp1)
    grp2.add_user(user1)
    grp2.add_computer('a*', u'dns')
    grp2.add_computer('192.168.1.*', 'ip')
    grp3 = add_group('grp3')
    grp3.add_user(user1)
    grp3.add_computer('b*', 'dns')
    grp3.add_template(tmpl1)
    grp4 = add_group('grp4')
    grp4.add_computer('c*', 'dns')
    grp5 = add_group('grp5')
    grp5.add_computer('c*', 'dns')
    grp5.add_software(software1)
    grp6 = add_group('grp6')
except:
    import traceback
    traceback.print_exc()
    print "unable to create a group"
    exit(1)

try:
    confuser = get_confuser()
    confcomputer = get_confcomputer()
    confcontext = get_confcontext()
    #rules
    category1 = add_category()
    category1.add_translation(lang='en', name='category1')
    tag1 = category1.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    #boolean
    rule1 = tag1.add_rule(type='boolean')
    rule2 = tag1.add_rule(type='boolean')
    rule3 = tag1.add_rule(type='unicode', defaultvalue='oui', display=False, defaultstate='on')
    rule4 = tag1.add_rule(type='boolean')
    variable1 = rule1.add_variable('variable_confuser', type='unicode',
                        value_on='yes', value_off='no', conflevel=confuser)
    variable1.add_platform(platform1)
    variable2 = rule2.add_variable('variable_confcomputer', type=u'boolean',
                        value_on='yes', value_off='no', conflevel=confcomputer)
    variable2.add_platform(platform1)
    variable2.add_platform(platform2)
    variable3 = rule3.add_variable('variable3_confuser', type='unicode',
                        value_off='no', conflevel=confuser)
    variable3.add_platform(platform1)
    variable4 = rule4.add_variable('variable4_confuser', type='unicode',
                        value_on='yes', value_off='no', conflevel=confuser)
    variable4.add_platform(platform2)
except:
    import traceback
    traceback.print_exc()
    print "unable to add rule"
    exit(1)

empty = {}
empty_confuser = {u'moz': {u'path1': [{'type': u'unicode', 'key': u'variable3_confuser', 'value': u'oui'}]}}

def add_choice_1():
    if (None, None) == rule1.get_choice(grp1):
        rule1.set_choice(grp1, 'on')

def add_choice_2():
    if (None, None) == rule2.get_choice(grp1):
        rule2.set_choice(grp1, 'on')

def add_tmpl_1():
    if (None, None) == rule1.get_choice(tmpl1):
        rule1.set_choice(tmpl1, 'off')

def add_choice_3():
    if None == rule1.get_choice(grp3)[0]:
        rule1.set_choice(grp3, 'on')

def add_choice_4():
    if None == rule1.get_choice(grp2)[0]:
        rule1.set_choice(grp2, 'off')

def test_apply_empty():
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp1]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp1]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp1]['user']['user']['user1']['os1_osversion11'] == empty_confuser
    assert plugins[grp1]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp2]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['user']['user1']['os1_osversion11'] == empty_confuser
    assert plugins[grp2]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp3]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['user']['user1']['os1_osversion11'] == empty_confuser
    assert plugins[grp3]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp4]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp4]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp5]['user']['all']['all']['os1_osversion11'] == empty_confuser
    #grp5 is link to software1
    raises(KeyError, "plugins[grp5]['user']['all']['all']['os1_osversion12']")
    assert not grp6 in plugins

def test_gen_apply_choices():
    group1 = {
        u'computer': {
            u'os1_osversion11': u'apply/grp1/os1/osversion11/computer.gasp',
            u'os1_osversion12': u'apply/grp1/os1/osversion12/computer.gasp'},
        u'user': {u'all': {u'all': {
            u'os1_osversion11': u'apply/grp1/os1/osversion11/user/ALL.gasp',
            u'os1_osversion12': u'apply/grp1/os1/osversion12/user/ALL.gasp'}},
            u'user': {u'user1': {
            u'os1_osversion11': u'apply/grp1/os1/osversion11/user/user/user1.gasp',
            u'os1_osversion12': u'apply/grp1/os1/osversion12/user/user/user1.gasp'}}},
        u'context': {u'all': {u'all': {
            u'os1_osversion11': u'apply/grp1/os1/osversion11/context/ALL.gasp',
            u'os1_osversion12': u'apply/grp1/os1/osversion12/context/ALL.gasp'}},
            u'user':{u'user1': {
            u'os1_osversion11': u'apply/grp1/os1/osversion11/context/user/user1.gasp',
            u'os1_osversion12': u'apply/grp1/os1/osversion12/context/user/user1.gasp'}}},
        'group': [(u'*', u'dns')]}
    group2 = {u'computer': {u'os1_osversion11': u'apply/grp2/os1/osversion11/computer.gasp',
            u'os1_osversion12': u'apply/grp2/os1/osversion12/computer.gasp'},
        u'user': {u'all': {u'all': {
            u'os1_osversion11': u'apply/grp2/os1/osversion11/user/ALL.gasp',
            u'os1_osversion12': u'apply/grp2/os1/osversion12/user/ALL.gasp'}},
            u'user': {u'user1': {
            u'os1_osversion11': u'apply/grp2/os1/osversion11/user/user/user1.gasp',
            u'os1_osversion12': u'apply/grp2/os1/osversion12/user/user/user1.gasp'}}},
        u'context': {u'all': {u'all': {
            u'os1_osversion11': u'apply/grp2/os1/osversion11/context/ALL.gasp',
            u'os1_osversion12': u'apply/grp2/os1/osversion12/context/ALL.gasp'}},
            u'user': {u'user1': {
            u'os1_osversion11': u'apply/grp2/os1/osversion11/context/user/user1.gasp',
            u'os1_osversion12': u'apply/grp2/os1/osversion12/context/user/user1.gasp'}}},
        u'group': [(u'a*', u'dns'), (u'192.168.1.*', u'ip')]}
    group3 = {
        u'computer': {u'os1_osversion11': u'apply/grp3/os1/osversion11/computer.gasp',
            u'os1_osversion12': u'apply/grp3/os1/osversion12/computer.gasp'},
        u'user': {u'all': {u'all': {
            u'os1_osversion11': u'apply/grp3/os1/osversion11/user/ALL.gasp',
            u'os1_osversion12': u'apply/grp3/os1/osversion12/user/ALL.gasp'}},
            u'user': {u'user1':
            {u'os1_osversion11': u'apply/grp3/os1/osversion11/user/user/user1.gasp',
            u'os1_osversion12': u'apply/grp3/os1/osversion12/user/user/user1.gasp'}}},
        u'context': {u'all': {u'all':
            {u'os1_osversion11': u'apply/grp3/os1/osversion11/context/ALL.gasp',
            u'os1_osversion12': u'apply/grp3/os1/osversion12/context/ALL.gasp'}},
            u'user': {u'user1':
            {u'os1_osversion11': u'apply/grp3/os1/osversion11/context/user/user1.gasp',
            u'os1_osversion12': u'apply/grp3/os1/osversion12/context/user/user1.gasp'}}},
        u'group': [(u'b*', u'dns')]}
    group5 = {
        u'computer':
            {u'os1_osversion11': u'apply/grp5/os1/osversion11/computer.gasp'},
        u'user': {u'all': {u'all':
            {u'os1_osversion11': u'apply/grp5/os1/osversion11/user/ALL.gasp'}}},
        u'context': {u'all': {u'all':
            {u'os1_osversion11': u'apply/grp5/os1/osversion11/context/ALL.gasp'}}},
        u'group': [(u'c*', u'dns')]}
    paths = apply_choices(create_file=False)[0]
    assert group5 == paths[grp5.name]
    assert group3 == paths[grp3.name]
    assert group2 == paths[grp2.name]
    assert group1 == paths[grp1.name]

def test_apply_user_choice1():
    add_choice_1()
    choices = {u'moz': {u'path1': [
        {'type': u'unicode', 'value': u'oui', 'key': u'variable3_confuser'},
        {'type': u'unicode', 'value': u'yes', 'key': u'variable_confuser'}
    ]}}
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp1]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp1]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp1]['user']['user']['user1']['os1_osversion11'] == choices
    assert plugins[grp1]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['user']['user1']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp3]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['user']['user1']['os1_osversion11'] == empty_confuser
    assert plugins[grp3]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp4]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp4]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp5]['user']['all']['all']['os1_osversion11'] == empty_confuser
    #grp5 is link to software1
    raises(KeyError, "plugins[grp5]['user']['all']['all']['os1_osversion12']")

def test_apply_computer_choice1():
    add_choice_1()
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp1]['computer']['all']['all']['os1_osversion11']['plugins'] == empty
    assert plugins[grp1]['computer']['all']['all']['os1_osversion12']['plugins'] == empty
    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['plugins'] == empty
    assert plugins[grp2]['computer']['all']['all']['os1_osversion12']['plugins'] == empty
    assert plugins[grp3]['computer']['all']['all']['os1_osversion11']['plugins'] == empty
    assert plugins[grp3]['computer']['all']['all']['os1_osversion12']['plugins'] == empty
    assert plugins[grp4]['computer']['all']['all']['os1_osversion11']['plugins'] == empty
    assert plugins[grp4]['computer']['all']['all']['os1_osversion12']['plugins'] == empty
    assert plugins[grp5]['computer']['all']['all']['os1_osversion11']['plugins'] == empty
    assert plugins[grp1]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp1]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp2]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp3]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp3]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp4]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp4]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp5]['computer']['all']['all']['os1_osversion11']['packages'] == []
    #grp5 is link to software1
    raises(KeyError, "plugins[grp5]['computer']['all']['all']['os1_osversion12']")

def test_apply_user_choice2():
    add_choice_1()
    add_choice_2()
    choices = {u'moz': {u'path1': [{'type': u'unicode', 'key': u'variable3_confuser', 'value': u'oui'}, {'type': u'unicode', 'value': u'yes', 'key': u'variable_confuser'}]}}
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp1]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp1]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp1]['user']['user']['user1']['os1_osversion11'] == choices
    assert plugins[grp1]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['user']['user1']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp3]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['user']['user1']['os1_osversion11'] == empty_confuser
    assert plugins[grp3]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp4]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp4]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp5]['user']['all']['all']['os1_osversion11'] == empty_confuser
    #grp5 is link to software1
    raises(KeyError, "plugins[grp5]['user']['all']['all']['os1_osversion12']")

def test_apply_computer_choice2():
    add_choice_1()
    add_choice_2()
    choices = {u'moz': {u'path1': [{'type': u'boolean', 'value': u'yes', 'key': u'variable_confcomputer'}]}}
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['plugins'] == choices
    assert plugins[grp1]['computer']['all']['all']['os1_osversion12']['plugins'] == choices
    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['plugins'] == choices
    assert plugins[grp2]['computer']['all']['all']['os1_osversion12']['plugins'] == choices
    assert plugins[grp3]['computer']['all']['all']['os1_osversion11']['plugins'] == empty
    assert plugins[grp3]['computer']['all']['all']['os1_osversion12']['plugins'] == empty
    assert plugins[grp4]['computer']['all']['all']['os1_osversion11']['plugins'] == empty
    assert plugins[grp4]['computer']['all']['all']['os1_osversion12']['plugins'] == empty
    assert plugins[grp5]['computer']['all']['all']['os1_osversion11']['plugins'] == empty

    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp1]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp2]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp3]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp3]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp4]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp4]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp5]['computer']['all']['all']['os1_osversion11']['packages'] == []
    #grp5 is link to software1
    raises(KeyError, "plugins[grp5]['computer']['all']['all']['os1_osversion12']")

def test_apply_empty_template():
    empty_tmpl = {tmpl1: {'computer': {'all': {'all': {}}},
            'user': {'all': {'all': {}}}, 'context': {'all': {'all': {}}}}}
    assert get_templates_gasp() == empty_tmpl

def test_apply_template():
    tmpl = {tmpl1: {'computer': {'all': {'all': {}}},
        'user': {'all': {'all': {u'os1_osversion11': {u'moz':
        {u'path1': {None: {u'variable_confuser': {'type': u'unicode', 'value': u'no', 'key': u'variable_confuser'}}}}}}},
        u'user': {u'user1': {u'os1_osversion11': {u'moz':
        {u'path1': {None: {u'variable_confuser': {'type': u'unicode', 'value': u'no', 'key': u'variable_confuser'}}}}}}}},
        'context': {'all': {'all': {}}}}}
    add_tmpl_1()
    gen_cache_db()
    assert get_templates_gasp() == tmpl

def test_apply_tmpl_user_choice2():
    add_choice_1()
    add_choice_2()
    add_tmpl_1()
    choices = {u'moz': {u'path1': [{'type': u'unicode', 'key': u'variable3_confuser', 'value': u'oui'}, {'type': u'unicode', 'value': u'yes', 'key': u'variable_confuser'}]}}
    choicestp = {u'moz': {u'path1': [{'type': u'unicode', 'key': u'variable3_confuser', 'value': u'oui'}, {'type': u'unicode', 'value': u'no', 'key': u'variable_confuser'}]}}
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp1]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp1]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp1]['user']['user']['user1']['os1_osversion11'] == choices
    assert plugins[grp1]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp2]['user']['user']['user1']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['all']['all']['os1_osversion11'] == choicestp
    assert plugins[grp3]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp3]['user']['user']['user1']['os1_osversion11'] == choicestp
    assert plugins[grp3]['user']['user']['user1']['os1_osversion12'] == empty
    assert plugins[grp4]['user']['all']['all']['os1_osversion11'] == empty_confuser
    assert plugins[grp4]['user']['all']['all']['os1_osversion12'] == empty
    assert plugins[grp5]['user']['all']['all']['os1_osversion11'] == empty_confuser
    #grp5 is link to software1
    raises(KeyError, "plugins[grp5]['user']['all']['all']['os1_osversion12']")

def test_apply_installpkg():
    add_choice_1()
    add_choice_2()
    add_tmpl_1()
    grp5.mod_installpkg(True)
    installpkg = ['application']
    gasp = Apply(grp5)
    packages = gasp.get_gasp_packages(platform1)
    assert packages == installpkg
    grp5.mod_installpkg(False)

def test_apply_installpkg_2():
    add_choice_1()
    add_choice_2()
    add_tmpl_1()
    grp5.mod_installpkg(True)
    installpkg = ['application']
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp1]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp2]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp2]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp3]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp3]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp4]['computer']['all']['all']['os1_osversion11']['packages'] == []
    assert plugins[grp4]['computer']['all']['all']['os1_osversion12']['packages'] == []
    assert plugins[grp5]['computer']['all']['all']['os1_osversion11']['packages'] == installpkg
    grp5.mod_installpkg(False)

def test_apply_herited_template():
    choices = {u'moz': {u'path1': [
        {'type': u'unicode', 'key': u'variable3_confuser', 'value': u'oui'},
        {'type': u'unicode', 'value': u'no', 'key': u'variable_confuser'}
    ]}}
    add_tmpl_1()
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp3]['user']['all']['all']['os1_osversion11'] == choices
    add_choice_3()
    choices['moz']['path1'][1]['value'] = u'yes'
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp3]['user']['all']['all']['os1_osversion11'] == choices

def test_apply_herited_group():
    choices = {u'moz': {u'path1': [
        {'type': u'unicode', 'key': u'variable3_confuser', 'value': u'oui'},
        {'type': u'unicode', 'value': u'yes', 'key': u'variable_confuser'}
    ]}}
    choices2 = {u'moz': {u'path1': [
        {'type': u'unicode', 'key': u'variable3_confuser', 'value': u'oui'},
        {'type': u'unicode', 'value': u'no', 'key': u'variable_confuser'}
    ]}}
    add_choice_1()
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp1]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['all']['all']['os1_osversion11'] == choices
    add_choice_4()
    plugins = apply_choices(create_file=False)[1]
    assert plugins[grp1]['user']['all']['all']['os1_osversion11'] == choices
    assert plugins[grp2]['user']['all']['all']['os1_osversion11'] == choices2

#def test_apply_choices():
#    add_choice_1()
#    print get_all_groups(add_user=True)
#    #print apply_choices(create_file=False)

def test_close_database():
    commit_database()
    close_database()

# vim: ts=4 sw=4 expandtab
