# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os.path import abspath, dirname, isdir, isfile, join
from os import makedirs, unlink
from py.test import raises

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'database.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import *

def test_uncreated_database():
    initialize_database(database=database)
    raises(Exception, "get_users()")

def test_created_database():
    initialize_database(create=True, database=database)
    assert get_users() == []

def test_rollback_database():
    assert [] == get_groups()
    grp1 = add_group('grp1')
    assert [grp1] == get_groups()
    rollback_database()
    assert [] == get_groups()

def test_commit_database():
    assert [] == get_groups()
    grp1 = add_group('grp1')
    assert [grp1] == get_groups()
    commit_database()
    assert [grp1] == get_groups()

def test_close_database():
    commit_database()
    close_database()

