# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
from py.test import raises
from sys import exit

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'group.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import *
from gaspacho.category import add_category, del_category
initialize_database(create=True, database=database)
from gaspacho.conflevel import get_confcomputer, get_confuser

##############################################################################
# conflevel                                                                  #
##############################################################################

def test_confcomputer():
    confcomputer = get_confcomputer()
    assert confcomputer.get_name() == u'confcomputer'

def test_confuser():
    confuser = get_confuser()
    assert confuser.get_name() == u'confuser'

##############################################################################
# category                                                                   #
##############################################################################

def test_add_category():
    category = add_category()
    category.add_translation(lang='en', name='example')

def test_get_category():
    category = add_category()
    category.add_translation(lang='en', name='test_get_category')
    new_category = get_category('test_get_category', 'en')
    assert category == new_category

def test_get_unknown_category():
    category = get_category('unknown', 'en')
    assert category == None

def test_delete_category():
    category = add_category()
    category.add_translation(lang='en', name='test_delete_category')
    new_category = get_category('test_delete_category', 'en')
    assert category == new_category
    del_category(new_category)
    assert get_category('test_delete_category', 'en') == None

def test_get_categories():
    #suppress all categories
    for cat in get_categories():
        del_category(cat)
    assert [] == get_categories()
    #add new category
    category = add_category()
    category.add_translation(lang='en', name='test_get_categories')
    assert [category] == get_categories()
    del_category(category)
    assert [] == get_categories()

def test_translation_category():
    category = add_category()
    category.add_translation(lang='en', name='test_translation_category')
    category.add_translation(lang='fr', name='exemple')
    category.add_translation(lang='it', name='esempio')
    category2 = get_category('exemple', 'fr')
    assert category == category2
    category2 = get_category('esempio', 'it')
    assert category == category2
    category2 = get_category('esempio', 'fr')
    assert None == category2
    del_category(category)

def test_get_name_category():
    category = add_category()
    category.add_translation(lang='en', name='test_get_name_category')
    category.add_translation(lang='fr', name='exemple')
    category.add_translation(lang='it', name='esempio')
    category = get_category('test_get_name_category', 'en')
    assert category.get_name('en') == u'test_get_name_category'
    assert category.get_name('fr') == u'exemple'
    assert category.get_name('it') == u'esempio'
    assert category.get_name('de') == u'test_get_name_category'
    del_category(category)

def test_category_without_translation():
    category = add_category()
    category.add_translation(lang='en',
                             name='test_category_without_translation')
    category1 = add_category()
    assert category1.get_name('en') == None
    assert set([category, category1]) == set(get_categories())
    del_category(category)
    del_category(category1)

def test_exists_category():
    category = add_category()
    category.add_translation(lang='en', name='test_exists_category')
    category1 = add_category()
    raises(Exception,
        "category1.add_translation(lang='en', name='test_exists_category')")
    del_category(category)
    del_category(category1)

def test_rewrite_translation():
    category = add_category()
    category.add_translation(lang='en', name='test_rewrite_translation')
    assert category.get_name('fr') == u'test_rewrite_translation'
    raises(Exception,
        "category.add_translation(lang='en', name='test_rewrite_translation)")
    del_category(category)

def test_mod_translation_category():
    category = add_category()
    category.add_translation(lang='en', name='test_mod_translation_category')
    category.mod_translation(lang='en', name='example')
    assert category.get_name('en') == u'example'
    raises(Exception, "category.mod_translation(lang='de', 'beispiel')")
    raises(Exception, "category.mod_translation(lang='en', 'example')")
    del_category(category)

#comment
def test_create_comment_category():
    category = add_category(comment='add comment')
    category.add_translation('comment_fr', 'fr')
    assert category.get_comment() == 'add comment'
    del_category(category)

def test_mod_comment_category():
    category = add_category()
    assert category.get_comment() == u''
    category.mod_comment('new comment')

    category2 = add_category(comment='add comment')
    assert category2.get_comment() == 'add comment'
    category2.mod_comment('new comment')
    assert category2.get_comment() == 'new comment'

    del_category(category)
    del_category(category2)

##############################################################################
# tag                                                                        #
##############################################################################

def test_add_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_add_tag')
    tag = category.add_tag()
    tag.add_translation(lang='en', name='add_tag1')
    category.del_tag(tag)
    del_category(category)

def test_add_tag_category_no_translation():
    category = add_category()
    raises(Exception, "category.add_tag()")
    del_category(category)

def test_del_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_del_tag')
    assert category.get_tags() == []
    tag = category.add_tag()
    tag.add_translation(lang='en', name='tag1')
    assert category.get_tags() == [tag]
    category.del_tag(tag)
    assert category.get_tags() == []
    del_category(category)
    
def test_get_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_get_tag')
    tag = category.add_tag()
    tag.add_translation(lang='en', name='tag1')
    assert category.get_tag('tag1', 'en') == tag
    category.del_tag(tag)
    del_category(category)

def test_get_unknown_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_get_unknown_tag')
    assert category.get_tag('unknowntag', 'en') == None
    del_category(category)

def test_get_tags():
    category = add_category()
    category.add_translation(lang='en', name='test_get_tags')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    assert [tag1] == category.get_tags()
    tag2 = category.add_tag()
    tag2.add_translation(lang='en', name='tag2')
    assert [tag1, tag2] == category.get_tags()
    category.del_tag(tag1)
    category.del_tag(tag2)
    del_category(category)

def test_translation_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_translation_tag')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    tag1.add_translation(lang='fr', name='tag1_fr')
    tag1.add_translation(lang='it', name='tag1_it')
    tag2 = category.get_tag('tag1_fr', 'fr')
    tag3 = category.get_tag('tag1_fr', 'fr')
    assert tag1 == tag2 == tag3
    category.del_tag(tag1)
    del_category(category)

def test_get_name_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_get_name_tag')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    tag1.add_translation(lang='fr', name='tag1_fr')
    tag1.add_translation(lang='it', name='tag1_it')
    assert tag1.get_name('fr') == u'tag1_fr'
    assert tag1.get_name('it') == u'tag1_it'
    assert tag1.get_name('unknown') == u'tag1'
    category.del_tag(tag1)
    del_category(category)

def test_tag_without_translation():
    category = add_category()
    category.add_translation(lang='en', name='test_tag_without_translation')
    tag1 = category.add_tag()
    assert tag1.get_name('en') == None
    assert set([tag1]) == set(category.get_tags())
    category.del_tag(tag1)
    del_category(category)

def test_exists_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_exists_tag')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    raises(Exception, "tag1.add_translation(lang='en', name='tag1')")
    category.del_tag(tag1)
    del_category(category)

def test_exists_tag_other_category():
    category1 = add_category()
    category1.add_translation(lang='en', name='test_exists_tag_other_category')
    tag1 = category1.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    category2 = add_category()
    category2.add_translation(lang='en', name='test_exists_tag_other_category1')
    tag2 = category2.add_tag()
    tag2.add_translation(lang='en', name='tag1')
    assert tag1.get_name('en') == tag2.get_name('en')
    category1.del_tag(tag1)
    del_category(category1)
    category2.del_tag(tag2)
    del_category(category2)

def test_rewrite_translation_tag():
    category = add_category()
    category.add_translation(lang='en', name='test_rewrite_translation_tag')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    assert tag1.get_name('en') == u'tag1'
    raises(Exception, "category.add_translation(lang='en', name='tag1_new')")
    category.del_tag(tag1)
    del_category(category)

def test_mod_translation_tag():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    tag1.add_translation(lang='fr', name='tag1_fr')
    tag1.mod_translation(lang='fr', name='tag1_fr_new')
    assert tag1.get_name('fr') == u'tag1_fr_new'
    raises(Exception, "tag1.mod_translation(lang='de', 'tag1_de')")
    raises(Exception, "tag1.mod_translation(lang='fr', 'tag1')")
    category.del_tag(tag1)
    del_category(category)

def test_delete_category_with_tags():
    """
    try to delete a category with associated tag
    """
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    raises(Exception, "del_category(category)")
    category.del_tag(tag1)
    del_category(category)

#comment
def test_create_comment_tag():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag(comment='add comment')
    tag1.add_translation('tag comment', 'en')
    assert tag1.get_comment() == 'add comment'
    category.del_tag(tag1)
    del_category(category)

def test_mod_comment_tag():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    assert tag1.get_comment() == u''
    tag1.mod_comment('new comment')
    assert tag1.get_comment() == 'new comment'
    tag2 = category.add_tag('new comment')
    tag2.add_translation('tag2', 'en')
    assert tag2.get_comment() == 'new comment'
    tag2.mod_comment('new comment')
    assert tag2.get_comment() == 'new comment'
    category.del_tag(tag2)
    category.del_tag(tag1)
    del_category(category)

##############################################################################
# rule                                                                       #
##############################################################################

def test_add_rule():
    category = add_category()
    category.add_translation(lang='en', name='category')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    rule2 = tag1.add_rule(type='boolean')
    rule2.add_translation(lang='en', name='example 2')
    try:
        tag1.del_rule(rule1)
        tag1.del_rule(rule2)
        category.del_tag(tag1)
        del_category(category)
    except:
        pass

def test_del_rule():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    del_category(category)

def test_del_rule_with_wrong_tag():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    tag2 = category.add_tag()
    tag2.add_translation(lang='en', name='tag2')
    raises(Exception, 'tag2.del_rule(rule1)')
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    category.del_tag(tag2)
    del_category(category)

def test_del_tag_with_rule():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    raises(Exception, "category.del_tag(tag1)")
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    del_category(category)

def test_get_rule():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    rule2 = tag1.add_rule(type='boolean')
    rule2.add_translation(lang='en', name='example 2')
    trule1 = tag1.get_rule("example 1", 'en')
    assert rule1 != None
    assert rule1 == trule1
    trule2 = tag1.get_rule("example 2", 'en')
    assert rule2 != None
    assert rule2 == trule2
    tag1.del_rule(rule1)
    tag1.del_rule(rule2)
    category.del_tag(tag1)
    del_category(category)

def test_get_rules():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    rule2 = tag1.add_rule(type='boolean')
    rule2.add_translation(lang='en', name='example 2')
    assert set([rule1, rule2]) == set(tag1.get_rules())
    tag1.del_rule(rule1)
    tag1.del_rule(rule2)
    category.del_tag(tag1)
    del_category(category)

def test_get_unknown_rule():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.get_rule("unknown 1", 'en')
    assert rule1 == None
    category.del_tag(tag1)
    del_category(category)

def test_get_rules_confuser():
    confcomputer = get_confcomputer()
    confuser = get_confuser()
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    v1 = rule1.add_variable(name='variable1', type='boolean', value_on=True,
                        value_off=False, conflevel=confuser)
    assert set([rule1]) == set(tag1.get_rules(user_level=True))
    assert set([rule1]) == set(tag1.get_rules())

    rule2 = tag1.add_rule(type='boolean')
    rule2.add_translation(lang='en', name='example 2')
    v2 = rule2.add_variable(name='variable2', type='boolean', value_on=True,
                        value_off=False, conflevel=confcomputer)
    assert set([rule1]) == set(tag1.get_rules(user_level=True))
    assert set([rule1, rule2]) == set(tag1.get_rules())
    v3 = rule2.add_variable(name='variable3', type='boolean', value_on=True,
                        value_off=False, conflevel=confuser)
    assert set([rule1, rule2]) == set(tag1.get_rules(user_level=True))
    assert set([rule1, rule2]) == set(tag1.get_rules())
    try:
        rule1.del_variable(v1)
        rule2.del_variable(v2)
        rule2.del_variable(v3)
        tag1.del_rule(rule1)
        tag1.del_rule(rule2)
        category.del_tag(tag1)
        del_category(category)
    except:
        pass

def test_del_rule_with_variable():
    confcomputer = get_confcomputer()
    confuser = get_confuser()
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    variable = rule1.add_variable(name='variable1', type='boolean', value_on=True,
                        value_off=False, conflevel=confuser)
    raises(Exception, "tag1.del_rule(rule1)")
    try:
        rule1.del_variable(variable)
        tag1.del_rule(rule1)
        category.del_tag(tag1)
        del_category(category)
    except:
        pass

def test_del_variable():
    confcomputer = get_confcomputer()
    confuser = get_confuser()
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    variable = rule1.add_variable(name='variable1', type='boolean', value_on=True,
                        value_off=False, conflevel=confuser)
    rule1.del_variable(variable)
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    del_category(category)

def test_add_rule_display():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean', display=False)
    rule1.add_translation(lang='en', name='example 1')
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    del_category(category)

def test_get_rules_display():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    rule2 = tag1.add_rule(type='boolean')
    rule2.add_translation(lang='en', name='example 2')
    rule3 = tag1.add_rule(type='boolean', display=False)
    rule3.add_translation(lang='en', name='example 3')
    assert set([rule1, rule2, rule3]) == set(tag1.get_rules())
    assert set([rule1, rule2]) == set(tag1.get_rules(display=True))
    assert set([rule1, rule2, rule3]) == set(tag1.get_rules(display=False))
    tag1.del_rule(rule3)
    tag1.del_rule(rule2)
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    del_category(category)

def test_add_same_translation_rule_same_tag():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean', display=False)
    rule1.add_translation(lang='en', name='same name rule')
    rule2 = tag1.add_rule(type='boolean', display=False)
    raises(Exception, "rule2.add_translation(lang='en', name='same name rule')")
    tag1.del_rule(rule2)
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    del_category(category)

def test_add_same_translation_rule_diff_tags():
    category = add_category()
    category.add_translation(lang='en', name='category1')
    tag1 = category.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    tag2 = category.add_tag()
    tag2.add_translation(lang='en', name='tag2')
    rule1 = tag1.add_rule(type='boolean', display=False)
    rule1.add_translation(lang='en', name='same name rule')
    rule2 = tag2.add_rule(type='boolean', display=False)
    rule2.add_translation(lang='en', name='same name rule')
    tag2.del_rule(rule2)
    tag1.del_rule(rule1)
    category.del_tag(tag1)
    category.del_tag(tag2)
    del_category(category)

def test_get_categories_conflevel():
    confcomputer = get_confcomputer()
    confuser = get_confuser()
    category1 = add_category()
    category1.add_translation(lang='en', name='category1')
    category2 = add_category()
    category2.add_translation(lang='en', name='category2')
    tag1 = category1.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    rule1 = tag1.add_rule(type='boolean')
    rule1.add_translation(lang='en', name='example 1')
    variable1 = rule1.add_variable(name='variable1', type='boolean', value_on=True,
                        value_off=False, conflevel=confuser)
    assert [category1] == get_categories(user_level=True)
    assert [category1, category2] == get_categories(user_level=False)
    rule2 = tag1.add_rule(type='boolean')
    rule2.add_translation(lang='en', name='name rule 3')
    variable2 = rule2.add_variable(name='variable2', type='boolean', value_on=True,
                        value_off=False, conflevel=confuser)
    assert [category1] == get_categories(user_level=True)
    assert [category1, category2] == get_categories(user_level=False)
    variable3 = rule2.add_variable(name='variable3', type='boolean', value_on=True,
                        value_off=False, conflevel=confcomputer)
    assert [category1] == get_categories(user_level=True)
    assert [category1, category2] == get_categories(user_level=False)
    rule2.del_variable(variable3)
    rule2.del_variable(variable2)
    rule1.del_variable(variable1)
    tag1.del_rule(rule2)
    tag1.del_rule(rule1)
    category1.del_tag(tag1)
    del_category(category2)
    del_category(category1)

##FIXME category par groupe


def test_close_database():
    commit_database()
    close_database()
