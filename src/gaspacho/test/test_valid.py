#!/usr/bin/python
#-*- coding: utf-8 -*-

import unittest
from gaspacho.valid import normalize_unicode, valid


class TestValid(unittest.TestCase):

    # normalise_unicode
    def test_normalize_unicode(self):
        self.assertEqual(type(normalize_unicode(str('toto'))), unicode)
        self.assertEqual(type(normalize_unicode(u'toto')), unicode)
        self.assertEqual(normalize_unicode(str('toto')), u'toto')

    def test_normalize_unicode_value(self):
        self.assertRaises(ValueError, normalize_unicode, 1)

    # valid
    def test_valid_unicode(self):
        self.assertEqual(valid(2, 'unicode'), u'2')
        self.assertEqual(valid(str('abc'), 'unicode'), u'abc')
        self.assertEqual(valid(u'abc', 'unicode'), u'abc')

#    def test_valid_raises_unicode(self):
#        self.assertRaises(ValueError, valid, ['a', 'b', 'c'], 'unicode')

    def test_valid_type(self):
        for typ in [u'boolean', u'unicode', u'integer', u'ip', u'enum', u'multi', u'list']:
            self.assertEqual(valid(typ, 'type'), typ)

    def test_valid_raises_type(self):
        self.assertRaises(ValueError, valid, 'abc', 'type')

    def test_valid_state(self):
        self.assertEqual(valid('on', 'state'), u'on')
        self.assertEqual(valid('off', 'state'), u'off')
        self.assertEqual(valid('free', 'state'), u'free')

    def test_valid_raises_state(self):
        self.assertRaises(ValueError, valid, 'abc', 'state')

    def test_valid_user(self):
        for user in [u'user', u'usergroup']:
            self.assertEqual(valid(user, 'user'), user)

    def test_valid_raises_user(self):
        self.assertRaises(ValueError, valid, 'abc', 'user')

    def test_valid_computer(self):
        for computer in [u'ip', u'dns']:
            self.assertEqual(valid(computer, 'computer'), computer)

    def test_valid_raises_computer(self):
        self.assertRaises(ValueError, valid, 'abc', 'computer')

    #FIXME: no test for multi

    def test_valid_boolean(self):
        for bool in [1, 'true', 'True', True]:
            self.assertEqual(valid(bool, 'boolean'), True)
        for bool in [0, 'false', 'False', False]:
            self.assertEqual(valid(bool, 'boolean'), False)

    def test_valid_raises_boolean(self):
        self.assertRaises(ValueError, valid, 'abc', 'boolean')

    def test_valid_integer(self):
        self.assertEqual(valid(2, 'integer'), 2)
        self.assertEqual(valid(str(2), 'integer'), 2)
        self.assertEqual(valid(1, 'integer', {'min': 1}), 1)
        self.assertEqual(valid(2, 'integer', {'min': 1}), 2)
        self.assertEqual(valid(2, 'integer', {'max': 2}), 2)
        self.assertEqual(valid(2, 'integer', {'max': 3}), 2)
        self.assertEqual(valid(2, 'integer', {'min': 1, 'max': 3}), 2)

    def test_valid_raises_integer(self):
        self.assertRaises(ValueError, valid, str('abc'), 'integer')
        self.assertRaises(ValueError, valid, 2, 'integer', {'min': 3})
        self.assertRaises(ValueError, valid, 6, 'integer', {'max': 5})
        # integer must not be None
        self.assertRaises(ValueError, valid, None, 'integer')

    def test_valid_list(self):
        self.assertRaises(ValueError, valid, str('abc'), 'list', {})
        self.assertEqual(valid(['abc'], 'list', {}), ['abc'])

    # FIXME: no test for ip

    def test_valid_opt_enum(self):
        self.assertEqual(valid([['a', '1'], ['b', '2'], ['c', '3']], 'opt_enum'), [['a', '1'], ['b', '2'], ['c', '3']])

    def test_valid_enum(self):
        self.assertEqual(valid('a', 'enum', [['a', '1'], ['b', '2'], ['c', '3']]), 'a')

    def test_valid_raises_enum(self):
        self.assertRaises(ValueError, valid, 'd', 'enum', [['a', '1'], ['b', '2'], ['c', '3']])

    def test_valid_raises_opt_enum(self):
        self.assertRaises(ValueError, valid, 'abc', 'opt_enum')
    # FIXME: no test for else

if __name__ == '__main__':
    unittest.main()

# vim: ts=4 sw=4 expandtab
