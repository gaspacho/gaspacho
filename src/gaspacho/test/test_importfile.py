# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
from py.test import raises
from sys import exit
try:
    import json
except:
    #for python 2.5
    import simplejson as json

from gaspacho import commit_database, initialize_database, \
        rollback_database, load, get_categories, get_category, get_path, \
        get_os, get_software, get_platform, add_group, get_platforms
from gaspacho.conflevel import get_confcomputer, get_confuser, get_confcontext
from gaspacho.error import AlreadyExistsError
from gaspacho.choice import Choice
from gaspacho.category import Tag
from gaspacho.rule import Rule, Variable

here = dirname(abspath(__file__))
from copy import deepcopy

dir_name = join(here, 'datas')
file_name = join(dir_name, 'importfile.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

gasp_orig = json.loads("""{"name" : "gasp_orig", "version" : "1", "database": "0.91",
 "os" : {"name" : "os_gasp_orig", "version" : "1"},
 "software" : {"name" : "soft_gasp_orig", "version" : "3"},
 "packages" : [{"name": "test"}],
 "categories" : [{"names": [{"lang": "en", "label": "cat_gasp_orig"}],
   "tags": [{"names": [{"lang": "en", "label": "tag_gasp_orig"}],
     "rules": [{"variables": [{"level": "user", "value_on": "a",
                               "value_off": "b", "type": "unicode",
                               "name": "var_gasp_orig", "extension": "xcu",
                               "path": "/u"}],
                "defaultvalue": "",
                "type": "unicode",
                "names": [{"lang": "en","label": "rule_gasp_orig"}]
               }]}]}]}""")
gasp_second = deepcopy(gasp_orig)
gasp_second[u'name'] = u'gasp_second'
gasp_second[u'categories'][0][u'tags'][0][u'rules'][0][u'variables'][0][u'name'] = u'var_gasp_second'

def init_import(with_second=False):
    global confuser, confcomputer, confcontext, grp
    if not isfile(file_name):
        initialize_database(create=True, database=database)
        grp = add_group('grp')
        confuser = get_confuser()
        confcomputer = get_confcomputer()
        confcontext = get_confcontext()
        load(gasp_orig)
        commit_database()
    if with_second:
        try:
            load(gasp_second)
        except AlreadyExistsError:
            pass

def get_informations():
    categories = get_categories()
    tags = Tag.query.all()
    rules = Rule.query.all()
    variables = Variable.query.all()
    platforms = get_platforms()
    return (categories, tags, rules, variables, platforms)

def get_rule(rule='rule_gasp_orig', tag='tag_gasp_orig',
            category='cat_gasp_orig', srule='rule_gasp_orig',
            stag='tag_gasp_orig', scategory='cat_gasp_orig', second_rule=False,
            second_tag=False, second_category=False):
    categories = [get_category(category, 'en')]
    if second_category:
        cat_second = get_category(scategory, 'en')
        categories.insert(0, cat_second)
        category = categories[1]
    else:
        category = categories[0]
    tags = [category.get_tag(tag, 'en')]
    if second_tag:
        tag_second = categories[0].get_tag(stag, 'en')
        tags.insert(0, tag_second)
        tag = tags[1]
    else:
        tag = tags[0]
    rules = [tag.get_rule(rule, 'en')]
    if second_rule:
        rule_second = tags[0].get_rule(srule, 'en')
        rules.insert(0, rule_second)
    return categories, tags, rules

def gen_informations_gasp(variable='var_gasp_orig', rule='rule_gasp_orig',
        srule='rule_gasp_orig', tag='tag_gasp_orig', stag='tag_gasp_orig',
        category='cat_gasp_orig', scategory='cat_gasp_orig', second=False,
        second_rule=False, second_tag=False, second_category=False):
    categories, tags, rules = get_rule(rule=rule, tag=tag, category=category, 
        srule=srule, stag=stag, scategory=scategory, second_rule=second_rule,
        second_tag=second_tag, second_category=second_category)
    from gaspacho.rule import Variable
    if second_rule:
        rule = rules[1]
    else:
        rule = rules[0]
    variables = [rule.get_variable(variable, confuser)]
    if second:
        var_second = rules[0].get_variable('var_gasp_second', confuser)
        variables.insert(0, var_second)
    osv = get_os('os_gasp_orig').get_version('1')
    swv = get_software('soft_gasp_orig').get_version('3')
    path = get_path('/u', 'xcu')
    platforms = [get_platform(path, osv, swv)]
    return (categories, tags, rules, variables, platforms)

def add_choice(rule='rule_gasp_orig', tag='tag_gasp_orig', category='cat_gasp_orig'):
    categories, tags, rules = get_rule(rule, tag, category)
    return rules[0].set_choice(grp, u'on', u'value')

def get_choices():
    return Choice.query.all()

#########################
def test_init_import():
    init_import()
    assert gen_informations_gasp() == get_informations()

def test_reimport_older():
    init_import()
    gasp_older = deepcopy(gasp_orig)
    gasp_older['version'] = '0.9'
    raises(AlreadyExistsError, "load(gasp_older)")
    assert gen_informations_gasp() == get_informations()
    rollback_database()

def test_reimport_same():
    init_import()
    raises(AlreadyExistsError, "load(gasp_orig)")
    assert gen_informations_gasp() == get_informations()
    rollback_database()

def test_reimport_newer():
    init_import()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    load(gasp_newer)
    assert gen_informations_gasp() == get_informations()
    rollback_database()

def test_reimport_newer_two_time():
    init_import()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    load(gasp_newer)
    assert gen_informations_gasp() == get_informations()
    raises(AlreadyExistsError, "load(gasp_newer)")
    rollback_database()

def test_reimport_rename_var():
    init_import()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['rules'][0]['variables'][0]['name'] = 'var_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(variable='var_gasp_new') == get_informations()
    rollback_database()

def test_reimport_rename_rule():
    init_import()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['rules'][0]['names'][0]['label'] = 'rule_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(rule='rule_gasp_new') == get_informations()
    rollback_database()

def test_reimport_rename_tag():
    init_import()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['names'][0]['label'] = 'tag_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(tag='tag_gasp_new') == get_informations()
    rollback_database()

def test_reimport_rename_category():
    init_import()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['names'][0]['label'] = 'cat_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(category='cat_gasp_new') == get_informations()
    rollback_database()

###
def test_reimport_rename_var_with_choice():
    init_import()
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['rules'][0]['variables'][0]['name'] = 'var_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(variable='var_gasp_new') == get_informations()
    assert [choice] == get_choices()
    rollback_database()

def test_reimport_rename_rule_with_choice():
    init_import()
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['rules'][0]['names'][0]['label'] = 'rule_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(rule='rule_gasp_new') == get_informations()
    assert [] == get_choices()
    rollback_database()

def test_reimport_rename_tag_with_choice():
    init_import()
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['names'][0]['label'] = 'tag_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(tag='tag_gasp_new') == get_informations()
    assert [] == get_choices()
    rollback_database()

def test_reimport_rename_category_with_choice():
    init_import()
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['names'][0]['label'] = 'cat_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(category='cat_gasp_new') == get_informations()
    assert [] == get_choices()
    rollback_database()

###
def test_reimport_rename_var_with_choice2():
    init_import(True)
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['rules'][0]['variables'][0]['name'] = 'var_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(variable='var_gasp_new', second=True) == get_informations()
    assert [choice] == get_choices()
    rollback_database()

def test_reimport_rename_rule_with_choice2():
    init_import(True)
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['rules'][0]['names'][0]['label'] \
                = 'rule_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(rule='rule_gasp_new', srule="rule_gasp_orig",
                second=True, second_rule=True) == get_informations()
    assert [choice] == get_choices()
    rollback_database()

def test_reimport_rename_tag_with_choice2():
    init_import(True)
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['tags'][0]['names'][0]['label'] \
                = 'tag_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(tag='tag_gasp_new', second=True,
                second_rule=True, second_tag=True) == get_informations()
    assert [choice] == get_choices()
    rollback_database()

def test_reimport_rename_category_with_choice2():
    init_import(True)
    choice = add_choice()
    assert [choice] == get_choices()
    gasp_newer = deepcopy(gasp_orig)
    gasp_newer['version'] = '1.1'
    gasp_newer['categories'][0]['names'][0]['label'] = 'cat_gasp_new'
    load(gasp_newer)
    assert gen_informations_gasp(category='cat_gasp_new', second=True,
                second_rule=True, second_tag=True, second_category=True) \
                == get_informations()
    assert [choice] == get_choices()
    rollback_database()

#FIXME verifier si Import mis à jour

# vim: ts=4 sw=4 expandtab
