# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
from py.test import raises

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'rule.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import (initialize_database, close_database, commit_database,
                      get_choices, get_user, get_group, get_template,
                      add_template, add_group, add_user, get_os, get_software,
                      get_category)
from gaspacho.category import add_category
from gaspacho.platform import (add_os, add_software, add_path, add_platform,
                               get_path, get_platform)
from gaspacho.rule import DefaultChoice
from gaspacho.choice import Choice
initialize_database(create=True, database=database)
from gaspacho.conflevel import get_confcomputer, get_confuser


def init_choice():
    confuser = get_confuser()
    confcomputer = get_confcomputer()

    #platform
    os1 = add_os('os1')
    os2 = add_os('os2')
    osversion11 = os1.add_version('osversion1')
    osversion21 = os2.add_version('osversion1')
    software1 = add_software('software1')
    software2 = add_software('software2')
    softwareversion11 = software1.add_version('softwareversion1')
    softwareversion21 = software2.add_version('softwareversion1')
    path1 = add_path('path1', 'moz')
    add_platform(path1, osversion11, softwareversion11,
                 [{'name': 'test'}])
    add_platform(path1, osversion11, softwareversion21,
                 [{'name': 'test'}])
    add_platform(path1, osversion21, softwareversion11,
                 [{'name': 'test'}])
    add_platform(path1, osversion21, softwareversion21,
                 [{'name': 'test'}])

    #rules
    category1 = add_category()
    category1.add_translation(lang='en', name='category1')
    tag1 = category1.add_tag()
    tag1.add_translation(lang='en', name='tag1')
    #boolean
    rule = tag1.add_rule(type='boolean')
    rule.add_translation(lang='en', name='boolean')
    rule.add_variable('variable_boolean_boolean', type=u'boolean',
                      value_on='yes', value_off='no', conflevel=confuser)
    rule.add_variable('variable_boolean_unicode', type='unicode',
                      value_on='yes', value_off='no', conflevel=confuser)

    #integer
    rule = tag1.add_rule(type='integer', defaultvalue='1',
                         options={'min': '1', 'max': '2'})
    rule.add_translation(lang='en', name='integer')
    rule.add_variable('variable_integer_unicode', type='unicode',
                      value_off='no', conflevel=confuser)
    rule.add_variable('variable_integer_unicode_value',
                      type='unicode', value_on='yes', value_off='no',
                      conflevel=confuser)
    #enum
    rule = tag1.add_rule(type='enum', defaultvalue='1',
                         options=[['1', 'first option'], ['2', 'second option']])
    rule.add_translation(lang='en', name='enum')
    rule.add_variable('variable_enum_unicode', type='unicode',
                      value_on='yes', value_off='no', conflevel=confuser)
    #list
    rule = tag1.add_rule(type='list', defaultvalue=['1'],
                         options={'separator': ';'})
    rule.add_translation(lang='en', name='list')
    #unicode
    rule = tag1.add_rule(type='unicode', defaultvalue='value')
    rule.add_translation(lang='en', name='unicode')
    rule.add_variable('variable_unicode_unicode', type='unicode',
                      value_on='yes', value_off='no', conflevel=confcomputer)
    #unicode_defaultstate
    rule = tag1.add_rule(type='unicode', defaultvalue='value',
                         defaultstate='off')
    rule.add_translation(lang='en', name='unicode_defaultstate')

    #group
    # grp1
    #  |-- grp11
    #  |     '-- tmpl1
    #  |-- grp12
    #  |     |-- grp121
    #  |     |     '-- grp1211
    #  |     |     '-- tmpl1
    #  |     '-- grp122 (software1)
    #  |     '-- tmpl2 (os1)
    # grp2
    #  |-- grp21
    #  |     '-- grp211
    # grp3 (software1, os1)
    tmpl1 = add_template('tmpl1')
    tmpl2 = add_template('tmpl2')
    tmpl2.add_os(os1)
    tmpl3 = add_template('tmpl3')
    grp1 = add_group('grp1')
    grp11 = add_group('grp11', parent=grp1)
    grp11.add_template(tmpl1)
    grp12 = add_group('grp12', parent=grp1)
    grp12.add_template(tmpl2)
    grp121 = add_group('grp121', parent=grp12)
    grp121.add_template(tmpl1)
    grp122 = add_group('grp122', parent=grp12)
    grp122.add_software(software1)
    add_group('grp1211', parent=grp121)
    grp2 = add_group('grp2')
    grp21 = add_group('grp21', parent=grp2)
    grp211 = add_group('grp211', parent=grp21)
    grp3 = add_group('grp3')
    grp3.add_os(os1)
    grp3.add_software(software1)
    user1 = add_user('user1')
    grp1.add_user(user1)
    grp11.add_user(user1)
    grp121.add_user(user1)
    grp2.add_user(user1)
    grp211.add_user(user1)
    grp12.add_user(user1)
    tmpl2.add_user(user1)
    tmpl3.add_user(user1)

init_choice()

#tester regle pas dans software


def get_platform_test(pathnum, osnum, osversionnum, softwarenum,
                      softwareversionnum):
    path = get_path('path%s' % pathnum, 'moz')
    os = get_os('os%s' % osnum)
    osversion = os.get_version('osversion%s' % osversionnum)
    software = get_software('software%s' % softwarenum)
    softwareversion = software.get_version('softwareversion%s' % softwareversionnum)
    return get_platform(path, osversion, softwareversion)


def get_rule_test(categorynum, tagnum, ruletype):
    category = get_category('category%s' % categorynum, 'en')
    tag = category.get_tag('tag%s' % tagnum, 'en')
    return tag.get_rule(lang='en', name='%s' % ruletype)


def get_variable_test(categorynum, tagnum, ruletype, vartype):
    confuser = get_confuser()
    rule = get_rule_test(categorynum, tagnum, ruletype)
    return rule.get_variable('variable_%s_%s' % (ruletype, vartype), confuser)


def diff_defaultstate(state1, state2):
    if not isinstance(state1, DefaultChoice) or \
            not isinstance(state2, DefaultChoice):
        return False

    return state1.get_state() == state2.get_state() and \
        state1.get_value() == state2.get_value()

##############################################################################
# platform                                                                   #
##############################################################################


def test_add_platform_variable():
    platform11 = get_platform_test('1', '1', '1', '1', '1')
    platform22 = get_platform_test('1', '2', '1', '2', '1')
    variable_boolean_unicode = get_variable_test('1', '1', 'boolean', 'unicode')
    variable_boolean_unicode.add_platform(platform11)
    variable_enum_unicode = get_variable_test('1', '1', 'enum', 'unicode')
    variable_enum_unicode.add_platform(platform22)


def test_get_platforms_variable():
    platform11 = get_platform_test('1', '1', '1', '1', '1')
    platform12 = get_platform_test('1', '1', '1', '2', '1')
    variable_boolean_boolean = get_variable_test('1', '1', 'boolean', 'boolean')
    variable_boolean_unicode = get_variable_test('1', '1', 'boolean', 'unicode')

    assert [] == variable_boolean_boolean.get_platforms()
    assert [platform11] == variable_boolean_unicode.get_platforms()
    variable_boolean_unicode.add_platform(platform12)
    assert set([platform11, platform12]) == \
        set(variable_boolean_unicode.get_platforms())


def test_add_duplicate_platform_variable():
    platform11 = get_platform_test('1', '1', '1', '1', '1')
    platform12 = get_platform_test('1', '1', '1', '2', '1')
    variable_boolean_unicode = get_variable_test('1', '1', 'boolean', 'unicode')
    raises(Exception, "variable_boolean_unicode.add_platform(platform12)")
    assert set([platform11, platform12]) == \
        set(variable_boolean_unicode.get_platforms())


def test_get_platforms_rule():
    platform11 = get_platform_test('1', '1', '1', '1', '1')
    platform12 = get_platform_test('1', '1', '1', '2', '1')
    platform21 = get_platform_test('1', '2', '1', '1', '1')
    variable_boolean_unicode = get_variable_test('1', '1', 'boolean', 'unicode')
    variable_boolean_boolean = get_variable_test('1', '1', 'boolean', 'boolean')
    rule = variable_boolean_unicode.rule
    assert set([platform11, platform12]) == set(rule.get_platforms())
    variable_boolean_boolean.add_platform(platform21)
    assert set([platform11, platform12]) == \
        set(variable_boolean_unicode.get_platforms())
    assert set([platform21]) == set(variable_boolean_boolean.get_platforms())
    assert set([platform11, platform12, platform21]) == \
        set(rule.get_platforms())

##############################################################################
# choice                                                                     #
##############################################################################
#FIXME: user and group not tested in set_choice, not already used


#group
def test_set_choice():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'boolean')
    rule1.set_choice(grp2, 'on')


def test_get_choice():
    grp1 = get_group('grp1')
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'list')
    rule2 = get_rule_test('1', '1', 'unicode')
    choice = rule2.set_choice(grp2, 'on', 'value2')

    assert rule1.get_choice(grp2) == (None, None)
    assert rule2.get_choice(grp2) == (choice, None)
    assert rule2.get_choice(grp1) == (None, None)


def test_get_choices():
    grp2 = get_group('grp2')
    rule2 = get_rule_test('1', '1', 'unicode')
    #rule2.get_choice(grp2)[0]
    oldchoice = rule2.get_choice(grp2)[0]
    oldchoicedefault = DefaultChoice('unicode', 'off', 'value', None)
    for rule, choices in get_choices(grp2).items():
        if not isinstance(choices[0], DefaultChoice) or \
                not isinstance(oldchoicedefault, DefaultChoice):
            ret = False
        else:
            ret = not diff_defaultstate(choices[0], oldchoicedefault)
        if choices[0] != oldchoice and ret:
            raise Exception('error in get_choices')


def test_set_choice_value_choice_boolean():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'boolean')

    raises(Exception, "rule1.set_choice(grp2, 'on', 'value')")
    rule1.set_choice(grp2, 'on')


def test_set_choice_value_choice_unicode():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'unicode')

    raises(Exception, "rule1.set_choice(grp2, 'on')")
    rule1.set_choice(grp2, 'on', 'value')


def test_set_choice_value_choice_integer():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'integer')

    raises(Exception, "rule1.set_choice(grp2, 'on', 'value')")
    rule1.set_choice(grp2, 'on', '1')
    rule1.set_choice(grp2, 'on', 1)


def test_set_choice_value_choice_enum():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'enum')

    raises(Exception, "rule1.set_choice(grp2, 'on', 'value')")
    rule1.set_choice(grp2, 'on', '1')
    raises(Exception, "rule1.set_choice(grp2, 'on', 1)")


def test_set_choice_value_choice_list():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'list')

    raises(Exception, "rule1.set_choice(grp2, 'on', '1')")
    raises(Exception, "rule1.set_choice(grp2, 'on', 'value')")
    raises(Exception, "rule1.set_choice(grp2, 'on', 1)")
    raises(Exception, "rule1.set_choice(grp2, 'on', ['ah', 'bah', 'error;')")
    raises(Exception, "rule1.set_choice(grp2, 'on', ['ah', 1])")
    rule1.set_choice(grp2, 'on', ['ah', 'bah'])
    rule1.set_choice(grp2, 'on', ['value'])


def test_set_choice_value_boolean():
    grp2 = get_group('grp2')
    rule2 = get_rule_test('1', '1', 'boolean')

    choice, choiceherited = rule2.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    assert choice.get_value() is None


def test_get_value_choice_unicode():
    grp2 = get_group('grp2')
    rule2 = get_rule_test('1', '1', 'unicode')
    choice, choiceherited = rule2.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    assert choice.get_value() == u'value'


def test_get_value_choice_enum():
    grp2 = get_group('grp2')
    rule2 = get_rule_test('1', '1', 'enum')
    choice, choiceherited = rule2.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    assert choice.get_value() == u'1'


def test_get_value_choice_integer():
    grp2 = get_group('grp2')
    rule2 = get_rule_test('1', '1', 'integer')
    choice, choiceherited = rule2.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    assert choice.get_value() == 1


def test_get_value_choice_list():
    grp2 = get_group('grp2')
    rule2 = get_rule_test('1', '1', 'list')
    choice, choiceherited = rule2.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    assert choice.get_value() == ['value']


def test_get_state_choice():
    grp2 = get_group('grp2')
    rule2 = get_rule_test('1', '1', 'unicode')
    choice, choiceherited = rule2.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    assert choice.get_state() == u'on'


def test_del_choice():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'unicode')
    rule2 = get_rule_test('1', '1', 'enum')
    choice, choiceherited = rule1.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    rule1.del_choice(grp2)
    assert rule1.get_choice(grp2) == (None, None)
    choice, choiceherited = rule2.get_choice(grp2)
    assert choice is not None
    assert choiceherited is None
    rule2.del_choice(grp2)
    assert rule2.get_choice(grp2) == (None, None)


def test_set_herited_choice():
    grp1 = get_group('grp1')
    grp12 = get_group('grp12', parent=grp1)
    grp121 = get_group('grp121', parent=grp12)
    rule1 = get_rule_test('1', '1', 'unicode')

    choice1 = rule1.set_choice(grp121, 'on', 'value')
    assert rule1.get_choice(grp1) == (None, None)
    assert rule1.get_choice(grp12) == (None, None)
    assert rule1.get_choice(grp121) == (choice1, None)

    choice2 = rule1.set_choice(grp1, 'on', 'value')
    assert rule1.get_choice(grp1) == (choice2, None)
    assert rule1.get_choice(grp12) == (None, choice2)
    assert rule1.get_choice(grp121) == (choice1, choice2)

    choice3 = rule1.set_choice(grp12, 'on', 'value')
    assert rule1.get_choice(grp1) == (choice2, None)
    assert rule1.get_choice(grp12) == (choice3, choice2)
    assert rule1.get_choice(grp121) == (choice1, choice3)


def test_del_herited_choice():
    grp1 = get_group('grp1')
    grp12 = get_group('grp12', parent=grp1)
    grp121 = get_group('grp121', parent=grp12)
    rule1 = get_rule_test('1', '1', 'unicode')

    choice1 = rule1.get_choice(grp121)[0]
    choice2 = rule1.get_choice(grp1)[0]
    choice3 = rule1.get_choice(grp12)[0]
    assert rule1.get_choice(grp1) == (choice2, None)
    assert rule1.get_choice(grp12) == (choice3, choice2)
    assert rule1.get_choice(grp121) == (choice1, choice3)
    rule1.del_choice(grp12)
    assert rule1.get_choice(grp1) == (choice2, None)
    assert rule1.get_choice(grp12) == (None, choice2)
    assert rule1.get_choice(grp121) == (choice1, choice2)
    rule1.del_choice(grp1)
    assert rule1.get_choice(grp1) == (None, None)
    assert rule1.get_choice(grp12) == (None, None)
    assert rule1.get_choice(grp121) == (choice1, None)
    rule1.del_choice(grp121)
    assert rule1.get_choice(grp1) == (None, None)
    assert rule1.get_choice(grp12) == (None, None)
    assert rule1.get_choice(grp121) == (None, None)


def test_set_choice_defaultvalue():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'unicode_defaultstate')

    choice1, choiceherited1 = rule1.get_choice(grp2)
    assert choice1 is None
    assert choiceherited1 is not None
    assert choiceherited1.get_state() == u'off'
    assert choiceherited1.get_value() == u'value'

    choice2 = rule1.set_choice(grp2, 'on', 'value')
    choice3, choiceherited3 = rule1.get_choice(grp2)
    assert choice2 == choice3
    assert diff_defaultstate(choiceherited1, choiceherited3)


def test_set_herited_choice_defaultvalue():
    grp1 = get_group('grp1')
    grp12 = get_group('grp12', parent=grp1)
    grp121 = get_group('grp121', parent=grp12)
    rule1 = get_rule_test('1', '1', 'unicode_defaultstate')

    choice, defaultchoice = rule1.get_choice(grp1)
    assert choice is None
    assert defaultchoice.get_state() == u'off'
    assert defaultchoice.get_value() == u'value'
    choice1, defaultchoice1 = rule1.get_choice(grp12)
    assert choice1 is None
    assert diff_defaultstate(defaultchoice, defaultchoice)
    choice1, defaultchoice1 = rule1.get_choice(grp121)
    assert choice1 is None
    assert diff_defaultstate(defaultchoice, defaultchoice)


#template
def test_add_choice_template():
    tmpl1 = get_template('tmpl1')
    rule1 = get_rule_test('1', '1', 'list')
    rule1.set_choice(tmpl1, 'on', ['value'])


def test_get_choice_template():
    tmpl1 = get_template('tmpl1')
    tmpl2 = get_template('tmpl2')
    rule1 = get_rule_test('1', '1', 'list')

    assert rule1.get_choice(tmpl2) == (None, None)

    choice, choiceherited = rule1.get_choice(tmpl1)
    assert choice.get_state() == u'on'
    assert choice.get_value() == ['value']
    assert choiceherited is None


def test_herited_choice_template():
    tmpl1 = get_template('tmpl1')
    grp1 = get_group('grp1')
    grp11 = get_group('grp11', parent=grp1)
    grp12 = get_group('grp12', parent=grp1)
    grp121 = get_group('grp121', parent=grp12)
    rule1 = get_rule_test('1', '1', 'list')

    choice1, choiceherited1 = rule1.get_choice(tmpl1)
    assert (None, None) == rule1.get_choice(grp1)
    assert (None, choice1) == rule1.get_choice(grp11)

    assert (None, None) == rule1.get_choice(grp12)
    assert (None, choice1) == rule1.get_choice(grp121)


def test_del_choice_template():
    tmpl1 = get_template('tmpl1')
    rule1 = get_rule_test('1', '1', 'list')

    choice, choiceherited = rule1.get_choice(tmpl1)
    assert choice is not None
    assert choiceherited is None
    rule1.del_choice(tmpl1)
    assert rule1.get_choice(tmpl1) == (None, None)


#user
def test_add_choice_group_user():
    for choice in Choice.query.all():
        choice.delete()
    grp2 = get_group('grp2')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')
    choice = rule1.set_choice(grp2, user=user1, state='on', value='1')


def test_add_choice_group_user_not_linked():
    grp3 = get_group('grp3')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')
    raises(Exception, "rule1.set_choice(grp3, user=user1, state='on', value='1')")


def test_add_choice_template_user():
    tmpl2 = get_template('tmpl2')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')
    rule1.set_choice(tmpl2, user=user1, state='on', value='2')


def test_add_choice_template_user_not_linked():
    tmpl1 = get_template('tmpl1')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')
    raises(Exception, "rule1.set_choice(tmpl1, user=user1, state='on', value='2')")


def test_add_choice_nousuer_group_user():
    grp2 = get_group('grp2')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'unicode')
    rule2 = get_rule_test('1', '1', 'unicode_defaultstate')
    raises(Exception,
           "rule1.set_choice(grp2, user=user1, state='on', value='2')")
    raises(Exception,
           "rule2.set_choice(grp2, user=user1, state='on', value='2')")


def test_add_choice_nousuer_template_user():
    tmpl2 = get_template('tmpl2')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'unicode')
    rule2 = get_rule_test('1', '1', 'unicode_defaultstate')
    raises(Exception,
           "rule1.set_choice(tmpl2, user=user1, state='on', value='2')")
    raises(Exception,
           "rule2.set_choice(tmpl2, user=user1, state='on', value='2')")


def test_get_choice_group_user():
    grp2 = get_group('grp2')
    grp1 = get_group('grp1')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')
    assert rule1.get_choice(grp1, user1) == (None, None)

    choice, choiceherited = rule1.get_choice(grp2, user1)
    assert choice.get_state() == u'on'
    assert choice.get_value() == u'1'
    assert choiceherited is None


def test_get_choice_group_user_2():
    grp3 = get_group('grp3')
    user1 = get_user('user1')
    raises(Exception, "assert rule1.get_choice(grp3, user1) == (None, None)")


def test_get_choice_template_user():
    tmpl2 = get_template('tmpl2')
    tmpl3 = get_template('tmpl3')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')

    assert rule1.get_choice(tmpl3, user1) == (None, None)

    choice, choiceherited = rule1.get_choice(tmpl2, user1)
    assert choice.get_state() == u'on'
    assert choice.get_value() == u'2'
    assert choiceherited is None


def test_herited_choice_user():
    tmpl2 = get_template('tmpl2')
    grp1 = get_group('grp1')
    grp11 = get_group('grp11', parent=grp1)
    grp12 = get_group('grp12', parent=grp1)
    grp121 = get_group('grp121', parent=grp12)
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')

    choice1, choiceherited1 = rule1.get_choice(tmpl2, user1)
    assert (None, None) == rule1.get_choice(tmpl2)
    assert (choice1, None) == rule1.get_choice(tmpl2, user1)
    assert (None, None) == rule1.get_choice(grp1)
    assert (None, None) == rule1.get_choice(grp1, user1)
    assert (None, None) == rule1.get_choice(grp11)
    assert (None, None) == rule1.get_choice(grp11, user1)
    assert (None, None) == rule1.get_choice(grp12)
    assert (None, choice1) == rule1.get_choice(grp12, user1)
    assert (None, None) == rule1.get_choice(grp121)
    assert (None, choice1) == rule1.get_choice(grp121, user1)

    choice2 = rule1.set_choice(grp12, user=user1, state='on', value='1')
    assert (None, None) == rule1.get_choice(tmpl2)
    assert (choice1, None) == rule1.get_choice(tmpl2, user1)
    assert (None, None) == rule1.get_choice(grp1)
    assert (None, None) == rule1.get_choice(grp1, user1)
    assert (None, None) == rule1.get_choice(grp11)
    assert (None, None) == rule1.get_choice(grp11, user1)
    assert (None, None) == rule1.get_choice(grp12)
    assert (choice2, choice1) == rule1.get_choice(grp12, user1)
    assert (None, None) == rule1.get_choice(grp121)
    assert (None, choice2) == rule1.get_choice(grp121, user1)

    choice3 = rule1.set_choice(grp1, user=user1, state='on', value='1')
    assert (None, None) == rule1.get_choice(tmpl2)
    assert (choice1, None) == rule1.get_choice(tmpl2, user1)
    assert (None, None) == rule1.get_choice(grp1)
    assert (choice3, None) == rule1.get_choice(grp1, user1)
    assert (None, None) == rule1.get_choice(grp11)
    assert (None, choice3) == rule1.get_choice(grp11, user1)
    assert (None, None) == rule1.get_choice(grp12)
    #assert (choice2, choice1) == rule1.get_choice(grp12, user1)
    assert (choice2, choice1) == rule1.get_choice(grp12, user1)
    assert (None, None) == rule1.get_choice(grp121)
    assert (None, choice2) == rule1.get_choice(grp121, user1)

    choice4 = rule1.set_choice(grp1, state='on', value='1')
    assert (None, None) == rule1.get_choice(tmpl2)
    assert (choice1, None) == rule1.get_choice(tmpl2, user1)
    assert (choice4, None) == rule1.get_choice(grp1)
    assert (choice3, choice4) == rule1.get_choice(grp1, user1)
    assert (None, choice4) == rule1.get_choice(grp11)
    assert (None, choice3) == rule1.get_choice(grp11, user1)
    assert (None, choice4) == rule1.get_choice(grp12)
    assert (choice2, choice1) == rule1.get_choice(grp12, user1)
    assert (None, choice4) == rule1.get_choice(grp121)
    assert (None, choice2) == rule1.get_choice(grp121, user1)

    choice5 = rule1.set_choice(tmpl2, state='on', value='1')
    assert (choice5, None) == rule1.get_choice(tmpl2)
    assert (choice1, choice5) == rule1.get_choice(tmpl2, user1)
    assert (choice4, None) == rule1.get_choice(grp1)
    assert (choice3, choice4) == rule1.get_choice(grp1, user1)
    assert (None, choice4) == rule1.get_choice(grp11)
    assert (None, choice3) == rule1.get_choice(grp11, user1)
    assert (None, choice5) == rule1.get_choice(grp12)
    assert (choice2, choice1) == rule1.get_choice(grp12, user1)
    assert (None, choice5) == rule1.get_choice(grp121)
    assert (None, choice2) == rule1.get_choice(grp121, user1)


def test_del_choice_group_user():
    grp2 = get_group('grp2')
    rule1 = get_rule_test('1', '1', 'enum')
    user1 = get_user('user1')

    choice, choiceherited = rule1.get_choice(grp2, user1)
    assert choice is not None
    rule1.del_choice(grp2, user1)
    assert rule1.get_choice(grp2, user1) == (None, choiceherited)


def test_del_choice_template_user():
    tmpl2 = get_template('tmpl2')
    rule1 = get_rule_test('1', '1', 'enum')
    user1 = get_user('user1')

    choice, choiceherited = rule1.get_choice(tmpl2, user1)
    assert choice is not None
    rule1.del_choice(tmpl2, user1)
    assert rule1.get_choice(tmpl2, user1) == (None, choiceherited)


#software/os
def test_set_choice_software_link():
    """
    grp122 is link to platform11 but rule1 is link to platform11 too
    can set choice for this rule
    """
    grp1 = get_group('grp1')
    grp12 = get_group('grp12', parent=grp1)
    grp122 = get_group('grp122', parent=grp12)
    rule1 = get_rule_test('1', '1', 'boolean')

    rule1.set_choice(grp122, 'on')


def test_set_choice_software_not_link():
    """
    grp122 is link to platform11 but rule1 is link to platform22
    can't set choice for this rule
    """
    grp1 = get_group('grp1')
    grp12 = get_group('grp12', parent=grp1)
    grp122 = get_group('grp122', parent=grp12)
    rule1 = get_rule_test('1', '1', 'enum')
    raises(Exception, "rule1.set_choice(grp122, 'on', value='1')")


def test_get_choices_herite():
    grp2 = get_group('grp2')
    grp21 = get_group('grp21', parent=grp2)
    grp211 = get_group('grp211', parent=grp21)
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')
    tchoice = rule1.set_choice(grp2, user=user1, state='on', value='1')
    choice, choiceherited = rule1.get_choice(grp211, user1)
    assert choiceherited == tchoice


def test_get_choices_heritage():
    grp2 = get_group('grp2')
    user1 = get_user('user1')
    rule1 = get_rule_test('1', '1', 'enum')
    rule2 = get_rule_test('1', '1', 'boolean')
    tchoice = rule1.set_choice(grp2, user=user1, state='on', value='1')
    choice, choiceherited = get_choices(grp2, user1, rule1)
    assert choice == tchoice
    assert choiceherited is None
    choice = get_choices(grp2, user1, rule1, heritage=False)
    assert choice == tchoice
    choice = get_choices(grp2, rule=rule1, heritage=False)
    assert choice is None
    choice = get_choices(grp2, rule=rule2, heritage=False)
    assert choice is None
    choice = get_choices(grp2, user1, rule=rule2, heritage=False)
    assert choice is None
    tchoice2 = rule2.set_choice(grp2, state='on')
    choice = get_choices(grp2, rule=rule2, heritage=False)
    assert choice == tchoice2
    choice = get_choices(grp2, user1, rule=rule2, heritage=False)
    assert choice is None

#FIXME: tester software1 et os1


def test_close_database():
    commit_database()
    close_database()

# vim: ts=4 sw=4 expandtab
