# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
from py.test import raises
from sys import exit

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'group.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import *
from gaspacho.group import Group, Template, User
from gaspacho.platform import add_os, add_software
initialize_database(create=True, database=database)

try:
    os1 = add_os('os1')
    os2 = add_os('os2')
except:
    print "unable to add new OS, please test add_os function"
    exit(1)

try:
    soft1 = add_software('soft1')
    soft2 = add_software('soft2')
except:
    print "unable to add new Software, please test add_software function"
    exit(1)

##############################################################################
# group                                                                      #
##############################################################################

#group without heritage
def test_create_group():
    """
    create a new group and test it
    """
    assert [] == get_groups()
    grp1 = add_group('grp1')
    assert [grp1] == get_groups()
    assert 'grp1' == grp1.get_name()

def test_get_group():
    """
    get a group by name
    """
    grp1 = get_group('grp1')
    assert 'grp1' == grp1.get_name()
    assert isinstance(grp1, Group) == True

def test_get_unknown_group():
    """
    try to get an unknown template
    """
    grpunset = get_group('grpunset')
    assert grpunset == None

def test_group_exists():
    """
    try to add a group with same name
    """
    raises(Exception, "add_group('grp1')")

def test_delete_group():
    """
    delete the template
    """
    del_group(get_group('grp1'))
    assert [] == get_groups()

#herited group
def test_create_herited_group():
    grp2 = add_group('grp2')
    grp3 = add_group('grp3', parent=grp2)
    assert [grp2] == get_groups()
    assert [grp3] == get_groups(parent=grp2)
    assert set([grp2, grp3]) == set(get_all_groups())

def test_get_herited_group():
    grp2 = get_group('grp2')
    grp3 = get_group('grp3')
    #try to get without heritage
    assert grp3 == None
    grp3 = get_group('grp3', parent=grp2)
    #try to get with heritage
    assert grp3.get_name() == 'grp3'
    #try to get with heritage when should not be
    grp2 = get_group('grp2', parent=grp3)
    assert grp2 == None

def test_herited_group_exists():
    grp2 = get_group('grp2')
    raises(Exception, "add_group('grp3', parent=grp2)")
    grp3 = get_group('grp3', parent=grp2)
    #same name but in herited group
    grp2_2 = add_group('grp2', parent=grp2)
    assert set([grp2, grp3, grp2_2]) == set(get_all_groups())

def test_delete_herited_group():
    grp2 = get_group('grp2')
    grp2_2 = get_group('grp2', parent=grp2)
    grp3 = get_group('grp3', parent=grp2)
    assert set([grp2, grp3, grp2_2]) == set(get_all_groups())
    assert set([grp2_2, grp3]) == set(get_groups(parent=grp2))
    assert [grp2] == get_groups()
    del_group(grp2_2)
    assert set([grp2, grp3]) == set(get_all_groups())
    assert [grp3] == get_groups(parent=grp2)
    assert [grp2] == get_groups()
    
def test_delete_group_with_herited_group():
    grp2 = get_group('grp2')
    grp3 = get_group('grp3', parent=grp2)
    assert set([grp2, grp3]) == set(get_all_groups())
    del_group(grp2)
    assert [] == get_all_groups()

#name
def test_rename_group():
    grp3 = add_group('grp3')
    grp4 = add_group('grp4')
    grp4.mod_name('grp4_new')
    assert grp4.get_name() == 'grp4_new'
    raises(Exception, "grp4.mod_name('grp3')")

#comment
def test_create_comment_group():
    grp5 = add_group('grp5', comment='add comment 5')
    assert grp5.get_comment() == 'add comment 5'

def test_mod_comment_group():
    grp4 = get_group('grp4_new')
    assert grp4.get_comment() == u''
    grp4.mod_comment('new comment 4')
    assert grp4.get_comment() == 'new comment 4'
    grp5 = get_group('grp5')
    assert grp5.get_comment() == 'add comment 5'
    grp5.mod_comment('new comment 5')
    assert grp5.get_comment() == 'new comment 5'

#computer
def test_add_computer():
    """
    add a computer, add a dns computer and add a already exists computer
    """
    grp5 = get_group('grp5')
    grp5.add_computer('192.168.1.1')
    assert [(u'192.168.1.1', u'ip')] == grp5.get_computers()
    grp5.add_computer('computer', 'dns')
    assert [(u'192.168.1.1', u'ip'), (u'computer', u'dns')] == grp5.get_computers()
    raises(Exception, "grp5.add_computer('computer', u'dns')")

def test_remove_computer():
    """
    del computer
    """
    grp5 = get_group('grp5')
    grp5.del_computer('computer', u'dns')
    assert [(u'192.168.1.1', u'ip')] == grp5.get_computers()
    raises(Exception, "grp5.del_computer('new')")

#FIXME: supprime suivant le type
#FIXME: tester is_computer

##############################################################################
# template                                                                   #
##############################################################################

#template
def test_create_template():
    """
    create a new template and test it
    """
    assert [] == get_templates()
    tmpl1 = add_template('tmpl1')    
    assert [tmpl1] == get_templates()
    assert 'tmpl1' == tmpl1.get_name()

def test_get_template():
    """
    get a template by name
    """
    tmpl1 = get_template('tmpl1')
    assert 'tmpl1' == tmpl1.get_name()
    assert isinstance(tmpl1, Template) == True

def test_get_unknown_template():
    """
    try to get an unknown template
    """
    tmplunset = get_template('tmplunset')
    assert tmplunset == None

def test_template_exists():
    """
    try to add a group with same name
    """
    raises(Exception, "add_template('tmpl1')")

def test_delete_template():
    """
    delete the template
    """
    del_template(get_template('tmpl1'))
    assert [] == get_templates()

def test_create_two_templates():
    """
    try to add two templates
    """
    assert [] == get_templates()
    tmpl2 = add_template('tmpl2')
    tmpl3 = add_template('tmpl3')
    assert set([tmpl2, tmpl3]) == set(get_templates())
    
def test_delete_two_templates():
    """
    del two templates
    """
    tmpl2 = get_template('tmpl2')
    tmpl3 = get_template('tmpl3')
    assert set([tmpl2, tmpl3]) == set(get_templates())
    del_template(tmpl2)
    assert [tmpl3] == get_templates()
    del_template(tmpl3)
    assert [] == get_templates()

#template with group
def test_add_template_group():
    """
    link templates to a group
    """
    grp5 = get_group('grp5')
    tmpl4 = add_template('tmpl4')
    tmpl5 = add_template('tmpl5')
    assert [] == grp5.get_templates()
    grp5.add_template(tmpl4)
    assert [tmpl4] == grp5.get_templates()
    grp5.add_template(tmpl5)
    assert set([tmpl4, tmpl5]) == set(grp5.get_templates())

def test_remove_template_group():
    """
    remove link to a group
    """
    grp5 = get_group('grp5')
    tmpl4 = get_template('tmpl4')
    tmpl5 = get_template('tmpl5')
    assert set([tmpl4, tmpl5]) == set(grp5.get_templates())
    grp5.del_template(tmpl5)
    assert [tmpl4] == grp5.get_templates()
    grp5.del_template(tmpl4)
    assert [] == grp5.get_templates()

def test_remove_template_in_group():
    """
    try to remove a template link to a group
    """
    grp5 = get_group('grp5')
    tmpl4 = get_template('tmpl4')
    grp5.add_template(tmpl4)
    raises(Exception, "del_template(tmpl4)")

def test_remove_unlink_group():
    """
    try to remove an unlink template
    """
    grp5 = get_group('grp5')
    tmpl4 = get_template('tmpl4')
    tmpl5 = get_template('tmpl5')
    assert [tmpl4] == grp5.get_templates()
    raises(Exception, "grp5.del_template(tmpl5)")
    del_template(tmpl5)

def test_link_wrong_template_group():
    """
    try to link a group instead of a template
    """
    grp5 = get_group('grp5')
    grp6 = add_group('grp6')
    raises(Exception, "grp5.add_template(grp6)")

#name
def test_rename_template():
    tmpl1 = add_template('tmpl1')
    tmpl4 = get_template('tmpl4')
    tmpl4.mod_name('tmpl4_new')
    assert tmpl4.get_name() == 'tmpl4_new'
    raises(Exception, "tmpl4.mod_name('tmpl1')")

#comment
def test_create_comment_template():
    tmpl5 = add_template('tmpl5', comment='add comment 5')
    assert tmpl5.get_comment() == 'add comment 5'

def test_mod_comment_template():
    tmpl4 = get_template('tmpl4_new')
    assert tmpl4.get_comment() == u''
    tmpl4.mod_comment('new comment 4')
    assert tmpl4.get_comment() == 'new comment 4'
    tmpl5 = get_template('tmpl5')
    assert tmpl5.get_comment() == 'add comment 5'
    tmpl5.mod_comment('new comment 5')
    assert tmpl5.get_comment() == 'new comment 5'

##############################################################################
# user                                                                       #
##############################################################################

def test_create_user():
    """
    add new user
    """
    assert [] == get_users()
    user1 = add_user('user1')
    assert [user1] == get_users()
    assert 'user1' == user1.get_name()
    assert user1.type == u'user'
    user2 = add_user('user2', type='usergroup')
    assert set([user1, user2]) == set(get_users())
    assert 'user2' == user2.get_name()
    assert user2.type == u'usergroup'

def test_get_user():
    """
    get an user by name
    """
    user1 = get_user('user1')
    assert 'user1' == user1.get_name()
    assert isinstance(user1, User) == True

def test_get_users():
    """
    get two different users
    """
    user1 = get_user('user1')
    user2 = get_user('user2', type='usergroup')
    assert set([user1, user2]) == set(get_users())

def test_get_unknown_user():
    """
    try to get an unknown user
    """
    tmplunknown = get_user('tmplunknown')
    assert tmplunknown == None

def test_create_existing_user():
    """
    try to add a new user with same name
    """
    raises(Exception, "add_user('user1')")

def test_del_user():
    """
    remove an existing user
    """
    user1 = get_user('user1')
    user2 = get_user('user2', type='usergroup')
    assert set([user1, user2]) == set(get_users())
    del_user(user1)
    assert [user2] == get_users()

#user with group
def test_add_user_group():
    """
    link users to a group
    """
    grp5 = get_group('grp5')
    user3 = add_user('user3')
    user4 = add_user('user4')
    assert [] == grp5.get_users()
    grp5.add_user(user3)
    assert [user3] == grp5.get_users()
    grp5.add_user(user4)
    assert set([user3, user4]) == set(grp5.get_users())

def test_remove_user_group():
    """
    remove link to a group
    """
    grp5 = get_group('grp5')
    user3 = get_user('user3')
    user4 = get_user('user4')
    assert set([user3, user4]) == set(grp5.get_users())
    grp5.del_user(user4)
    assert [user3] == grp5.get_users()
    grp5.del_user(user3)
    assert [] == grp5.get_users()

def test_remove_user_in_group():
    """
    try to remove a user link to a group
    """
    grp5 = get_group('grp5')
    user3 = get_user('user3')
    grp5.add_user(user3)
    raises(Exception, "del_user(user3)")

def test_remove_user_unlink_group():
    """
    try to remove an unlink user
    """
    grp5 = get_group('grp5')
    user3 = get_user('user3')
    user4 = get_user('user4')
    assert [user3] == grp5.get_users()
    raises(Exception, "grp5.del_user(user4)")

def test_link_wrong_user_group():
    """
    try to link a group instead of a user
    """
    grp5 = get_group('grp5')
    grp6 = get_group('grp6')
    raises(Exception, "grp5.add_user(grp6)")

#name
def test_rename_user():
    user4 = get_user('user4')
    user4.mod_name('user4_new')
    assert user4.get_name() == 'user4_new'
    raises(Exception, "user4.mod_name('user3')")
    user4.mod_name('user4')

#comment
def test_create_comment_user():
    user5 = add_user('user5', comment='add comment 5')
    assert user5.get_comment() == 'add comment 5'

def test_mod_comment_user():
    user4 = get_user('user4')
    assert user4.get_comment() == u''
    user4.mod_comment('new comment 4')
    assert user4.get_comment() == 'new comment 4'
    user5 = get_user('user5')
    assert user5.get_comment() == 'add comment 5'
    user5.mod_comment('new comment 5')
    assert user5.get_comment() == 'new comment 5'

##############################################################################
# manager                                                                    #
##############################################################################

def test_add_manager_group():
    """
    link managers to a group
    """
    grp5 = get_group('grp5')
    user3 = get_user('user3')
    user4 = get_user('user4')
    assert [] == grp5.get_managers()
    grp5.add_manager(user3)
    assert [user3] == grp5.get_managers()
    grp5.add_manager(user4)
    assert set([user3, user4]) == set(grp5.get_managers())

def test_remove_manager_group():
    """
    remove link to a group
    """
    grp5 = get_group('grp5')
    user3 = get_user('user3')
    user4 = get_user('user4')
    assert set([user3, user4]) == set(grp5.get_managers())
    grp5.del_manager(user4)
    assert [user3] == grp5.get_managers()
    grp5.del_manager(user3)
    assert [] == grp5.get_managers()

def test_remove_manager_in_group():
    """
    try to remove a manager link to a group
    """
    grp5 = get_group('grp5')
    user3 = get_user('user3')
    grp5.add_manager(user3)
    raises(Exception, "del_user(user3)")

def test_remove_manager_unlink_group():
    """
    try to remove an unlink manager
    """
    grp5 = get_group('grp5')
    user3 = get_user('user3')
    user4 = get_user('user4')
    assert [user3] == grp5.get_managers()
    raises(Exception, "grp5.del_manager(user4)")

def test_link_wrong_manager_group():
    """
    try to link a group instead of a manager
    """
    grp5 = get_group('grp5')
    grp6 = get_group('grp6')
    raises(Exception, "grp5.add_manager(grp6)")

##############################################################################
# software                                                                   #
##############################################################################
def test_add_software_group():
    """
    link softwares to a group
    """
    grp5 = get_group('grp5')
    assert [] == grp5.get_softwares()
    grp5.add_software(soft1)
    assert [soft1] == grp5.get_softwares()
    grp5.add_software(soft2)
    assert set([soft1, soft2]) == set(grp5.get_softwares())

def test_remove_software_group():
    """
    remove link to a group
    """
    grp5 = get_group('grp5')
    assert set([soft1, soft2]) == set(grp5.get_softwares())
    grp5.del_software(soft2)
    assert [soft1] == grp5.get_softwares()
    grp5.del_software(soft1)
    assert [] == grp5.get_softwares()

def test_remove_softwares_group():
    """
    remove all link to a group
    """
    grp5 = get_group('grp5')
    grp5.add_software(soft1)
    grp5.add_software(soft2)
    assert set([soft1, soft2]) == set(grp5.get_softwares())
    grp5.del_softwares()
    assert [] == grp5.get_softwares()

def test_remove_software_unlink_group():
    """
    try to remove an unlink software
    """
    grp5 = get_group('grp5')
    grp5.add_software(soft1)
    assert [soft1] == grp5.get_softwares()
    raises(Exception, "grp5.del_software(soft2)")

def test_link_wrong_software_group():
    """
    try to link an os instead of a software
    """
    grp5 = get_group('grp5')
    raises(Exception, "grp5.add_software(os1)")

#software in template
def test_add_software_template():
    """
    link softwares to a template
    """
    tmpl5 = get_template('tmpl5')
    assert [] == tmpl5.get_softwares()
    tmpl5.add_software(soft1)
    assert [soft1] == tmpl5.get_softwares()
    tmpl5.add_software(soft2)
    assert set([soft1, soft2]) == set(tmpl5.get_softwares())

def test_remove_software_template():
    """
    remove link to a template
    """
    tmpl5 = get_template('tmpl5')
    assert set([soft1, soft2]) == set(tmpl5.get_softwares())
    tmpl5.del_software(soft2)
    assert [soft1] == tmpl5.get_softwares()
    tmpl5.del_software(soft1)
    assert [] == tmpl5.get_softwares()

def test_remove_softwares_template():
    """
    remove all link to a template
    """
    tmpl5 = get_template('tmpl5')
    tmpl5.add_software(soft1)
    tmpl5.add_software(soft2)
    assert set([soft1, soft2]) == set(tmpl5.get_softwares())
    tmpl5.del_softwares()
    assert [] == tmpl5.get_softwares()

def test_remove_software_unlink_template():
    """
    try to remove an unlink software
    """
    tmpl5 = get_template('tmpl5')
    tmpl5.add_software(soft1)
    assert [soft1] == tmpl5.get_softwares()
    raises(Exception, "tmpl5.del_software(soft2)")

def test_link_wrong_software_template():
    """
    try to link an os instead of a software
    """
    tmpl5 = get_template('tmpl5')
    raises(Exception, "tmpl5.add_software(os1)")

##############################################################################
# os                                                                         #
##############################################################################

#os in group
def test_add_os_group():
    """
    link oses to a group
    """
    grp5 = get_group('grp5')
    assert [] == grp5.get_oses()
    grp5.add_os(os1)
    assert [os1] == grp5.get_oses()
    grp5.add_os(os2)
    assert set([os1, os2]) == set(grp5.get_oses())

def test_remove_os_group():
    """
    remove link to a group
    """
    grp5 = get_group('grp5')
    assert set([os1, os2]) == set(grp5.get_oses())
    grp5.del_os(os2)
    assert [os1] == grp5.get_oses()
    grp5.del_os(os1)
    assert [] == grp5.get_oses()

def test_remove_oses_group():
    """
    remove all link to a group
    """
    grp5 = get_group('grp5')
    grp5.add_os(os1)
    grp5.add_os(os2)
    assert set([os1, os2]) == set(grp5.get_oses())
    grp5.del_oses()
    assert [] == grp5.get_oses()

def test_remove_os_unlink_group():
    """
    try to remove an unlink os
    """
    grp5 = get_group('grp5')
    grp5.add_os(os1)
    assert [os1] == grp5.get_oses()
    raises(Exception, "grp5.del_os(os2)")

def test_link_wrong_os_group():
    """
    try to link an os instead of a 
    """
    grp5 = get_group('grp5')
    raises(Exception, "grp5.add_os(soft1)")

#os in template
def test_add_os_template():
    """
    link oses to a template
    """
    tmpl5 = get_template('tmpl5')
    assert [] == tmpl5.get_oses()
    tmpl5.add_os(os1)
    assert [os1] == tmpl5.get_oses()
    tmpl5.add_os(os2)
    assert set([os1, os2]) == set(tmpl5.get_oses())

def test_remove_os_template():
    """
    remove link to a template
    """
    tmpl5 = get_template('tmpl5')
    assert set([os1, os2]) == set(tmpl5.get_oses())
    tmpl5.del_os(os2)
    assert [os1] == tmpl5.get_oses()
    tmpl5.del_os(os1)
    assert [] == tmpl5.get_oses()

def test_remove_oses_template():
    """
    remove all link to a template
    """
    tmpl5 = get_template('tmpl5')
    tmpl5.add_os(os1)
    tmpl5.add_os(os2)
    assert set([os1, os2]) == set(tmpl5.get_oses())
    tmpl5.del_oses()
    assert [] == tmpl5.get_oses()

def test_remove_os_unlink_template():
    """
    try to remove an unlink os
    """
    tmpl5 = get_template('tmpl5')
    tmpl5.add_os(os1)
    assert [os1] == tmpl5.get_oses()
    raises(Exception, "tmpl5.del_os(os2)")

def test_link_wrong_os_template():
    """
    try to link an os instead of a 
    """
    tmpl5 = get_template('tmpl5')
    raises(Exception, "tmpl5.add_os(soft1)")

def test_close_database():
    commit_database()
    close_database()

# vim: ts=4 sw=4 expandtab
