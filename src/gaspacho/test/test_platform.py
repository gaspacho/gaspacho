# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import makedirs, unlink
from os.path import abspath, dirname, isdir, isfile, join
from py.test import raises
from sys import exit

here = dirname(abspath(__file__))

dir_name = join(here, 'datas')
file_name = join(dir_name, 'platform.sqlite')
database = 'sqlite:///%s' % file_name

if not isdir(dir_name):
    makedirs(dir_name)

if isfile(file_name):
    unlink(file_name)

from gaspacho import *
from gaspacho.platform import (add_os, add_software, add_path, add_platform,
            get_path, get_paths, get_platform)
initialize_database(create=True, database=database)

##############################################################################
# os                                                                         #
##############################################################################

def test_add_os():
    os1 = add_os('os1')

def test_add_os_same_name():
    raises(Exception, "add_os('os1')")

def test_get_os():
    os2 = add_os('os2')
    os22 = get_os('os2')
    assert os2 == os22

def test_get_unknown_os():
    assert None == get_os('unknown')

def test_get_oses():
    os1 = get_os('os1')
    os2 = get_os('os2')
    assert set([os1, os2]) == set(get_oses())

def test_get_name():
    os1 = get_os('os1')
    assert 'os1' == os1.get_name()

def test_comment_os():
    os3 = add_os('os3', 'comment3')

def test_get_comment_os():
    os2 = get_os('os2')
    os3 = get_os('os3')
    assert '' == os2.get_comment()
    assert 'comment3' == os3.get_comment()

def test_mod_comment_os():
    os3 = get_os('os3')
    assert 'comment3' == os3.get_comment()
    os3.mod_comment('new comment 3')
    assert 'new comment 3' == os3.get_comment()

#version
def test_add_os_version():
    os1 = get_os('os1')
    os1.add_version('osversion1')

def test_add_os_version_same_name():
    os1 = get_os('os1')
    raises(Exception, "os1.add_version('osversion1')")

def test_add_os_version_same_name_diff_OS():
    os3 = get_os('os3')
    os3.add_version('osversion1')

def test_get_os_version():
    os1 = get_os('os1')
    osversion2 = os1.add_version('osversion2')
    assert osversion2 == os1.get_version('osversion2')

def test_get_unknown_os_version():
    os1 = get_os('os1')
    assert None == os1.get_version('unknown')

def test_get_os_versions():
    os2 = get_os('os2')
    assert [] == os2.get_versions()
    osversion3 = os2.add_version('osversion3')
    assert [osversion3] == os2.get_versions()
    osversion4 = os2.add_version('osversion4')
    assert set([osversion3, osversion4]) == set(os2.get_versions())

def test_get_name_osversion():
    os2 = get_os('os2')
    osversion3 = os2.get_version('osversion3')
    assert 'osversion3' == osversion3.get_name()

def test_set_comment_osversion():
    os2 = get_os('os2')
    osversion5 = os2.add_version('osversion5', 'comment 5')

def test_get_comment_osversion():
    os2 = get_os('os2')
    osversion3 = os2.get_version('osversion3')
    osversion5 = os2.get_version('osversion5')
    assert '' == osversion3.get_comment()
    assert 'comment 5' == osversion5.get_comment()

def test_mod_comment_osversion():
    os2 = get_os('os2')
    osversion3 = os2.get_version('osversion3')
    assert '' == osversion3.get_comment()
    osversion3.mod_comment('new comment 3')
    assert 'new comment 3' == osversion3.get_comment()
    
##############################################################################
# software                                                                   #
##############################################################################

def test_add_software():
    software1 = add_software('software1')

def test_add_software_same_name():
    raises(Exception, "add_software('software1')")

def test_get_software():
    software2 = add_software('software2')
    software22 = get_software('software2')
    assert software2 == software22

def test_get_unknown_software():
    assert None == get_software('unknown')

def test_get_softwares():
    software1 = get_software('software1')
    software2 = get_software('software2')
    assert set([software1, software2]) == set(get_softwares())

def test_get_name():
    software1 = get_software('software1')
    assert 'software1' == software1.get_name()

def test_comment_software():
    software3 = add_software('software3', 'comment3')

def test_get_comment_software():
    software2 = get_software('software2')
    software3 = get_software('software3')
    assert '' == software2.get_comment()
    assert 'comment3' == software3.get_comment()

def test_mod_comment_software():
    software3 = get_software('software3')
    assert 'comment3' == software3.get_comment()
    software3.mod_comment('new comment 3')
    assert 'new comment 3' == software3.get_comment()

#version
def test_add_software_version():
    software1 = get_software('software1')
    software1.add_version('softwareversion1')

def test_add_software_version_same_name():
    software1 = get_software('software1')
    raises(Exception, "software1.add_version('softwareversion1')")

def test_add_software_version_same_name_diff_OS():
    software3 = get_software('software3')
    software3.add_version('softwareversion1')

def test_get_software_version():
    software1 = get_software('software1')
    softwareversion2 = software1.add_version('softwareversion2')
    assert softwareversion2 == software1.get_version('softwareversion2')

def test_get_unknown_software_version():
    software1 = get_software('software1')
    assert None == software1.get_version('unknown')

def test_get_software_versions():
    software2 = get_software('software2')
    assert [] == software2.get_versions()
    softwareversion3 = software2.add_version('softwareversion3')
    assert [softwareversion3] == software2.get_versions()
    softwareversion4 = software2.add_version('softwareversion4')
    assert set([softwareversion3, softwareversion4]) == set(software2.get_versions())

def test_get_name_softwareversion():
    software2 = get_software('software2')
    softwareversion3 = software2.get_version('softwareversion3')
    assert 'softwareversion3' == softwareversion3.get_name()

def test_set_comment_softwareversion():
    software2 = get_software('software2')
    softwareversion5 = software2.add_version('softwareversion5', 'comment 5')

def test_get_comment_softwareversion():
    software2 = get_software('software2')
    softwareversion3 = software2.get_version('softwareversion3')
    softwareversion5 = software2.get_version('softwareversion5')
    assert '' == softwareversion3.get_comment()
    assert 'comment 5' == softwareversion5.get_comment()

def test_mod_comment_softwareversion():
    software2 = get_software('software2')
    softwareversion3 = software2.get_version('softwareversion3')
    assert '' == softwareversion3.get_comment()
    softwareversion3.mod_comment('new comment 3')
    assert 'new comment 3' == softwareversion3.get_comment()

##############################################################################
# path                                                                   #
##############################################################################

def test_add_path():
    path1 = add_path('path1', 'moz')

def test_add_path_info():
    path2 = add_path('path2', 'moz', 'info')

def test_add_path_same_name():
    raises(Exception, "add_path('path1', 'moz')")
    add_path('path1', 'moz', 'info')

def test_get_path():
    path2 = add_path('path2', 'moz')
    path22 = get_path('path2', 'moz')
    assert path2 == path22

def test_get_unknown_path():
    assert None == get_path('unknown', 'moz')

def test_get_paths():
    path1 = get_path('path1', 'moz')
    path2 = get_path('path2', 'moz')
    path3 = get_path('path1', 'moz', 'info')
    path4 = get_path('path2', 'moz', 'info')
    assert set([path1, path2, path3, path4]) == set(get_paths())

def test_get_path_info():
    path3 = add_path('path3', 'moz', 'info')
    path32 = get_path('path3', 'moz', 'info')
    assert path3 == path32
    path33 = add_path('path3', 'moz')
    path34 = get_path('path3', 'moz')
    assert path33 == path34
    assert path33 != path3

def test_get_name_path():
    path1 = get_path('path1', 'moz')
    assert 'path1' == path1.get_name()

def test_get_extension_path():
    path1 = get_path('path1', 'moz')
    assert 'moz' == path1.get_extension()

def test_get_info_path():
    path1 = get_path('path1', 'moz', 'info')
    assert 'info' == path1.get_info()

def test_comment_path():
    path4 = add_path('path4', 'moz', comment='comment4')

def test_get_comment_path():
    path2 = get_path('path2', 'moz')
    path4 = get_path('path4', 'moz')
    assert '' == path2.get_comment()
    assert 'comment4' == path4.get_comment()

def test_mod_comment_path():
    path4 = get_path('path4', 'moz')
    assert 'comment4' == path4.get_comment()
    path4.mod_comment('new comment 4')
    assert 'new comment 4' == path4.get_comment()

##############################################################################
# platform                                                               #
##############################################################################

def test_add_platform():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform1 = add_platform(path1, osversion1, softwareversion1, [{'name': 'test'}])

def test_add_same_platform():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    raises(Exception, "platform2 = add_platform(path1, osversion1, softwareversion1,[{'name': 'test'}])")

def test_get_platform():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    assert None != get_platform(path1, osversion1, softwareversion1)

def test_get_unknown_platform():
    path1 = get_path('path1', 'moz')
    os2 = get_os('os2')
    osversion3 = os2.get_version('osversion3')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    assert None == get_platform(path1, osversion3, softwareversion1)

def test_get_platforms():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform1 = get_platform(path1, osversion1, softwareversion1)
    assert [platform1] == get_platforms()

def test_get_path_platform():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform1 = get_platform(path1, osversion1, softwareversion1)
    assert path1 == platform1.get_path()

def test_get_osversion_platform():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform1 = get_platform(path1, osversion1, softwareversion1)
    assert osversion1 == platform1.get_osversion()

def test_get_softwareversion_platform():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform1 = get_platform(path1, osversion1, softwareversion1)
    assert softwareversion1 == platform1.get_softwareversion()

def test_comment_platform():
    path1 = get_path('path1', 'moz')
    os2 = get_os('os2')
    osversion3 = os2.get_version('osversion3')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform2 = add_platform(path1, osversion3, softwareversion1, [{'name': 'test'}], 'comment1')

def test_get_comment_platform():
    path1 = get_path('path1', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    os2 = get_os('os2')
    osversion3 = os2.get_version('osversion3')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform1 = get_platform(path1, osversion1, softwareversion1)
    platform2 = get_platform(path1, osversion3, softwareversion1)
    assert u'os1 osversion1 - software1 softwareversion1' == \
                platform1.get_comment()
    assert u'comment1' == platform2.get_comment()

def test_mod_comment_platform():
    path1 = get_path('path1', 'moz')
    os2 = get_os('os2')
    osversion3 = os2.get_version('osversion3')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    platform2 = get_platform(path1, osversion3, softwareversion1)
    assert u'comment1' == platform2.get_comment()
    platform2.mod_comment('new comment1')
    assert u'new comment1' == platform2.get_comment()

def test_add_wrong_platform():
    path2 = get_path('path2', 'moz')
    os1 = get_os('os1')
    osversion1 = os1.get_version('osversion1')
    software1 = get_software('software1')
    softwareversion1 = software1.get_version('softwareversion1')
    raises(Exception, "add_platform(path2, osversion1, softwareversion1, [{'nameerror': 'test'}])")

def test_close_database():
    commit_database()
    close_database()

# vim: ts=4 sw=4 expandtab
