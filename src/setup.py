#!/usr/bin/env python

# Gaspacho

#from setuptools import setup
from distutils.core import setup
from os.path import walk, join, isfile
from os import chdir

data_files = []
def getPaths(var, dirname, fnames):
    d=dirname.split('/')
    if d[0] == 'web':
        d.pop(0)
    dirn = "/".join(d)
    var.append((join('share/gaspacho/', dirn), [join(dirname, fname) for fname in fnames if isfile(join(dirname, fname))]))

walk("web", getPaths, data_files)
walk("import", getPaths, data_files)

setup(
    author='Emmanuel Garette',
    author_email='gnunux@gnunux.info',
    name='gaspacho',
    version='VERSION',
    description='Centralized configuration tools',
    url='http://www.gaspacho-project.net/',
    packages=['gaspacho'],
    license='GPLv3', 
    data_files=data_files
)
 

