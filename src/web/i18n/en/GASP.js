/* Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* French translation for all ext-js strings
 */
Ext.i18n = {
    loading: 'Loading...',
    apply: 'Apply',
    //common
    add: 'Add',
    ok: 'OK',
    save: 'Save',
    cancel: 'Cancel',
    ready: 'Ready',
    delete: 'Delete',
    modify: 'Modify',
    confirm_delete_title: 'Confirmation',
    confirm_delete: 'Are you sure you want delete this(ose) group(s) (and associated choices) ?',
    confirm_delete_usergroup: 'Are you sure you want delete this(ose) users (and associated choices) ?',
    confirm_delete_user: 'Are you sure you want delete this user ?',
    confirm_delete_computer: 'Are you sure you want delete this computer ?',
    error_selection: 'Select first',
    //observable
    observable_error_title: 'Error',
    observable_server_unreachable: 'Server unreachable',
    observable_http_error: 'Error ',
    //login
    login_title: 'Login',
    login_user: 'User',
    login_password: 'Password',
    login_language: 'Language',
    login_login: 'Login',
    //rule
    rule_rule: 'Rule ',
    rule_value: 'Value ',
    rule_display_platforms: 'See platforms',
    rule_platforms: 'Platforms ',
    rule_description: 'Description ',
    rule_not_an_object: "not an object",
    rule_please_set_a_value: 'Please, set a value',
    //addgroup
    addgroup_title: 'Add group',
    addgroup_name: 'Group\'s name',
    addgroup_comment: 'Comment',
    addgroup_installpkg: 'Install packages',
    addgroup_template: 'Template',
    addgroup_generaltab: 'General',
    addgroup_usertab: 'User',
    addgroup_user: 'User',
    addgroup_user_type: 'Type',
    addgroup_softwaretab: 'Software',
    addgroup_software: 'Software',
    addgroup_ostab: 'OS',
    addgroup_os: 'OS',
    addgroup_managertab: 'Manager',
    addgroup_manager: 'Manager',
    addgroup_only_list_oses: 'Define list of OS',
    addgroup_only_list_softwares: 'Define list of softwares',
    addgroup_templatetab: 'Template',
    addgroup_template: 'Template',
    addgroup_computertab: 'Computer',
    addgroup_computer: 'Computer',
    please_set_groupname: 'Please set a group name',
    //category
    category_category: 'Category',
    //group
    group_group: 'Group',
    //choice
    choices: 'choices',
    //user
    user: 'User',
    usergroup: 'User Group',
    adduser_name: 'User name',
    adduser_title: "User",
    usertype: "Type",
    //computer
    addcomputer_name: "Nom",
    dns: "Domain name",
    ip: "IP",
    computertype: "Type",
    addcomputer_title: "Computer",
    not_allow_for_this_group: 'Display informations not allowed for this group',
    about_link: 'About',
    about_title: 'About Gaspacho',
    about_license: 'License',
    about_version: 'Version',
    about_copyright: "Copyright @ Gaspacho's Team",
};

Ext.i18n.python = {
/*    'Successful login': '',
    'Login failed': '',
    'Group added': '',
    'Group deleted': '',
    'Unknown state': '',
    'Choices saved': '',
    'Reorganisation saved': '',
    'Group modified': '',
    'Start display platforms': '',
    'Stop display platforms': '',
    'Applied': '',
    'User added': '',
    'User already exists': "",
    'Computer added': '',
    'Computer already exists': "",
    'Unable to delete: user already used in a group': '',
    'Not allowed for a manager': '',
    */
}

// vim: ts=4 sw=4 expandtab
