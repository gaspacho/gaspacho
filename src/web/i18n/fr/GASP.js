/* Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* French translation for all ext-js strings
 */
Ext.i18n = {
    loading: 'Chargement ...',
    apply: 'Appliquer',
    //common
    add: 'Créer',
    ok: 'OK',
    save: 'Enregistrer',
    cancel: 'Annuler',
    ready: 'Prêt',
    delete: 'Supprimer',
    modify: 'Modifier',
    confirm_delete_title: 'Confirmation',
    confirm_delete: 'Êtes-vous sûr de vouloir supprimer ce(s) groupe(s) (et les choix associés) ?',
    confirm_delete_usergroup: 'Êtes-vous sûr de vouloir supprimer ce(s) utilisateur(s) (et les choix associés) ?',
    confirm_delete_user: 'Êtes-vous sûr de vouloir supprimer cet utilisateur ?',
    confirm_delete_computer: 'Êtes-vous sûr de vouloir supprimer cette machine ?',
    error_selection: 'Selectionner un élément d\'abord',
    //observable
    observable_error_title: 'Erreur',
    observable_server_unreachable: 'Serveur injoignable',
    observable_http_error: 'Erreur ',
    //login
    login_title: 'Connexion',
    login_user: 'Utilisateur',
    login_password: 'Mot de passe',
    login_language: 'Langue',
    login_login: 'Connexion',
    //rule
    rule_rule: 'Règle ',
    rule_value: 'Valeur ',
    rule_display_platforms: 'Afficher plateformes',
    rule_platforms: 'Plateformes ',
    rule_description: 'Description ',
    rule_not_an_object: "n'est pas un objet",
    rule_please_set_a_value: 'Spécifier une valeur',
    //addgroup
    addgroup_title: 'Ajout d\'un groupe',
    addgroup_name: 'Nom du groupe',
    addgroup_comment: 'Commentaire',
    addgroup_installpkg: 'Installer les packages',
    addgroup_template: 'Type template',
    addgroup_generaltab: 'Général',
    addgroup_usertab: 'Utilisateur',
    addgroup_user: 'Utilisateur',
    addgroup_user_type: 'Type',
    addgroup_softwaretab: 'Logiciel',
    addgroup_software: 'Logiciel',
    addgroup_ostab: 'OS',
    addgroup_os: 'OS',
    addgroup_managertab: 'Manager',
    addgroup_manager: 'Manager',
    addgroup_only_list_oses: 'Définir une liste restreinte d\'OS',
    addgroup_only_list_softwares: 'Définir une liste restreinte de logiciels',
    addgroup_templatetab: 'Template',
    addgroup_template: 'Template',
    addgroup_computertab: 'Machine',
    addgroup_computer: 'Machine',
    please_set_groupname: 'Veuillez spécifier le nom du groupe',
    please_set_all_values: 'Veuillez spécifier toutes les valeurs',
    //category
    category_category: 'Catégorie',
    //group
    group_group: 'Groupe',
    //choice
    choices: 'choix',
    //user
    user: 'Utilisateur',
    usergroup: "Groupe d'utilisateur",
    adduser_name: "Nom de l'utilisateur",
    adduser_title: "Utilisateur",
    usertype: "Type",
    //computer
    addcomputer_name: "Nom",
    dns: "Nom de domaine",
    ip: "IP",
    computertype: "Type",
    addcomputer_title: "Machine",
    not_allow_for_this_group: "L'affichage des informations n'est pas autorisé pour ce groupe",
    about_link: 'À propos',
    about_title: 'À propos de Gaspacho',
    about_license: 'Licence ',
    about_version: 'Version ',
    about_copyright: "Copyright @ L'équipe Gaspacho",
};
Ext.i18n.python = {
    'Successful login': 'Connexion réussi',
    'Login failed': 'Echec de connexion',
    'Group added': 'Groupe ajouté',
    'Group deleted': 'Groupe supprimé',
    'Unknown state': 'Etat inconnu',
    'Choices saved': 'Choix enregistré',
    'Reorganisation saved': 'Réorganisation enregistrée',
    'Group modified': 'Groupe modifié',
    'Start display platforms': "Démarre l'affichage des plateformes",
    'Stop display platforms': "Stop l'affichage des plateformes",
    'Applied': "Appliqué",
    'User added': 'Utilisateur ajouté',
    'User already exists': "L'utilisateur existe déjà",
    'Computer added': 'Machine ajoutée',
    'Computer already exists': "Le nom de la machine existe déjà",
    'Unable to delete: user already used in a group': "Ne peut supprimer : l'utilisateur est utilisé dans un groupe",
    'User deleted': 'Utilisateur supprimé',
    'Not allowed for a manager': 'Pas autorisé pour un manager',
}

// vim: ts=4 sw=4 expandtab
