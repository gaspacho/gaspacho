from flask import Flask, url_for, json, jsonify, g, request, Response, render_template, make_response, send_from_directory
from flaskext.babel import Babel
from gaspacho.util import is_object, get_object_by_id
from gaspacho.valid import valid
from gaspacho import *
from gaspacho.config import (store_dir_apply, serv_port, serv_tls, serv_crt,
        serv_key, serv_static_path, admin_pam_user, auth_type, gaspacho_login,
        gaspacho_password, default_serv_lang, default_separator,
        force_resolve_dns_name)
from util import row2dict, make_json_response, make_error_response, bad_id_response, try_commit

app = Flask(__name__)
#app.config.from_pyfile('settings.cfg')
babel = Babel(app)

@babel.localeselector
def get_locale():
    user = getattr(g, 'user', None)
    if user is not None:
        return user.locale
    return 'fr' #request.accept_languages.best_match(['fr', 'en'])

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp

'''
INDEX ---------------------------------------------------------------
'''

@app.route('/')
def api_root():
    return render_template('index.html')

'''
CONVERSION METHODS --------------------------------------------------
'''

def group2dict(group):
    attributes = row2dict(group)
    relations  = {
        'children':  [group2dict(child) for child in get_groups(parent=group)],
        'users':     [user2dict(user)  for user  in group.get_users()],
        'managers':  [user2dict(manager) for manager in group.get_managers()],
        'computers': [computer2dict(computer) for computer in group.get_computers()],
        'templates': [template2dict(template)  for template  in group.get_templates()],
        'softwares': [software  for software  in group.get_softwares()],
        'oses':      [os2dict(os)    for os    in group.get_oses()]
    }
    return dict(attributes.items() + relations.items())

def computer2dict(computer):
    return dict(name=computer[0], type=computer[1])

def user2dict(user):
    attributes = row2dict(user)
    relations  = {}
    return dict(attributes.items() + relations.items())

def os2dict(os):
    attributes = row2dict(os)
    return dict(attributes.items())

def software2dict(software):
    attributes = row2dict(software)
    return dict(attributes.items())

def template2dict(template):
    attributes = row2dict(template)
    relations  = {}
    return dict(attributes.items() + relations.items())

def category2dict(category):
    attributes = row2dict(category)
    attributes['name'] = attributes['name'][get_locale()]
    relations = {
        #'tags': [tag2dict(get_object_by_id(tag['id'], 'tag')) for tag in t_get_tags()]
        'tags': [tag2dict(tag) for tag in category.get_tags()]
    }
    return dict(attributes.items() + relations.items())

def tag2dict(tag):
    attributes = row2dict(tag)
    attributes['name'] = attributes['name'][get_locale()]
    relations  = {
        'rules': [rule2dict(rule) for rule in tag.get_rules()]
    }
    return dict(attributes.items() + relations.items())

def rule2dict(rule):
    attributes = row2dict(rule)
    #attributes['name'] = attributes['name'][get_locale()]
    relations  = {}
    return dict(attributes.items() + relations.items())

'''
GROUPS --------------------------------------------------------------
'''

@app.route('/groups', methods = ['GET'])
def api_fetch_groups():
    groups = []
    for group in get_groups():
        groups.append(group2dict(group))
    return make_json_response(groups)

@app.route('/groups', methods = ['POST'])
def api_create_group():
    name       = request.json.get('name')
    comment    = request.json.get('comment')
    parent_id  = request.json.get('parent_id')
    if parent_id:
        parent = get_object_by_id(parent_id, 'group')
    else:
        parent = None
    installpkg = request.json.get('installpkg')
    group      = add_group(name=name, comment=comment, parent=parent, installpkg=installpkg)
    try: commit_database()
    except Exception, err:
        rollback_database()
        return make_error_response(str(err), 500)
    return make_json_response(group2dict(group))

@app.route('/groups/<groupid>', methods = ['GET'])
def api_read_group(groupid):
    try:
        group = get_object_by_id(int(groupid), 'group')
        return make_json_response(group2dict(group))
    except:
        return not_found()

@app.route('/groups/set/<groups_ids>', methods = ['GET'])
def api_get_group_set(groups_ids):
    groups = []
    for group_id in groups_ids.split(';'):
        try:
            group = get_object_by_id(int(group_id), 'group')
            groups.append(group2dict(group))
        except:
            groups.append({'id': group_id, 'error': 'Not Found'})
    return make_json_response(groups)

@app.route('/groups/<groupid>', methods = ['PUT'])
def api_update_group(groupid):
    #try:
        group   = get_object_by_id(int(groupid), 'group')
        name    = valid(request.json.get('name',    group.get_name()),    'unicode')
        comment = valid(request.json.get('comment', group.get_comment()), 'unicode')
        computers = request.json.get('computers')
        users = [get_object_by_id(int(user['id']), 'user') for user in request.json.get('users')]
        if name    != group.get_name():    group.mod_name(name)
        if comment != group.get_comment(): group.mod_comment(comment)
        for computer in group.get_computers(): group.del_computer(name=computer[0], type=computer[1])
        for computer in computers: group.add_computer(name=computer['name'], type=computer['type'])
        commons = list(set(users) & set(group.get_users()))
        for user in list(set(group.get_users()) - set(commons)): group.del_user(user)
        for user in list(set(users) - set(commons)): group.add_user(user)
        try: commit_database()
        except Exception, err:
            rollback_database()
            return make_error_response(str(err), 500)
        return make_json_response(group2dict(group))
    #except:
        return not_found()

@app.route('/groups/<groupid>', methods = ['DELETE'])
def api_delete_group(groupid):
    try:
        group = get_object_by_id(int(groupid), 'group')
        del_group(group)
        return make_json_response({'success': True})
    except:
        return not_found() # Change to get thrown error

'''
USERS -------------------------------------------------------------
'''

@app.route('/users', methods = ['GET'])
def api_fetch_users():
    users = []
    for user in get_users():
        users.append(user2dict(user))
    return make_json_response(users)

@app.route('/users', methods = ['POST'])
def api_create_user():
    name    = request.json.get('name')
    comment = request.json.get('comment')
    type    = request.json.get('type')
    try:
        user = add_user(name=name, type=type, comment=comment)
        try_commit()
        return make_json_response(user2dict(user), 201)
    except Exception, err:
        return make_error_response(str(err), 400)

@app.route('/users/<userid>', methods = ['GET'])
def api_read_user(userid):
    try:
        user = get_object_by_id(int(userid), 'user')
        return make_json_response(user2dict(user))
    except:
        return not_found()

@app.route('/users/set/<users_ids>', methods = ['GET'])
def api_get_user_set(users_ids):
    users = []
    for user_id in users_ids.split(';'):
        try:
            user = get_object_by_id(int(user_id), 'user')
            users.append(user2dict(user))
        except:
            users.append({'id': user_id, 'error': 'Not Found'})
    return make_json_response(users)

@app.route('/users/<userid>', methods = ['PUT'])
def api_update_user(userid):
    try:
        user    = get_object_by_id(int(userid), 'user')
        name    = request.json.get('name', user.get_name())
        comment = request.json.get('comment', user.get_comment())
       #type    = valid(request.json.get('type',    user.get_type()),    'unicode')
        #if name    != user.get_name():
        user.mod_name(name)
        #if comment != user.get_comment():
        user.mod_comment(comment)
       #if type    != user.get_type(): user.mod_type(type)
        return make_json_response(user2dict(user))
    except:
        return not_found()

@app.route('/users/<userid>', methods = ['DELETE'])
def api_delete_user(userid):
    try:
        user = get_object_by_id(int(userid), 'user')
        del_user(user)
        return make_json_response({'success': True})
    except:
        return not_found() # Change to get thrown error

'''
TEMPLATES --------------------------------------------------------------
'''

@app.route('/templates', methods = ['GET'])
def api_fetch_templates():
    templates = []
    for template in get_templates():
        templates.append(template2dict(template))
    return make_json_response(templates)

@app.route('/templates', methods = ['POST'])
def api_create_template():
    name       = request.json.get('name')
    comment    = request.json.get('comment')
    parent     = request.json.get('parent')
    installpkg = request.json.get('installpkg')
    template      = add_template(name=name, comment=comment, parent=parent, installpkg=installpkg)
    return make_json_response(template2dict(template))

@app.route('/templates/<templateid>', methods = ['GET'])
def api_read_template(templateid):
    try:
        template = get_object_by_id(int(templateid), 'template')
        return make_json_response(template2dict(template))
    except:
        return not_found()

@app.route('/templates/set/<templates_ids>', methods = ['GET'])
def api_get_template_set(templates_ids):
    templates = []
    for template_id in templates_ids.split(';'):
        try:
            template = get_object_by_id(int(template_id), 'template')
            templates.append(template2dict(template))
        except:
            templates.append({'id': template_id, 'error': 'Not Found'})
    return make_json_response(templates)

@app.route('/templates/<templateid>', methods = ['PUT'])
def api_update_template(templateid):
    try:
        template   = get_object_by_id(int(templateid), 'template')
        name    = valid(request.json.get('name',    template.get_name()),    'unicode')
        comment = valid(request.json.get('comment', template.get_comment()), 'unicode')
        if name    != template.get_name():    template.mod_name(name)
        if comment != template.get_comment(): template.mod_comment(comment)
        return make_json_response(template2dict(template))
    except:
        return not_found()

@app.route('/templates/<templateid>', methods = ['DELETE'])
def api_delete_template(templateid):
    try:
        template = get_object_by_id(int(templateid), 'template')
        del_template(template)
        return make_json_response({'success': True})
    except:
        return not_found() # Change to get thrown error

'''
CATEGORIES
'''

@app.route('/categories/set/<categories_ids>', methods = ['GET'])
def api_get_set_categories(categories_ids):
    data = []
    for category_id in categories_ids.split(';'):
        try:
            category = get_object_by_id(int(category_id), 'category')
            data.append({"id": category.id, "name": category.get_name(lang='en')})
        except:
            data.append({'id': category.id, 'error': 'Not Found'})
    return make_json_response(data)

'''
RULES --------------------------------------------------------------
'''

@app.route('/rules', methods = ['GET'])
def api_fetch_rules():
    rules = []
    for rule in get_choices():
        rules.append(rule2dict(rule))
    return make_json_response(rules)

@app.route('/rules', methods = ['POST'])
def api_create_rule():
    name       = request.json.get('name')
    comment    = request.json.get('comment')
    parent     = request.json.get('parent')
    installpkg = request.json.get('installpkg')
    rule      = add_rule(name=name, comment=comment, parent=parent, installpkg=installpkg)
    return make_json_response(rule2dict(rule))

@app.route('/rules/<ruleid>', methods = ['GET'])
def api_read_rule(ruleid):
    try:
        rule = get_object_by_id(int(ruleid), 'rule')
        return make_json_response(rule2dict(rule))
    except:
        return not_found()

@app.route('/rules/set/<rules_ids>', methods = ['GET'])
def api_get_rule_set(rules_ids):
    rules = []
    for rule_id in rules_ids.split(';'):
        try:
            rule = get_object_by_id(int(rule_id), 'rule')
            rules.append(rule2dict(rule))
        except:
            rules.append({'id': rule_id, 'error': 'Not Found'})
    return make_json_response(rules)

@app.route('/groups/<groupid>/rules/<ruleid>', methods = ['PUT'])
def api_update_rule(groupid,ruleid):  
    #try:
        group      = get_object_by_id(int(groupid), 'group')
        config     = load_config(group, None)
        categoryid = request.json.get('categoryid')
        tagid      = request.json.get('tagid')
        state      = request.json.get('state')
        value      = request.json.get('value')
        t_set_choice(config, categoryid, tagid, ruleid, state, value)
        return make_json_response(t_get_config(group))
    #except:
        return not_found()

@app.route('/rules/<ruleid>', methods = ['DELETE'])
def api_delete_rule(ruleid):
    try:
        rule = get_object_by_id(int(ruleid), 'rule')
        del_rule(rule)
        return make_json_response({'success': True})
    except:
        return not_found() # Change to get thrown error


'''
NESTED MODELS
'''
@app.route('/groups/<groupid>/categories', methods = ['GET'])
def api_fetch_group_categories(groupid):
    #try:
        group = get_object_by_id(int(groupid), 'group')
        config = load_config(group, None)
        data = t_get_categories(config)
        return make_json_response(data)
        '''
        config = load_config(group, None)
        data = t_get_categories(config)
        return make_json_response(data)
        '''
    #except:
    #    return not_found()

@app.route('/groups/<groupid>/users/<userid>/categories', methods = ['GET'])
def api_fetch_group_user_categories(groupid, userid):
    #try:
        group = get_object_by_id(int(groupid), 'group')
        user = get_object_by_id(int(userid), 'user')
        config = load_config(group, user)
        data = t_get_categories(config)
        return make_json_response(data)
    #except:
        return not_found()

@app.route('/groups/<groupid>/categories/<categoryid>/tags', methods = ['GET'])
def api_fetch_tags(groupid,categoryid):
    #try:
        category = get_object_by_id(int(categoryid), 'category')
        group = get_object_by_id(int(groupid), 'group')
        config = load_config(group, None)
        data = []
        for tag in t_get_tags(config, categoryid): # category.get_tags():
            data.append(tag) #(tag2dict(tag))
        return make_json_response(data)
    #except:
        return not_found()

@app.route('/groups/<groupid>/categories/<categoryid>/rules', methods = ['GET'])
def api_fetch_group_category_rules(groupid,categoryid):
    #try:
        group    = get_object_by_id(int(groupid), 'group')
        category = get_object_by_id(int(categoryid), 'category')
        data = []
        #return make_json_response({"id": tag.id, "name": tag.get_name(lang='en')})
        for rule in category.get_rules(softwares=group.get_softwares(), oses=group.get_oses()):
            data.append({"id": rule.id, "name": rule.get_name(lang='en')})
        return make_json_response(data)
    #except:
        return not_found()

@app.route('/groups/<groupid>/categories/<categoryid>/tags/<tagid>/rules', methods = ['GET'])
def api_fetch_group_category_tag_rules(groupid,categoryid,tagid):
    #try:
        group    = get_object_by_id(int(groupid), 'group')
        category = get_object_by_id(int(categoryid), 'category')
        tag      = get_object_by_id(int(tagid), 'tag')
        config = load_config(group, None)
        data = []
        #return make_json_response({"id": tag.id, "name": tag.get_name(lang='en')})
        for rule in t_get_rules(config, categoryid, tagid): #,tag.get_rules(softwares=group.get_softwares(), oses=group.get_oses()):
            data.append(rule) #(rule2dict(rule))
        return make_json_response(data)
    #except:
        return not_found()


@app.route('/softwares', methods = ['GET'])
def api_fetch_softwares():
    data = []
    for software in get_softwares():
        data.append(software2dict(software))
    return make_json_response(data);


@app.route('/oses', methods = ['GET'])
def api_fetch_oses():
    data = []
    for os in get_oses():
        data.append(os2dict(os))
    return make_json_response(data);


@app.route('/rules', methods = ['GET'])
def api_fetch_rules():
    #try:
        data = []
        #return make_json_response({"id": tag.id, "name": tag.get_name(lang='en')})
        for rule in get_rules():
            data.append(rule2dict(rule))
        return make_json_response(data)
    #except:
        return not_found()


if __name__ == '__main__':
    initialize_database()
    app.run(debug=True, port=5000, host='0.0.0.0')
