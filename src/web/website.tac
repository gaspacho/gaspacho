# _*_ coding: UTF8 _*_
# Copyright (C) 2010-2013 Team Gaspacho (see README for all contributors)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from sys import exit
from twisted.application import internet, service
from twisted.web import server, resource, static, server
from twisted.internet import ssl #, reactor
from twisted.web.error import NoResource, Error
from twisted.python import components

from zope.interface import Interface
from elixir import *
from sqlalchemy.exc import OperationalError

import pwd, grp
from fnmatch import fnmatch

from socket import gethostbyaddr, herror
from os.path import join, isfile

from gaspacho.util import is_object, get_object_by_id
from gaspacho.valid import valid

from gaspacho import (get_groups, get_templates, add_group, 
        get_users, get_user, del_group, del_template, add_template, 
        add_user, del_user, get_categories, get_choices, 
        get_softwares, get_oses, load, commit_database, initialize_database,
        rollback_database)

from gaspacho.config import (store_dir_apply, serv_port, serv_tls, serv_crt, 
        serv_key, serv_static_path, admin_pam_user, auth_type, gaspacho_login,
        gaspacho_password, default_serv_lang, default_separator,
        force_resolve_dns_name)

try:
    import json
except:
    #for python 2.5
    import simplejson as json

if auth_type == 'PAM':
    import PAM


class IPreferences(Interface):
    pass

class Preferences(components.Adapter):
    __implements__ = IPreferences

    def __init__(self, original):
        self.authentified = 'no'
        self.bad_auth = '0'
        self.user = ""
        components.Adapter.__init__(self,original)

components.registerAdapter(Preferences, server.Session, IPreferences)

class Page(resource.Resource):
    """
        abstract page
    """
    isLeaf = True
    allowed_for_manager = False
    def __init__(self, save_database=False):
        #when simply query, don't save database
        self.save_database = save_database

    ## authentification
    def close_session(self, request):
        prefs = request.getSession(IPreferences)
        prefs.authentified = 'no'
#        prefs.bad_auth = '0'
        prefs.user = ''
        prefs.lang = ''
        prefs.level = ''

    def require_auth(self, request):
        prefs = request.getSession(IPreferences)
        return self.send_json(auth=False)

    def render(self, request):
        # is the session authentified ? authentified = yes or no
        prefs = request.getSession(IPreferences)
        if hasattr(prefs, 'authentified'):
            if prefs.authentified != 'yes':
                return self.require_auth(request)
            else:
                try:
                    if prefs.level == 'manager' and \
                                    self.allowed_for_manager == False:
                        return self.send_json(error='Not allowed for a manager')
                    return self.render_page(request)
                except Exception, e:
                    import traceback
                    traceback.print_exc()
                    return self.send_json(error=str(e))
        return self.require_auth(request)

    ## getters
    def get_data_from_request(self, request, arg, load_json=False, 
                    checkbox=False, object_type=None, required=True):
        """
        Return arg from request. If object_type specified, return object
        request is return by twister
        arg: name of argument return by user
        load_json: if value is a json type
        checkbox: if value is a checkbox boolean
        object_type: if value is an id of an object, specify the object type
                     to return directly the object
        required: if True, return a ValueError if argument is not specify by
                  user or is no object has this id
        """
        if checkbox and object_type != None:
            raise Exception('If checkbox, object_type must not be set')
        #if arg is in the request
        if request.args.has_key(arg):
            try:
                #return value
                if object_type == None:
                    ret = request.args[arg][0]
                    if load_json:
                        #load json value
                        return json.loads(ret)
                    elif checkbox:
                        #if checkbox, return 'on' for True
                        if ret == 'on':
                            return True
                        else:
                            raise Exception('Are you should %s is a checkbox type ?' % arg)
                    else:
                        return ret
                #return object
                else:
                    obj = get_object_by_id(request.args[arg][0], object_type)
                    if required and obj == None:
                        raise ValueError("value %s required"% str(arg))
                    return obj
            except Exception, e:
                raise ValueError(str(e))
        else:
            if checkbox:
                #if checkbox, not value is return for False
                return False
            elif required:
                raise ValueError("value %s required"% str(arg))
            else:
                return None

    def get_object_by_webid(self, request, request_name='groupid', webid=None, 
                required=True):
        """
        Return grp and user id from webid
        """
        prefs = request.getSession(IPreferences)
        if prefs.level == 'manager':
            #if manager, required is always True...
            required=True
        #split webid in too part: [group, user]
        if webid == None:
            webid = self.get_data_from_request(request=request, 
                        arg=request_name, required=required)
        if webid == '':
            return [None, None]
        webid = webid.split('-')
        #group
        if webid[0] == 'src':
            #root
            grp = None
        elif webid[0][0] == 't':
            #template
            grp = get_object_by_id(webid[0][1:], 'template')
        else:
            #group
            grp = get_object_by_id(webid[0], 'group')
        if prefs.level == 'manager':
            if prefs.user not in grp.managers:
                raise Exception('No autorisation on this group')
        #user
        if len(webid) == 2:
            user = get_object_by_id(webid[1], 'user')
        else:
            user = None
        return [grp, user]

    def convert_to_webid(self, grp, user=None):
        """
        Return webid from user and grp
        -----------------------------------------------------------------------
        Webid factory
        -----------------------------------------------------------------------
        webid's format: group-user
        group:
         - src: root of the treeview
         - txx: template's id
         - xx: group's id
        user:
         - xx: user's id
        """
        if is_object(grp, 'template'):
            webid = 't'
        else:
            webid = ''
        webid += str(grp.id)
        if user != None:
            webid += '-'+str(user.id)
        return webid

    def send_json(self, ret=None, error=None, message=None, auth=True, treenode=False):
        """
        Convert answer to json for extjs
        error: if return an error, error must be the error message
        message: if a message must be display (not an error message)
        """
        #if authentified session
        if auth == True:
            buf = {'success': True, 'data': '', 'authentification': True}
            if error == None:
                if self.save_database:
                    try:
                        commit_database()
                    except Exception, e:
                        # FIXME traceback is bad in production mode
                        import traceback
                        traceback.print_exc()
                        error = "error while saving database"
                if ret != None:
                    if treenode == True:
                        #if treenode
                        buf = ret
                    else:
                        buf['data'] = ret
                if message != None:
                    buf['message'] = message
            if error != None:
                if self.save_database:
                    rollback_database()
                buf = {'success': False, 'data': '', 'message': error}
        #if session is not authentified
        else:
            buf = {'success': True, 'data': '', 'authentification': False}
            if error != None:
                buf['success'] = False
                buf['message'] = error
                
        return json.dumps(buf)

class Login(Page):
    def render(self, request):
        try:
            user = self.get_data_from_request(request=request, arg='user')
            password = self.get_data_from_request(request=request, arg='password')
            lang = self.get_data_from_request(request=request, arg='countryCode')
            #FIXME: default langage!
            #lang = default_serv_lang
        except Exception, e:
            return self.send_json(error=str(e))

        if auth_type == 'PAM':
            if admin_pam_user != None and user in admin_pam_user:
                luser = user
                level = 'admin'
            else:
                luser = get_user(unicode(user))
                if luser != None:
                    level = 'manager'
                else:
                    return self.send_json(error='User not allowed', auth=False)

        prefs = request.getSession(IPreferences)

        if auth_type == 'PAM':
            # PAM stuff
            def pam_conv(auth, query_list, userData):
                #pam password authentification utility
                resp = []
                for i in range(len(query_list)):
                    query, type = query_list[i]
                    if type == PAM.PAM_PROMPT_ECHO_ON or type == PAM.PAM_PROMPT_ECHO_OFF:
                        resp.append((password, 0))
                    elif type == PAM.PAM_PROMPT_ERROR_MSG or type == PAM.PAM_PROMPT_TEXT_INFO:
                        resp.append(('', 0))
                    else:
                        return None
                return resp

            auth = PAM.pam()
            auth.start('GASP')
            auth.set_item(PAM.PAM_USER, user)
            auth.set_item(PAM.PAM_CONV, pam_conv)
            try:
                auth.authenticate()
                auth.acct_mgmt()
                prefs.user = luser
                prefs.level = level
                prefs.authentified = 'yes'
                prefs.lang = lang
                prefs.display_platforms = False
                return self.send_json(ret={'level': prefs.level}, message='Successful login', auth=True)
            except PAM.error, resp:
                #print 'Go away! (%s)' % resp
                prefs.authentified = 'no'
                return self.send_json(error='Login failed', auth=False)
            except:
                prefs.authentified = 'no'
                return self.send_json(error='Login failed', auth=False)
                #print 'Internal error'
        else:
            #internal
            if gaspacho_login == None or password == None:
                return self.send_json(error='Login failed', auth=False)

            if user == gaspacho_login and password == gaspacho_password:
                #request.getSession().touch()
                prefs.user = user
                prefs.level = 'admin'
                prefs.authentified = 'yes'
                prefs.lang = lang
                prefs.display_platforms = False
                return self.send_json(ret={'level': prefs.level}, message='Successful login', auth=True)
            else:
                prefs.authentified = 'no'
                return self.send_json(error='Login failed', auth=False)


class Logout(Page):
    allowed_for_manager = True
    def render_page(self, request):
        self.close_session(request)
        return self.send_json(message="session closed", auth=False)

class GetGroups(Page):
    allowed_for_manager = True
    def _get_groups(self, group, filter, expanded=False, type='group'):
        #convert current group to json
        id = self.convert_to_webid(group)
        prefix_cls = ''
        readable = True
        if filter != None and filter not in group.managers:
            #FIXME: maybe should be hidden
            prefix_cls = 'forbidden_'
            readable = False
            
        #leaf must always be False. If True, we can't draganddrop on it
        ret = {"text": group.name, "id": id, "cls": "%s%s" % (prefix_cls, type), "leaf": False, \
                "expanded": expanded, 'readable': readable}
        if filter != None or type == 'template':
            ret['allowDrop'] = False
            ret['draggable'] = False
        ret_children = []
        #if type is group, check children's group
        if type == 'group':
            for grp in get_groups(parent=group):
                ret_children.append(self._get_groups(grp, filter))
        #get users for this group
        for user in group.get_users():
            id = self.convert_to_webid(group, user)
            if readable:
                cls = user.type
            else:
                cls = {u'user': 'forbidden_user', u'usergroup':
                        'forbidden_usergroup'}.get(user.type)
            ret_children.append({"text": user.name, "id": id, "cls": cls, \
                    "leaf": True, 'draggable': False, 'readable': readable})
        #if children
        if ret_children != []:
            ret['children'] = ret_children
        return ret

    def render_page(self, request):
        node = self.get_data_from_request(request=request, arg='node')
        ret = []
        if node == 'src':
            prefs = request.getSession(IPreferences)
            if prefs.level == 'manager':
                filter = prefs.user
            else:
                filter = None
            for grp in get_groups():
                ret.append(self._get_groups(grp, filter, expanded=True))

            #add template
            for tmpl in get_templates():
                ret.append(self._get_groups(tmpl, filter, type='template'))
                
        return self.send_json(ret, treenode=True)

class GetCategories(Page):
    allowed_for_manager = True
    def render_page(self, request):
        ret = []
        prefs = request.getSession(IPreferences)
        grp, user = self.get_object_by_webid(request)
        if user == None:
            user_level = False
        else:
            user_level = True
        for category in get_categories(grp=grp, user_level=user_level,
                    display=True):
            name = category.get_name(lang=prefs.lang)
            ret.append({"text": name, "id": category.id})
        return self.send_json(ret)

class GetRules(Page):
    allowed_for_manager = True
    def render_page(self, request):
        prefs = request.getSession(IPreferences)
        category = self.get_data_from_request(request=request,
                        arg='categoryid', object_type='category')
        grp, user = self.get_object_by_webid(request)
        #if user selected, only confuser rules
        if user == None:
            user_level = False
        else:
            user_level = True

        conv_state = {u'off': False, None: False, u'on': True, u'free': None}

        choices = get_choices(grp, user)

        ret = []
        #get tags for a category
        for tag in category.get_tags():
            tagname = tag.get_name(lang=prefs.lang)
            #get rules for this tag (with conflevel)
            for rule in tag.get_rules(softwares=grp.softs, oses=grp.oses, 
                    user_level=user_level, display=True):
                choice, hchoice = choices.get(rule, (None, None))

                #build rule in json
                rule_json = {'tagid':tag.id, 'tag': tagname,
                        'name': rule.get_name(lang=prefs.lang),
                        'id': rule.id, 'options': rule.options, 
                        'type': rule.type, 'platforms': None,
                        'value': rule.get_defaultvalue(),
                        'state': conv_state[rule.defaultstate],
                        'description': rule.get_comment(lang=prefs.lang),
                        'herited': True}

                #build platforms
                if prefs.display_platforms:
                    for platform in rule.get_platforms():
                        if rule_json['platforms'] == None:
                            rule_json['platforms'] = []
                        rule_json['platforms'].append(platform.comment)

                #set state/value with herited value
                if hchoice != None:
                    rule_json['state'] = conv_state[hchoice.state]
                    rule_json['value'] = hchoice.get_value()

                #this state/value are herited
                rule_json['hstate'] = rule_json['state']
                rule_json['hvalue'] = rule_json['value']

                #change state/value if choice
                if choice != None:
                    rule_json['state'] = conv_state[choice.state]
                    rule_json['value'] = choice.get_value()
                    rule_json['herited'] = False

                ret.append(rule_json)
        return self.send_json(ret)

class GetUsers(Page):
    allowed_for_manager = True
    def render_page(self, request):
        ret = []
        grp, user = self.get_object_by_webid(request, required=False)
        if grp:
            active_user = grp.users
        else:
            active_user = []
        for user in get_users():
            if user in active_user:
                activate = True
            else:
                activate = False
            ret.append({"id": user.id, "user": user.name, 'type': user.type, 'activate': activate})
        return self.send_json(ret)

class GetProperty(Page):
    def render_page(self, request):
        grp, user = self.get_object_by_webid(request)
        if is_object(grp, 'template'):
            template = True
            installpkg = False
        else:
            template = False
            installpkg = grp.get_installpkg()
        if grp.oses == []:
            os = False
        else:
            os = True
        if grp.softs == []:
            software = False
        else:
            software = True
        ret = {"name": grp.name, "comment": grp.comment, 'template': template,
               'os': os, 'software': software, "installpkg": installpkg}
        return self.send_json(ret)

class GetSoftwares(Page):
    def render_page(self, request):
        ret = []
        grp, user = self.get_object_by_webid(request, required=False)
        if grp:
            softwares = grp.softs
        else:
            softwares = []
        for software in get_softwares():
            if software in softwares:
                activate = True
            else:
                activate = False
            ret.append({"id": software.id, "software": software.name, 'activate': activate})
        return self.send_json(ret)

class GetOSes(Page):
    def render_page(self, request):
        ret = []
        grp, user = self.get_object_by_webid(request, required=False)
        if grp:
            oses = grp.oses
        else:
            oses = []
        for os in get_oses():
            if os in oses:
                activate = True
            else:
                activate = False
            ret.append({"id": os.id, "os": os.name, 'activate': activate})
        return self.send_json(ret)

class GetManagers(Page):
    def render_page(self, request):
        ret = []
        grp, user = self.get_object_by_webid(request, required=False)
        if grp:
            managers = grp.managers
        else:
            managers = []
        for manager in get_users():
            if manager in managers:
                activate = True
            else:
                activate = False
            ret.append({"id": manager.id, "manager": manager.name, 'type': manager.type, 'activate': activate})
        return self.send_json(ret)

class GetTemplates(Page):
    def render_page(self, request):
        ret = []
        grp, user = self.get_object_by_webid(request, required=False)
        if grp:
            if is_object(grp, 'template'):
                templates = []
            else:
                templates = grp.tmpls
        else:
            templates = []
        for template in get_templates():
            if template in templates:
                activate = True
            else:
                activate = False
            ret.append({"id": template.id, "template": template.name, 'activate': activate})
        return self.send_json(ret)

class GetComputers(Page):
    def render_page(self, request):
        ret = None
        grp, user = self.get_object_by_webid(request, required=False)
        ret = []
        if is_object(grp, 'group'):
            for computer, typ in grp.get_computers():
                ret.append({"id": computer, "computer": computer, 'type': typ})
        return self.send_json(ret)

#----------------------------------- ADD -------------------------------------- 

class AddGroup(Page):
    def render_page(self, request):
        name = self.get_data_from_request(request=request, arg='name')
        installpkg = self.get_data_from_request(request=request,
                    arg='installpkg', required=False)
        comment = self.get_data_from_request(request=request, arg='comment',
                    required=False)
        templates = self.get_data_from_request(request=request, arg='templates',
                    load_json=True)
        users = self.get_data_from_request(request=request, arg='users',
                    load_json=True)
        parent, user = self.get_object_by_webid(request, 'node')
        softwares = self.get_data_from_request(request=request, arg='softwares',
                    load_json=True)
        oses = self.get_data_from_request(request=request, arg='oses',
                    load_json=True)
        managers = self.get_data_from_request(request=request, arg='managers',
                    load_json=True)
        computers = self.get_data_from_request(request=request, arg='computers',
                    load_json=True)
        #set only if checked
        os = self.get_data_from_request(request=request, arg='os',
                    checkbox=True)
        template = self.get_data_from_request(request=request, arg='template',
                    checkbox=True)
        software = self.get_data_from_request(request=request, arg='software',
                    checkbox=True)

        if template:
            group = add_template(name=name, comment=comment)
        else:
            #if parent is a template, just add in root not in template
            if is_object(parent, 'template'):
                parent=None
            group = add_group(name=name, comment=comment, parent=parent,
                                installpkg=installpkg)
            for id in templates:
                obj = get_object_by_id(id, 'template')
                group.add_template(obj)
        for id in users:
            user = get_object_by_id(id, 'user')
            group.add_user(user)
        for id in managers:
            user = get_object_by_id(id, 'user')
            group.add_manager(user)
        if software:
            for id in softwares:
                obj = get_object_by_id(id, 'software')
                group.add_software(obj)
        if os:
            for id in oses:
                obj = get_object_by_id(id, 'os')
                group.add_os(obj)
        if not template:
            for computer in computers:
                group.add_computer(name=computer['name'], type=computer['type'])
        return self.send_json(message='Group added')

class AddUser(Page):
    def render_page(self, request):
        name = self.get_data_from_request(request=request, arg='name')
        usertype = self.get_data_from_request(request=request, arg='usertype')
        add_user(name=name, type=usertype)
        return self.send_json(message='User added')

#----------------------------------- DEL --------------------------------------

class DelGroup(Page):
    def render_page(self, request):
        group, user = self.get_object_by_webid(request, 'id')
        if user == None:
            if is_object(group, 'group'):
                del_group(group)
            else:
                del_template(group)

        else:
            if is_object(group, 'group'):
                group.del_user(user)
            else:
                template.del_user(user)
        return self.send_json(message='Group deleted')

class DelUser(Page):
    def render_page(self, request):
        user = self.get_data_from_request(request=request, arg='userid', object_type='user')
        del_user(user)
        return self.send_json(message='User deleted')

#----------------------------------- SET --------------------------------------

class SetChoices(Page):
    allowed_for_manager = True
    def render_page(self, request):
        grp, user = self.get_object_by_webid(request)
        rules = self.get_data_from_request(request=request,
                    arg='modifiedrules', load_json=True)
        for id in rules.keys():
            rule = get_object_by_id(id, 'rule')
            #FIXME: need support platform + value
            herited = rules[id][u'herited']
            if herited:
                rule.del_choice(group=grp, user=user)
            else:
                state = rules[id][u'state']
                value = rules[id][u'value']
                if state == False:
                    tstate = u'off'
                elif state == True:
                    tstate = u'on'
                elif state == None:
                    tstate = u'free'
                else:
                    self.send_json(error='Unknown state')
                rule.set_choice(group=grp, user=user, state=tstate, value=value)
        return self.send_json(message='Choices saved')

class SetLevels(Page):
    def parse_sub_level(self, request, sub_level, group=None):
        for id, opts in sub_level.items():
            id = str(id)
            if type(opts) != dict:
                raise Exception('Error in groups, not a valid group')
            grp, user = self.get_object_by_webid(request, webid=id)
            if grp.parent != group:
                grp.parent = group
            if not opts.has_key('level'):
                raise Exception('Error in groups dict, should have level')
            if not opts.has_key('children'):
                raise Exception('Error in groups dict, should have children')
            grp.level = opts['level']
            self.parse_sub_level(request, opts['children'], grp)

    def render_page(self, request):
        groups = self.get_data_from_request(request=request, arg='groups', load_json=True)
        self.parse_sub_level(request, groups)

        return self.send_json(message='Reorganisation saved')

class SetGroup(Page):
    """
    modify existing group
    need:
    - name: name of the group
    - node: node id of the modified group
    - users: dict for users
    - softwares: dict for softwares
    - oses: dict for OS
    - templates: dict for Template
    optionnal:
    - comment: comment for this group
    """
    def render_page(self, request):
        name = self.get_data_from_request(request=request, arg='name')
        comment = self.get_data_from_request(request=request, arg='comment', required=False)
        group, user = self.get_object_by_webid(request, 'node')
        users = self.get_data_from_request(request=request, arg='users', load_json=True)
        installpkg = self.get_data_from_request(request=request,
                    arg='installpkg', checkbox=True)
        softwares = self.get_data_from_request(request=request, arg='softwares', load_json=True)
        oses = self.get_data_from_request(request=request, arg='oses', load_json=True)
        managers = self.get_data_from_request(request=request, arg='managers', load_json=True)
        templates = self.get_data_from_request(request=request, arg='templates', load_json=True)
        computers = self.get_data_from_request(request=request, arg='computers', load_json=True, required=False)
        #set only if checked
        os = self.get_data_from_request(request=request, arg='os', checkbox=True)
        #template is disable when modified a group
        #template = self.get_data_from_request(request=request, arg='template', checkbox=True)
        if is_object(group, 'template'):
            template = True
        else:
            template = False
        software = self.get_data_from_request(request=request, arg='software', checkbox=True)

        #modify the name
        name = valid(name, 'unicode')
        if name != group.get_name():
            group.mod_name(name)
        #modify installpkg
        group.mod_installpkg(installpkg)
        #modify the comment
        group.mod_comment(comment)
        #add or remove user to this group
        for id, activate in users.items():
            user = get_object_by_id(id, 'user')
            if activate == False:
                group.del_user(user)
            else:
                group.add_user(user)

        for id, activate in managers.items():
            user = get_object_by_id(id, 'user')
            if activate == False:
                group.del_manager(user)
            else:
                group.add_manager(user)
        if not template:
            for id, activate in templates.items():
                tmpl = get_object_by_id(id, 'template')
                if activate == False:
                    group.del_template(tmpl)
                else:
                    group.add_template(tmpl)
        if software:
            for id, activate in softwares.items():
                obj = get_object_by_id(id, 'software')
                if activate == False:
                    group.del_software(obj)
                else:
                    group.add_software(obj)
        else:
            group.del_softwares()
        if os:
            for id, activate in oses.items():
                obj = get_object_by_id(id, 'os')
                if activate == False:
                    group.del_os(obj)
                else:
                    group.add_os(obj)
        else:
            group.del_oses()
        if not template:
            old_computers = []
            add_computers = []
            for computer in computers:
                if computer.has_key('id'):
                    old_computers.append((computer['name'], computer['type']))
                else:
                    add_computers.append(computer)
            current_computer = []
            for tname, ttype in group.get_computers():
                current_computer.append((tname, ttype))
            for computer in list(set(current_computer) - set(old_computers)):
                group.del_computer(computer[0], computer[1])
            #after, before remove computer
            for computer in add_computers:
                group.add_computer(name=computer['name'], type=computer['type'])
        return self.send_json(message='Group modified')

class SetDisplayPlaforms(Page):
    def render_page(self, request):
        prefs = request.getSession(IPreferences)
        display = valid(self.get_data_from_request(request=request,
                        arg='display'), 'boolean')
        prefs.display_platforms = display
        if display:
            return self.send_json(message='Start display platforms')
        else:
            return self.send_json(message='Stop display platforms')

class SetApply(Page):
    #FIXME: allow apply all groups for manager ?
    allowed_for_manager = True
    def render_page(self, request):
        apply_choices()
        return self.send_json(message='Applied')

#------------------------------------------------------------------------------
class DelPages(resource.Resource):
    def getChild(self, name, request):
        pages = {'group': DelGroup(save_database=True),
                 'user': DelUser(save_database=True),
                }

        if static.isDangerous(name):
            return static.dangerousPathError
        if name in pages.keys():
            return pages[name]
        # unauthorized request
        return NoResource()
#------------------------------------------------------------------------------

class SetPages(resource.Resource):
    def getChild(self, name, request):
        pages = {'choices': SetChoices(save_database=True),
                'levels': SetLevels(save_database=True),
                'group': SetGroup(save_database=True),
                'display_plaforms': SetDisplayPlaforms(),
                'apply': SetApply(),
                }

        if static.isDangerous(name):
            return static.dangerousPathError
        if name in pages.keys():
            return pages[name]
        # unauthorized request
        return NoResource()

class GetPages(resource.Resource):
    def getChild(self, name, request):
        pages = {'groups': GetGroups(),
                 'categories': GetCategories(),
                 'rules': GetRules(),
                 'users': GetUsers(),
                 'property': GetProperty(),
                 'softwares': GetSoftwares(),
                 'oses': GetOSes(),
                 'managers': GetManagers(),
                 'templates': GetTemplates(),
                 'computers': GetComputers(),
                }
        if static.isDangerous(name):
            return static.dangerousPathError
        if name in pages.keys():
            return pages[name]
        # unauthorized request
        return NoResource()

class LogPage(resource.Resource):
    def getChild(self, name, request):
        pages = {'in': Login(),
                'out': Logout(),
        }
        if static.isDangerous(name):
            return static.dangerousPathError
        if name in pages.keys():
            return pages[name]
        # unauthorized request
        return NoResource()

class AddPages(resource.Resource):
    def getChild(self, name, request):
        pages = {'group': AddGroup(save_database=True),
                'user': AddUser(save_database=True),
                }

        if static.isDangerous(name):
            return static.dangerousPathError
        if name in pages.keys():
            return pages[name]
        # unauthorized request
        return NoResource()


class ApplyPages(resource.Resource):
    isLeaf = True
    def render(self, request):
#        try:
        ip = request.client.host
        try:
            conflevel = request.args['conflevel'][0]
            osname = request.args['os'][0]
            hostname = request.args['hostname'][0]
        except:
            raise ValueError('Missing value')

        if conflevel == 'user' or conflevel == 'context':
            try:
                username = request.args['user'][0]
            except:
                raise ValueError('Missing value')
            #FIXME: should be send by client
            try:
                #get primary usergroup
                usergroup = grp.getgrgid(pwd.getpwnam(username).pw_gid).gr_name
            except:
                raise ValueError('Unknown user')

        fname = join(store_dir_apply, 'gaspacho.json')
        if not isfile(fname):
            #log this
            #raise Exception('file %s not found' % fname)
            return '{}'
        fh = file(fname, 'r')
        tree = json.load(fh, encoding='utf-8')
        fh.close()
        is_break = False
        for group in tree:
            for computer, typ in group[u'group']:
                if typ == 'dns':
                    if force_resolve_dns_name:
                        try:
                            matchname = gethostbyaddr(ip)[0]
                        except:
                            raise Exception('Unable to get name for %s' % ip)
                    else:
                        matchname = hostname

                else:
                    matchname = ip
                if fnmatch(matchname, computer):
                    is_break = True
                    break
        if not is_break:
            print "FIXME: no result !"
            return '{}'
        if conflevel == 'user' or conflevel == 'context':
            if group[u'path'][conflevel].has_key('user') and \
                    group[u'path'][conflevel][u'user'].has_key(username):
                if not group[u'path'][conflevel][u'user'][username].has_key(
                    osname):
                    print "FIXME: not for this OS !"
                    return '{}'
                path = group[u'path'][conflevel][u'user'][username][osname]
            elif group[u'path'][conflevel].has_key(u'usergroup') and \
                    group[u'path'][conflevel][u'usergroup'].has_key(usergroup):
                if not group[u'path'][conflevel][u'usergroup'][usergroup].has_key(osname):
                    print "FIXME: not for this OS !"
                    return '{}'
                path = group[u'path'][conflevel][u'usergroup'][usergroup][osname]
            elif group[u'path'][conflevel][u'all'].has_key(osname):
                path = group[u'path'][conflevel][u'all'][osname]
            else:
                raise Exception("FIXME: no connexion!")
        else:
            if not group[u'path'][conflevel].has_key(osname):
                print "FIXME: not for this OS !"
                return '{}'
            path = group[u'path'][conflevel][osname]

        if not isfile(path):
            raise Exception('file %s not found'%path)
        fh = file(path, 'r')
        content = json.load(fh, encoding='utf-8')
        fh.close()
        return json.dumps(content)
        #except Exception, e:
        #    raise Exception('Error in ApplyPages: %s'%e)

class LangPages(resource.Resource):
    isLeaf = False

    def getChild(self, name, request):
        #print request.getHeader('if-modified-since')
        request.setHeader('if-modified-since', None)
        #request.setHeader('Cache-Control','no-cache')
        #request.setHeader('Pragma','no-cache')
        #request.setHeader('Expires',"Thu, 01 Jan 1970 00:00:00 GMT") 
        #print request.getAllHeaders()
        if name not in ['ext.js', 'GASP.js']:
            return None
        prefs = request.getSession(IPreferences)
        if hasattr(prefs, 'lang') and prefs.lang != '':
            fullpath = join(serv_static_path, 'i18n', prefs.lang, name)
            if not isfile(fullpath):
                fullpath = join(serv_static_path, 'i18n', default_serv_lang, name)
        else:
            fullpath = join(serv_static_path, 'i18n', default_serv_lang, name)
        if not isfile(fullpath):
            return None
        return static.File(fullpath)

index_file = join(serv_static_path, 'static', 'index.html')
if not isfile(index_file):
    print 'Unable to find %s, please configure serv_static_path in gaspacho.conf' % index_file
    exit(1)
root = static.File(join(serv_static_path, 'static'))
root.putChild("log", LogPage())
root.putChild("get", GetPages())
root.putChild("set", SetPages())
root.putChild("add", AddPages())
root.putChild("del", DelPages())
root.putChild("apply", ApplyPages())
root.putChild("i18n", LangPages())

site = server.Site(root)

initialize_database()#debug=True)

from gaspacho.apply import apply_choices

#reactor.listenTCP(8080, site)
#reactor.run()
application = service.Application("Gaspacho")

if serv_tls == "disabled":
    serv = internet.TCPServer(serv_port, site)
else:
    sslContext = ssl.DefaultOpenSSLContextFactory(
        serv_key,
        serv_crt,
    )
    serv = internet.SSLServer(serv_port, site, contextFactory = sslContext)
serv.setServiceParent(application)

# vim: ts=4 sw=4 expandtab
