define(['vent','models/Group','models/User'], 
function (vent, Group, User) {
  "use strict";

  return {
    breadcrumbs: function(item, groupCollection){

        var rec_breadcrumbs = function(group_id){
            if(!group_id) return '<li><span class="divider">/</span></li>';
            var group = groupCollection.get(group_id);
            return rec_breadcrumbs(group.get('parent_id'))+
                '<li><a href="#"><i class="icon-folder-open"></i>'+group.get('name')+'</a> <span class="divider">/</span></li>'; 
        }

        var parent_id = null, iconClass;
        if(item instanceof User){
            parent_id = item.group_id;
            iconClass = item.get('type');
        } else 
        if(item instanceof Group){
            parent_id = item.get('parent_id');
            iconClass = 'folder-close'
        }

        return rec_breadcrumbs(parent_id)+
            '<li class="active"><i class="icon-'+iconClass+'"></i>'+item.get('name')+'</li>';
    }
  };
});
