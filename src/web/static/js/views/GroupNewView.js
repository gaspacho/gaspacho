define(['marionette','vent','templates'], 
function (Marionette,vent,templates) {
  "use strict";

  return Marionette.Layout.extend({

    template : templates.groupNewView,

    regions : {},

    ui : {
      name: 'input[name=name]',
      comment: 'input[name=comment]'
    },

    events : {
      'click button[type=submit]': 'create'
    },

    initialize : function() {
      this.bindTo(this.model, 'change', this.render);
    },

    onRender : function() {

    },

    create: function(){
      var name = this.ui.name.val();
      var comment = this.ui.comment.val();
      this.model.save({
        'name': name,
        'comment': comment
      },{ 
        success: _.bind(function(model, response){
          if(this.model.get('parent')) this.model.get('parent').children.add(model);
          else this.collection.add(model);
          vent.trigger('group:show', this.model.id);
        }, this),
        error: _.bind(function(model, response){
          // Handle Error
        }, this)
      });
      return false;
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      return data;
    },

    templateHelpers: {

      templates: templates,

      breadcrumbs: function(){
        var $icon = $('<i>').addClass('icon-folder-close');
        var $edit = $('<input>')
            .attr('type', 'text')
            .attr('name', 'name')
            .attr('placeholder', this.name)
            .addClass('input-small')
            .css('margin-bottom', 0);
        var $active = $('<li>')
            .append($icon)
            .append(' ')
            .append($edit);

        var crumb = function(group){
          if(!group) return '';
          var $divider = $('<span>').addClass('divider').text('/');
          var $icon = $('<i>').addClass('icon-folder-open');
          var $group = $('<a>').attr('href', '#groups/'+group.id)
            .append($icon)
            .append(' ')
            .append(group.get('name'));
          var $e = $('<li>')
            .append($group)
            .append($divider);
          return crumb(group.get('parent')) + $e[0].outerHTML;
        };

        return $('<ul>')
          .append(crumb(this.parent))
          .append($active)
          .addClass('breadcrumb')[0].outerHTML;
      },

      formUrl: function(){
        if(this.parent_id) return "#/groups/"+this.parent_id+"/groups/create";
        else return "#/groups/create";
      }
    }
  });

});
