define(['marionette','templates','vent','views/OsItemView','collections/Oses'], 
function (Marionette,templates,vent,OsItemView,Oses) {
  "use strict";

  return Marionette.CompositeView.extend({

    template : templates.groupOsesEditView,
    itemView: OsItemView,
    itemViewContainer : 'tbody',

    ui : {},

    initialize: function(){
      this.bindTo(this.model, 'change:oses', this.render, this);
      this.itemViewOptions = { group: this.model };
      this.options.oses = new Oses();
      this.collection = this.options.oses;
      this.bindTo(this.options.oses, 'reset', this.render, this);
      this.options.oses.fetch();
    },

    onRender: function(){
        
    }

  });
});
