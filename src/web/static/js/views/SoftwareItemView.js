define(['marionette','templates','vent','models/Software'], 
function (Marionette,templates,vent,Software) {
  "use strict";

  return Marionette.ItemView.extend({

    model: Software,

    tagName: 'tr',

    template : templates.softwareItemView,
    
    ui : {
      checkbox: 'input[type="checkbox"]'
    },
    
    events : {
      'click input[type="checkbox"]'  : 'onSelect',
      'keypress'  : 'onEditKeypress'
    },

    initialize: function(){
    },

    destroy : function() {
      this.model.destroy();
    },

    onRender: function(){
      if(this.options.group.softwares.get(this.model.id))
        this.ui.checkbox.attr('checked','checked');
    },

    onSelect: function(e){
      e.stopPropagation();
      if(this.ui.checkbox.is(':checked'))
        this.options.group.softwares.add([this.model]);
      else 
        this.options.group.softwares.remove([this.model]);
    },

    onEditClick : function(e) {
    },

    onEditKeypress : function(evt) {
      var ENTER_KEY = 13;

      if ( evt.which === ENTER_KEY ) {
      }
    }
  });

});
