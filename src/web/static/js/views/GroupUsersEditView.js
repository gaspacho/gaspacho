define(['marionette','templates','vent','views/UserItemView','models/User','collections/Users','lib/bootstrap-typeahead'], 
function (Marionette,templates,vent,UserItemView,User,Users) {
  "use strict";

  return Marionette.CompositeView.extend({

    template : templates.groupUsersEditView,
    itemViewContainer : 'tbody',
    itemView: UserItemView,

    ui : {
      name: 'input[name=userName]',
      type: 'select[name=userType]',
      nameControls: '.userName.control-group',
      nameHelper: '.userName .help-inline'
    },
    
    events: {
      'click .add': 'addUser'
    },

    initialize: function(){
      this.collection = this.model.users;
      this.bindTo(this.model, 'change:users', this.render, this);

      this.options.users = new Users();
      this.bindTo(this.options.users, 'reset', this.typeahead, this);
      this.options.users.fetch();
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      return data
    },

    onRender: function(){
    },

    typeahead: function(){
      this.ui.name.typeahead({
        
        display: "name",

        val: "id",

        source: _.bind(this.options.users.toJSON, this.options.users),

        select: function () {
            var $selectedItem = this.$menu.find('.active');
            if($selectedItem.length >0)
                this.$element.val($selectedItem.text()).change();
            this.options.itemSelected($selectedItem, $selectedItem.attr('data-value'), $selectedItem.text());
            return this.hide();
        },

        itemSelected: _.bind(function($el,id,name){
          if(id){
            this.collection.add(this.options.users.get(id));
            this.ui.nameControls.removeClass('error');
            this.ui.nameHelper.empty();
            this.ui.name.val('');
          } else this.addUser();
        }, this),

        render: function (items) {
            var that = this;

            items = $(items).map(function (i, item) {
                var $icon = templates.helpers.user_icon(item);
                i = $(that.options.item).attr('data-value', item[that.options.val]);
                i.find('a').html($icon+that.highlighter(item[that.options.display], item));
                return i[0];
            });

            items.first().addClass('active');
            this.$menu.html(items);
            return this;
        }
      });
    },

    addUser: function(){
      var name = this.ui.name.val();
      var type = this.ui.type.val();
      if(name){
        new User().save({
          name: name,
          type: type
        },{
          success: _.bind(function(model, response, xhr){
            this.collection.add(model);
            this.options.users.add(model);
            this.ui.nameControls.removeClass('error');
            this.ui.nameHelper.empty();
            this.ui.name.val('');
          }, this),
          error: _.bind(function(model, response, xhr){
            this.ui.nameControls.addClass('error');
            console.log(model, response, xhr);
            this.ui.nameHelper.text(response.responseText);
          }, this)
        });
      } else {
        this.ui.nameControls.addClass('error');
        this.ui.nameHelper.text('You must specify a name');
      }
      return false;
    }
  });
});
