define(['marionette','vent','templates'], 
function (Marionette,vent,templates) {
  "use strict";

  return Marionette.ItemView.extend({

    tagName : 'tr',
    
    template : templates.ruleView,

    ui: {
      btnFree: '.free',
      btnOn: '.on',
      btnOff: '.off'
    },

    events: {
        'click': 'onClick',
        'click .free': 'selectFree',
        'click .on': 'selectOn',
        'click .off': 'selectOff'
    },

    initialize: function(){
      //this.bindTo(this.model, 'change', this.render, this);
      this.model.sync = _.bind(function(method, model, options){
        if(true || method == 'PUT') 
          options.url = '/groups/'+this.options.group.id+'/rules/'+this.model.id;
        return Backbone.sync(method, model, options);
      }, this);
    },

    onClick: function(){
    },

    selectFree: function(){
      this.$('.state').children().removeClass('active btn-danger btn-success');
      this.model.save('state', 'free');
      this.ui.btnFree.addClass('active');
    },

    selectOn: function(){
      this.$('.state').children().removeClass('active btn-danger btn-success');
      this.model.save('state', 'on');
      this.ui.btnOn.addClass('active btn-success');
    },

    selectOff: function(){
      this.$('.state').children().removeClass('active btn-danger btn-success');
      this.model.save('state', 'off');
      this.ui.btnOff.addClass('active btn-danger');
    },

    templateHelpers: {

      templates: templates,

      hstateClass: function(){
        return Object({'free': '', 'on': 'btn-success', 'off': 'btn-danger'})[this.hstate];
      }
    }
  });

});
