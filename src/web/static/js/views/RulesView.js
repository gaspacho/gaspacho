define(['marionette','vent','templates','collections/Rules','views/RuleView'], 
function (Marionette,vent,templates,Rules,RuleView) {
  "use strict";

  return Marionette.CompositeView.extend({

    tagName: 'div',

    className: 'accordion-inner',

    template: templates.rulesView,

    itemView: RuleView,
    
    itemViewContainer: 'tbody',

    ui: {},

    events: {},

    initialize: function(){
      this.itemViewOptions = {
        group : this.options.group
        //user  :  this.options.user
      };
      this.model = this.options.tag;
      this.collection = new Rules([], { 
        group_id: this.options.group.id,
        user_id: this.options.user ? this.options.user.id : null,
        category_id: this.options.category.id,
        tag_id: this.options.tag.id
      });
      this.bindTo(this.collection, 'reset', this.render, this);
      //this.collection.fetch();
      this.initChildViewStorage();
      this.collection.reset(this.model.rules.toJSON());
    },

    onRender: function(){ 

    }
  });
});
