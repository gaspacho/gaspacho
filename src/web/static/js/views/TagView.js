define(['marionette','vent','templates','views/RulesView'], 
function (Marionette,vent,templates,RulesView) {
  "use strict";

  return Marionette.Layout.extend({
    template : templates.tagView,

    regions: {
        rules: '.accordion-body'
    },

    initialize: function(){
      //this.bindTo(this.model, 'change', this.render, this);
    },

    onRender: function(){
        this.rules.show(new RulesView({ 
          group: this.options.group,
          user: this.options.user,
          category: this.options.category,
          tag: this.model
        }));
    }
  });

});