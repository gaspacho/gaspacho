define(['marionette','templates','vent','models/Os'], 
function (Marionette,templates,vent,Os) {
  "use strict";

  return Marionette.ItemView.extend({

    model: Os,

    tagName: 'tr',

    template : templates.osItemView,
    
    ui : {
      checkbox: 'input[type="checkbox"]'
    },
    
    events : {
      'click input[type="checkbox"]'  : 'onSelect',
      'keypress'  : 'onEditKeypress'
    },

    initialize: function(){
    },

    destroy : function() {
      this.model.destroy();
    },

    onRender: function(){
      if(this.options.group.oses.get(this.model.id))
        this.ui.checkbox.attr('checked','checked');
    },

    onSelect: function(e){
      e.stopPropagation();
      if(this.ui.checkbox.is(':checked'))
        this.options.group.oses.add([this.model]);
      else 
        this.options.group.oses.remove([this.model]);
    },

    onEditClick : function(e) {
    },

    onEditKeypress : function(evt) {
      var ENTER_KEY = 13;

      if ( evt.which === ENTER_KEY ) {
      }
    }
  });

});
