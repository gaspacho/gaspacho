define(['marionette','templates','vent','models/Group','collections/Groups','views/UserView','lib/bootstrap-contextmenu'], 
function (Marionette,templates,vent,Group,Groups,UserView) {
  "use strict";

  return Marionette.CompositeView.extend({

    template : templates.groupView,

    tagName: "li",

    ui : {
      view    : '.view',
      edit    : '.edit',
      context : '.context'
    },

    events : {
      'click .destroy'  : 'destroy',
      'click .edit'     : 'edit',
      'click .toggle'   : 'toggle',
      'click .addChild' : 'addChild',
      'dblclick .view'  : 'onEditClick',
      'keypress .edit'  : 'onEditKeypress',
      'click i.icon-edit': 'goEdit',
      'click i.icon-remove': 'goDelete'
    },

    modelEvents: {
      'change': 'render'
    },

    collectionEvents: {
      "add": "modelAdded"
    },

    initialize: function(){
      this.collection = this.model.children;
      this.bindTo(this.model, 'change', this.render, this);
      this.bindTo(this.collection, 'change', this.render, this);
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      return data
    },

    appendHtml: function(collectionView, itemView){
      collectionView.$("ul.children:first").append(itemView.el);
    },
    
    onRender : function() {
      this.model.users.each(function(user){
        this.appendHtml(this, new UserView({ model: user, group: this.model }).render());
      }, this);
      //if(this.model.get('toggled')) this.$("ul.children:first").slideDown();
      //else this.$("ul.children:first").slideUp();
    },

    edit: function(){
      vent.trigger('group:edit');
    },

    destroy : function() {
      this.model.destroy();
    },

    toggle : function(e) {
      e.stopPropagation();
      this.model.toggle();
    },

    goEdit: function(e){
      e.stopPropagation();
      Backbone.View.goTo('groups/'+this.model.id+'/edit');
      return false;
    },

    goDelete: function(e){
      e.stopPropagation();
      Backbone.View.goTo('groups/'+this.model.id+'/edit');
      return false;
    },
    
    onEditClick : function(e) {
      e.stopPropagation();
      this.$el.addClass('editing');
      this.ui.edit.focus();
    },

    onEditKeypress : function(evt) {
      var ENTER_KEY = 13;
      var groupName = this.ui.edit.val().trim();

      if ( evt.which === ENTER_KEY ) {
        if(groupName) this.model.set('name', groupName).save();
        this.$el.removeClass('editing');
      }
    },

    addChild: function(e){
      e.stopPropagation();
      Backbone.View.goTo('groups/'+this.model.id+'/groups/new');
      //vent.trigger('group:new', { parent_id: this.model.id });
      this.ui.context.removeClass('open');
    },

    templatesHelpers: {
      templates: templates,
      url: function(action){
	var route = "#groups/"+this.id;
        switch(action){
	  case 'edit': return route+"/edit";
          case 'delete': return route+"/delete";
          default: return route;
	}
      }
    }
  });
});
