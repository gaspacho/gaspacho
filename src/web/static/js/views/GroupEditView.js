define(['marionette','vent','templates',
  'views/GroupUsersEditView',
  'views/GroupManagersEditView',
  'views/GroupComputersEditView',
  'views/GroupOsesEditView',
  'views/GroupSoftwaresEditView'
],function(Marionette,vent,templates,
  GroupUsersEditView,
  GroupManagersEditView,
  GroupComputersEditView,
  GroupOsesEditView,
  GroupSoftwaresEditView
){

  return Marionette.Layout.extend({

    template : templates.groupEditView,

    regions : {
      users: '#tabUsers',
      managers: '#tabManagers',
      computers: '#tabComputers',
      oses: '#tabOses',
      softwares: '#tabSoftwares'
    },

    ui : {
      name: 'input[name=name]',
      comment: 'input[name=comment]'
    },

    events : {
      'click button[type=submit]': 'update'
    },

    initialize : function() {
        this.bindTo(this.model, 'change', this.render);
    },

    onRender : function() {
      this.users.show(new GroupUsersEditView({ model: this.model, collection: this.model.users }));
      this.managers.show(new GroupManagersEditView({ model: this.model, collection: this.model.managers }));
      this.computers.show(new GroupComputersEditView({ model: this.model, collection: this.model.computers }));
      this.oses.show(new GroupOsesEditView({ model: this.model, collection: this.model.oses }));
      this.softwares.show(new GroupSoftwaresEditView({ model: this.model, collection: this.model.softwares }));
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      return data;
    },

    update: function(){
      var name = this.ui.name.val() || this.model.get('name');
      var comment = this.ui.comment.val() || this.model.get('comment');
      this.model.save({
        'name': name,
        'comment': comment
      },{ 
        success: _.bind(function(model, response){
          if(this.model.get('parent')) this.model.get('parent').children.add(model);
          else this.collection.add(model);
          Backbone.View.goTo('#groups/'+this.model.id);
	  app.flash('Group Successfully modified','success');
        }, this),
        error: _.bind(function(model, response){
          // Handle Error
        }, this)
      });
      return false;
    },

    templateHelpers: {

      templates: require('templates'),

      breadcrumbs: function(){
        var $icon = $('<i>').addClass('icon-folder-close');
        var $edit = $('<input>')
            .attr('type', 'text')
            .attr('name', 'name')
            .attr('placeholder', this.name)
            .addClass('input-small')
            .css('margin-bottom', 0);
        var $active = $('<li>')
            .append($icon)
            .append(' ')
            .append($edit);

        var crumb = function(group){
          if(!group) return '';
          var $divider = $('<span>').addClass('divider').text('/');
          var $icon = $('<i>').addClass('icon-folder-open');
          var $group = $('<a>').attr('href', '#groups/'+group.id)
            .append($icon)
            .append(' ')
            .append(group.get('name'));
          var $e = $('<li>')
            .append($group)
            .append($divider);
          return crumb(group.get('parent')) + $e[0].outerHTML;
        };

        return $('<ul>')
          .append(crumb(this.parent))
          .append($active)
          .addClass('breadcrumb')[0].outerHTML;
      },

      formUrl: function(){
        return "#/groups/"+this.id+"/update";
      }
    }
  });

});
