define(['marionette','templates','vent','models/User'], 
function (Marionette,templates,vent,User) {
  "use strict";

  return Marionette.ItemView.extend({

    model: User,

    tagName: 'tr',

    template : templates.userItemView,
    
    ui : {
      view : '.view',
      edit : '.edit'
    },
    
    events : {
      'click .destroy'  : 'destroy',
      'dblclick .view'  : 'onEditClick',
      'keypress .edit'  : 'onEditKeypress',
      'click .view'     : 'onSelect'
    },

    initialize: function(){
      this.bindTo(this.model, 'change', this.render, this);
    },

    destroy : function() {
      this.model.destroy();
    },

    onSelect: function(e){
      e.stopPropagation();
      vent.trigger('user:select');
      this.$el.addClass('active');
    },

    onEditClick : function(e) {
      e.stopPropagation();
      this.$el.addClass('editing');
      this.ui.edit.focus();
    },

    onEditKeypress : function(evt) {
      var ENTER_KEY = 13;
      var groupName = this.ui.edit.val().trim();

      if ( evt.which === ENTER_KEY ) {
        if(groupName) this.model.set('name', groupName).save();
        this.$el.removeClass('editing');
      }
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      data.group = this.options.group;
      return data;
    },
    
    templateHelpers: {
      templates: templates
    }
  });
});
