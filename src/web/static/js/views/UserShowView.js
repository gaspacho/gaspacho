define(['marionette','vent','templates','collections/Categories','collections/Tags','views/CategoryView','views/TagsView'], 
function (Marionette,vent,templates,Categories,Tags,CategoryView,TagsView) {
  "use strict";

  return Marionette.CompositeView.extend({

    template : templates.groupShowView,

    itemView: CategoryView,

    tagName: 'div',

    className: 'row-fluid',

    ui : {
      tabs : '#categories',
      tags : '#category'
    },

    events : {},

    initialize: function(){
      this.itemViewOptions = {
        user: this.model,
        group: this.options.group
      };
      this.collection = new Categories([], { group_id: this.options.group.id, user_id: this.model.id });
      this.collection.fetch({
        success: _.bind(function(collection){ 
          vent.trigger('category:show', collection.first().id, this);
        }, this)
      });
      this.bindTo(vent,'category:show', this.categoryShow, this);
    },

    appendHtml: function(collectionView, itemView){
      collectionView.$("#categories").append(itemView.el);
    },
    
    onRender: function(){
      this.$('.category:first').addClass('active');
    },

    categoryShow: function(id){
      this.ui.tabs.children().removeClass('active');
      this.$('li > a[href$="'+window.location.hash+'"]').parent().addClass('active');
      var tags = new Tags({ category_id: id, group_id: this.model.id });
      this.ui.tags.html(new TagsView({ collection: tags, group: this.model, category: this.collection.get(id) }).el);
      tags.fetch();
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      return data;
    },

    templateHelpers: {
      templates: templates
    }
  });
});
