define(['marionette','vent','templates'], 
function (Marionette,vent,templates) {
  "use strict";

  return Marionette.ItemView.extend({

    tagName : 'li',

    className: 'category',
    
    template : templates.categoryView,

    initialize: function(){
      
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.id = this.model.id;
      data.model = data;
      data.group = this.options.group.toJSON();
      if(this.options.user) data.user = this.options.user.toJSON();
      return data;
    },

    templateHelpers: {

      templates: templates,

      url: function(){
        return /*'#category'+this.id; //*/'#groups/'+this.group.id+(this.user ? '/users/'+this.user.id : '')+'/categories/'+this.id;
      }
    }
  });
});
