define(['marionette','vent','templates','collections/Categories','views/CategoryView','views/TagsView'], 
function (Marionette,vent,templates,Categories,CategoryView,TagsView) {
  "use strict";

  return Marionette.CompositeView.extend({

    template : templates.groupShowView,

    itemView: CategoryView,

    emptyView: Marionette.View.extend({
       template: templates.loadingView
    }),

    tagName: 'div',

    className: 'row-fluid',

    ui : {
      tabs : '#categories_tabs',
      contents : '#categories_contents'
    },

    events : {},

    initialize: function(){
      this.model = this.options.group
      this.collection = new Categories([], { group_id: this.model.id });
      this.itemViewOptions = {
        group: this.model
      };
      //this.bindTo(this.collection, 'reset', this.render, this);
      this.collection.fetch({
        success: _.bind(function(collection){ 
          Backbone.View.goTo('groups/'+this.model.id+'/categories/'+collection.first().id);
        }, this)
      });
      this.bindTo(vent,'groupCategory:show', this.categoryShow, this);
    },

    appendHtml: function(collectionView, itemView){
      collectionView.$("#categories_tabs").append(itemView.el);
      /*collectionView.$("#categories_contents").append(new TagsView({
        group: this.model,
        user: this.options.user,
        category: itemView.model
      }).el);*/
    },
    
    onRender: function(){
      console.log('render');//this.tabs.show(new CategoriesView());
    },

    categoryShow: function(group_id, category_id){
      this.ui.tabs.children().removeClass('active');
      this.$('li > a[href$="'+window.location.hash+'"]').parent().addClass('active');
      this.ui.contents.html(new TagsView({ 
        group: this.model, 
        user: this.options.user,
        category: this.collection.get(category_id) 
      }).el);
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      return data;
    },

    templateHelpers: {
      templates: templates
    }
  });
});
