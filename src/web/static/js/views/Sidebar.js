define(['marionette','vent','templates','views/GroupsView'], 
function (Marionette,vent,templates,GroupsView) {
  "use strict";

  return Marionette.Layout.extend({

    template : templates.sidebar,

    tagName: 'div',

    className: 'well sidebar-nav',
    
    regions : {
      groupTree : '#groups-tree',
      templatesList : '#templates-list'
    },

    ui : {},

    events : {},

    initialize: function(){
      this.bindTo(vent, 'group:show groupUser:show group:edit groupUser:edit', this.updateGroupSelect, this);
    },

    onRender : function() {
      this.groupTree.show(new GroupsView({ collection: this.options.groups }));
    },

    updateGroupSelect: function(group_id, user_id){
      var url = "#groups/"+group_id+(user_id ? "/users/"+user_id : "");
      this.$('li').removeClass('active');
      this.$('li > a[href~="'+url+'"]').parent().addClass('active');
    }
  });
});
