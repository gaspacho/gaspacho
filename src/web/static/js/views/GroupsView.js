define(['marionette','vent','views/GroupView'], 
function (Marionette,vent,GroupView) {
  "use strict";

  return Marionette.CollectionView.extend({

    tagName: 'ul',

    className: 'nav nav-list',

    itemView: GroupView
    
  });
});