define(['marionette','vent','templates',
  'views/Sidebar',
  'views/GroupNewView',
  'views/GroupShowView',
  'views/GroupEditView',
  'views/UserShowView',
  'models/Group',
  'collections/Categories'
],function (Marionette,vent,templates,
  Sidebar,
  GroupNewView,
  GroupShowView,
  GroupEditView,
  UserShowView,
  Group,
  Categories
){
  "use strict";

  return Marionette.Layout.extend({

    template : templates.body,

    tagName: 'div',

    className: 'row-fluid',

    regions : {
      sidebar : '#sidebar',
      main    : '#main',
      breadcrumbs: '#breadcrumbs'
    },

    ui : {},

    events : {'click .new': 'groupNew'},

    initialize : function() {
      this.bindTo(vent, 'group:new', this.groupNew, this);
      this.bindTo(vent, 'group:show', this.groupShow, this);
      this.bindTo(vent, 'group:edit', this.groupEdit, this);
      this.bindTo(vent, 'groupUser:show', this.groupUserShow, this);
    },

    onRender : function() {
      this.sidebar.show(new Sidebar({ groups : this.options.groups }));
    },

    groupNew: function(params){
      if(params.parent_id)
        $.extend(params, { parent: this.options.groups.get(params.parent_id)});
      this.main.show(new GroupNewView({ 
        model: new Group(params),
        collection: this.options.groups 
      }));
    },

    groupShow: function(id) {
      this.main.show(new GroupShowView({
        group: this.options.groups.get(id) 
        //model: this.options.groups.get(id)
        //collection: this.options.groups 
      }));
    },

    groupEdit: function(id) {
      this.main.show(new GroupEditView({ 
        model: this.options.groups.get(id),
        collection: this.options.groups 
      }));
    },

    groupUserShow: function(group_id, user_id){
      var group = this.options.groups.get(group_id);
      this.main.show(new UserShowView({ 
        model: group.users.get(user_id),
        group: group,
        collection: this.options.groups 
      }));
    }
  });

});
