define(['marionette','templates','vent','models/Computer'], 
function (Marionette,templates,vent,Computer) {
  "use strict";

  return Marionette.ItemView.extend({

    model: Computer,

    tagName: 'tr',

    template : templates.computerItemView,
    
    ui : {
      view     : '.view',
      nameView : '.name.view',
      typeView : '.type.view',
      edit     : '.edit',
      nameEdit : '.name.edit',
      typeEdit : '.type.edit'
    },
    
    events : {
      'click .destroy'       : 'destroy',
      'dblclick .view.name'  : 'onNameEditClick',
      'dblclick .view.type'  : 'onTypeEditClick',
      'keypress .edit'       : 'onEditKeypress',
      'click .view'          : 'onSelect'
    },

    initialize: function(){
      this.bindTo(this.model, 'change', this.render, this);
    },

    destroy : function() {
      this.model.destroy();
    },

    onSelect: function(e){
      e.stopPropagation();
      //vent.trigger('user:select');
      this.$el.addClass('active');
    },

    onNameEditClick : function(e) {
      e.stopPropagation();
      this.$el.addClass('editing');
      this.ui.nameEdit.focus();
    },

    onTypeEditClick : function(e) {
      e.stopPropagation();
      this.$el.addClass('editing');
      this.ui.typeEdit.focus();
    },

    onEditKeypress : function(evt) {
      var ENTER_KEY = 13;
      var computerName = this.ui.nameEdit.val().trim();
      var computerType = this.ui.typeEdit.val().trim();

      if ( evt.which === ENTER_KEY ) {
        if(computerName) this.model.set('name', computerName);
        if(computerType) this.model.set('type', computerType);
        this.$el.removeClass('editing');
      }
    }
  });

});
