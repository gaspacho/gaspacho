define(['marionette','vent','templates','collections/Tags','views/TagView'], 
function (Marionette,vent,templates,Tags,TagView) {
  "use strict";

  return Marionette.CompositeView.extend({

    tagName: 'div',

    //id: function(){ return 'category'+this.model.id; },

    className: 'tab-pane accordion',

    itemView: TagView,

    template: templates.tagsView,

    itemViewContainer: '#tags',

    emptyView: Marionette.ItemView.extend({
      template: templates.loadingView
    }),

    ui: {},

    events: {},

    initialize: function(){
      this.itemViewOptions = {
        group: this.options.group,
        user: this.options.user,
        category: this.options.category,
      };
      this.model = this.options.category;
      this.collection = new Tags([], { 
        group_id: this.options.group.id,
        user_id: this.options.user ? this.options.user.id : null,
        category_id: this.options.category.id
      });
      this.bindTo(this.collection, 'reset', this.render, this);
      this.collection.fetch();
      this.initChildViewStorage();
      this.collection.reset(this.model.tags.toJSON());
    },

    onRender: function(){
      this.$el.attr('id', 'category'+this.model.id); 
      this.$(".accordion-body:first").addClass('in');
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      data.group = this.options.group.toJSON();
      data.category = this.options.category.toJSON();
      return data;
    },

    templateHelpers: {

      templates: templates,

      breadcrumbs: function(){
        var $icon = $('<i>').addClass('icon-tags');
        var $active = $('<li>')
          .append($icon)
          .append(' ')
          .append(this.category.name)
          .addClass('active');

        var $divider = $('<span>').addClass('divider').text('/');
            $icon = $('<i>').addClass('icon-folder-open');
        var $group = $('<li>')
          .append($icon)
          .append(' ')
          .append(this.group.name)
          .append($divider);

        var crumb = function(group){
          if(!group) return '';
          var $divider = $('<span>').addClass('divider').text('/');
          var $icon = $('<i>').addClass('icon-folder-open');
          var $group = $('<a>').attr('href', '#groups/'+group.id)
            .append($icon)
            .append(' ')
            .append(group.get('name'));
          var $e = $('<li>')
            .append($group)
            .append($divider);
          return crumb(group.get('parent')) + $e[0].outerHTML;
        };

        return $('<ul>')
          .append(crumb(this.group.parent))
          .append($group)
          .append($active)
          .addClass('breadcrumb')[0].outerHTML;
      }
    }
  });
});
