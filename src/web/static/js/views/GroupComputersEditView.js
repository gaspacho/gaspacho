define(['marionette','templates','vent','views/ComputerItemView'], 
function (Marionette,templates,vent,ComputerItemView) {
  "use strict";

  return Marionette.CompositeView.extend({

    template : templates.groupComputersEditView,

    itemView: ComputerItemView,

    events: {
      'click .add': 'addComputer'
    },

    ui : {
      name: 'input[name=computerName]',
      type: 'select[name=computerType]',
      nameControls: '.computerName.control-group',
      nameHelper: '.computerName .help-inline'
    },

    initialize: function(){
      this.collection = this.model.computers;
      this.bindTo(this.model, 'change:computers', this.render, this);
    },

    serializeData: function(){
      var data = Marionette.CompositeView.prototype.serializeData.call(this);
      data.model = data;
      return data
    },

    appendHtml: function(collectionView, itemView){
      collectionView.$("tbody").append(itemView.el);
    },
    
    onRender: function(){
        
    },

    addComputer: function(){
      var name = this.ui.name.val();
      var type = this.ui.type.val();
      if(name){
        this.model.computers.add({ 
          name: name,
          type: type
        });
        this.ui.name.val('');
      } else {
        this.ui.nameControls.addClass('error');
        this.ui.nameHelper.text('You must specify a name');
      }
      return false;
    }

  });

});