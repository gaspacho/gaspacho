define(['marionette','templates','vent','views/SoftwareItemView','collections/Softwares'], 
function (Marionette,templates,vent,SoftwareItemView,Softwares) {
  "use strict";

  return Marionette.CompositeView.extend({

    template : templates.groupSoftwaresEditView,
    itemView: SoftwareItemView,
    itemViewContainer : 'tbody',

    ui : {},

    initialize: function(){
      this.bindTo(this.model, 'change:softwares', this.render, this);
      this.itemViewOptions = { group: this.model };
      this.options.softwares = new Softwares();
      this.collection = this.options.softwares;
      this.bindTo(this.options.softwares, 'reset', this.render, this);
      this.options.softwares.fetch();
    },

    onRender: function(){
        
    }

  });
});
