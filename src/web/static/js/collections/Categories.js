define(['models/Category','lib/backbone-localStorage'],
function(Category) {
  'use strict';

  return Backbone.Collection.extend({

    model: Category,

    initialize: function(models, options){
        this.options = options;
    },

    url: function(models){
      if(models)
        return '/categories/set/'+_.pluck( models, 'id' ).join(';');
      else
        return '/groups/'+this.options.group_id+
               (this.options.user_id ? '/users/'+this.options.user_id : '')+
               '/categories';
    }

  });

});
