define(['backbone','models/Computer','lib/backbone-localStorage'],function(Backbone,Computer) {
  'use strict';

  return Backbone.Collection.extend({

    model: Computer,

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    url: function(models){
      return '/computers' + (models ? '/set/' + _.pluck( models, 'id' ).join(';') : '');
    }
  });

});