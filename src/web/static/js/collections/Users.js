define(['backbone','models/User','lib/backbone-localStorage'],function(Backbone,User) {
  'use strict';

  return Backbone.Collection.extend({

    model: User,

    url: function(models){
      return '/users' + (models ? '/set/' + _.pluck( models, 'id' ).join(';') : '');
    }

  });

});