define(['backbone','models/Rule','lib/backbone-localStorage'],function(Backbone,Rule) {
  'use strict';

  return Backbone.Collection.extend({

    model: Rule,

    initialize: function(models, options){
        this.options = options;
    },

    url: function(models){
      if(models)
        return '/rules/set/'+_.pluck( models, 'id' ).join(';');
      else
        return '/groups/'+this.options.group_id+
               (this.options.user_id ? '/users/'+this.options.user_id : '')+
               '/categories/'+this.options.category_id+
               '/tags/'+this.options.tag_id+
               '/rules';
    }
  });

});