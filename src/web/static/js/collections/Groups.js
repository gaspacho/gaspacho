define(['backbone','models/Group','lib/backbone-localStorage'],function(Backbone,Group) {
  'use strict';

  function isRoot(group) { return group.get('parent_id') == null; }

  return Backbone.Collection.extend({

    model: Group,

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    url: function(models){
      return '/groups' + (models ? '/set/' + _.pluck( models, 'id' ).join(';') : '');
    },
    
    get: function(id){
        var model = Backbone.Collection.prototype.get.call(this, id);
        if(model) return model;
        else {
          var child = _.find(this.models, function(group){ 
            return group.children.get(id); 
          }, this);
          return child ? child.children.get(id) : false;
        }
    },
    
    getRoots: function() {
      return this.filter(isRoot);
    },
    getOthers: function() {
      return this.reject(isRoot);
    },
    comparator: function(group) {
      return group.get('created');
    }

  });

});