define(['models/Software','lib/backbone-localStorage'],
function(Software) {
  'use strict';

  return Backbone.Collection.extend({

    model: Software,

    initialize: function(models, options){
        this.options = options;
    },

    url: function(models){
      if(models)
        return '/softwares/set/'+_.pluck( models, 'id' ).join(';');
      else
	return '/softwares';
        return '/groups/'+this.options.group_id+
               (this.options.user_id ? '/users/'+this.options.user_id : '')+
               '/softwares';
    }

  });

});
