define(['backbone','models/Tag','lib/backbone-localStorage'],function(Backbone,Tag) {
  'use strict';

  return Backbone.Collection.extend({

    model: Tag,

    initialize: function(models, options){
        this.options = options;
    },

    url: function(models){
      if(models)
        return '/tags/set/'+_.pluck( models, 'id' ).join(';');
      else
        return '/groups/'+this.options.group_id+
               (this.options.user_id ? '/users/'+this.options.user_id : '')+
               '/categories/'+this.options.category_id+
               '/tags';
    }

    /*
    setCategoryId: function(id){
        this.options.category_id = id;
    }
    */
  });

});