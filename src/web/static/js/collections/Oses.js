define(['models/Os','lib/backbone-localStorage'],
function(Os) {
  'use strict';

  return Backbone.Collection.extend({

    model: Os,

    initialize: function(models, options){
        this.options = options;
    },

    url: function(models){
      if(models)
        return '/oses/set/'+_.pluck( models, 'id' ).join(';');
      else
	return '/oses';
        return '/groups/'+this.options.group_id+
               (this.options.user_id ? '/users/'+this.options.user_id : '')+
               '/oses';
    }

  });

});
