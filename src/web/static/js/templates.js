define(function(require){
  "use strict";

  return {
    header                 : require('tpl!templates/header.tmpl'),
    body                   : require('tpl!templates/body.tmpl'),
    sidebar                : require('tpl!templates/sidebar.tmpl'),
    footer                 : require('tpl!templates/footer.tmpl'),
    groupView              : require('tpl!templates/groupView.tmpl'),
    groupNewView           : require('tpl!templates/groupNewView.tmpl'),
    groupShowView          : require('tpl!templates/groupShowView.tmpl'),
    groupEditView          : require('tpl!templates/groupEditView.tmpl'),
    userView               : require('tpl!templates/userView.tmpl'),
    categoryView           : require('tpl!templates/categoryView.tmpl'),
    configurationView      : require('tpl!templates/configurationView.tmpl'),
    _groupForm             : require('tpl!templates/_groupForm.tmpl'),

    osItemView             : require('tpl!templates/osItemView.tmpl'),
    softwareItemView       : require('tpl!templates/softwareItemView.tmpl'),
    userItemView           : require('tpl!templates/userItemView.tmpl'),
    computerItemView       : require('tpl!templates/computerItemView.tmpl'),
    groupOsesEditView      : require('tpl!templates/groupOsesEditView.tmpl'),
    groupSoftwaresEditView : require('tpl!templates/groupSoftwaresEditView.tmpl'),
    groupUsersEditView     : require('tpl!templates/groupUsersEditView.tmpl'),
    groupComputersEditView : require('tpl!templates/groupComputersEditView.tmpl'),
    tagView                : require('tpl!templates/tagView.tmpl'),
    tagsView               : require('tpl!templates/tagsView.tmpl'),
    rulesView              : require('tpl!templates/rulesView.tmpl'),
    ruleView               : require('tpl!templates/ruleView.tmpl'),
    loadingView            : require('tpl!templates/loadingView.tmpl'),


    helpers : {

        user_icon: function(user){
            return $('<i>')
              .addClass('icon-'+Object({'user': 'user', 'usergroup': 'group'})[user.type])
              .wrap('<div></div>').parent().append(' ').html();
        },

        computer_icon: function(computer){
            return $('<i>')
              .addClass('icon-'+Object({'dns': 'globe', 'ip': 'sitemap'})[computer.type])
              .wrap('<div></div>').parent().append(' ').html();
        }
/*
        breadcrumbs: function(item, category){
            if(item instanceof User)
            if(item instanceof Group)
            if(category instanceof Category)
        }*/
    }
  };
});

