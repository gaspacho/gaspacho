define(['backbone','collections/Rules','lib/backbone-localStorage'],
function(Backbone, Rules){
  'use strict';

  return Backbone.Model.extend({

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    urlRoot: '/tags',

    defaults : {
        name : 'Tag Name'
    },
    
    initialize: function(){        
        this.rules = new Rules();
        this.on('change:rules',function() { this.rules.reset(this.get('rules')); }, this);
        this.rules.on('reset',this.adoptAll, this);
        this.rules.on('add',this.adoptOne, this);
        this.rules.reset(this.get('rules'));
    },

    adoptAll:function() {
        this.rules.each(this.adoptOne, this);
    },

    adoptOne:function(model) {
        model.set({
          'tag'        : this,
          'tagid'      : this.id,
          'category'   : this.get('category'),
          'categoryid' : this.get('categoryid')
        });
    },

    toJSON: function(){
        if(this._isSerializing) return this.id || this.cid;
        this._isSerializing = true;
        var json = _.extend(_.clone(this.attributes), {
            rules: this.rules.toJSON()
        });
        this._isSerializing = false;
        return json;
    }

  });

});

