define(['backbone', 'collections/Groups','collections/Users','collections/Computers','collections/Softwares','lib/backbone-localStorage'],
function(Backbone,Groups,Users,Computers,Softwares){
  'use strict';

  return Backbone.Model.extend({

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    urlRoot: '/groups',

    idAttribute: 'id',

    defaults: {
        name: 'Group name',
        comment: 'Comment on this group',   
        cls: 'root',
        toggled: false,
        leaf: false,
        children: [],
        users: [],
        managers: [],
        oses: [],
        softwares: [],
        computers: [],
        parent: null
    },

    initialize: function(){
        if(!Groups){ Groups = require('collections/Groups'); }
        if (this.isNew()) this.set('created', Date.now());

        this.children 	= new Groups();
        this.users 	= new Users();
        this.managers 	= new Users();
        this.computers 	= new Computers();
        this.oses 	= new Backbone.Collection();
        this.softwares 	= new Softwares();

        this.on('change:children', 	function() { this.children.reset(this.get('children')); }, this);
        this.on('change:users', 	function() { this.users.reset(this.get('users')); }, this);
        this.on('change:managers', 	function() { this.managers.reset(this.get('managers')); }, this);
        this.on('change:computers', 	function() { this.computers.reset(this.get('computers')); }, this);
        this.on('change:oses', 		function() { this.oses.reset(this.get('oses')); }, this);
        this.on('change:softwares', 	function() { this.softwares.reset(this.get('softwares')); }, this);

        this.children.on('reset',this.adoptAll, this);
        this.children.on('add',this.adoptOne, this);

        this.children.reset(this.get('children'));
        this.users.reset(this.get('users'));
        this.managers.reset(this.get('managers'));
        this.computers.reset(this.get('computers'));
        this.oses.reset(this.get('oses'));
        this.softwares.reset(this.get('softwares'));

        /*
        this.set('children', new Groups(this.get('children')));
        //this.get('children').each(function(group){ group.set('parent', this) }, this);
        this.set('users', new Users(this.get('users')));
        this.set('managers', new Backbone.Collection(this.get('managers')));
        this.set('computers', new Computers(this.get('computers')));
        this.set('oses', new Backbone.Collection(this.get('oses')));
        this.set('softwares', new Backbone.Collection(this.get('softwares')));
        */
    },

    set: function(attributes, options) {
        if(!Groups){ Groups = require('collections/Groups'); }
/*
        if(_.has(attributes, 'children') && this.get("children"))
            this.get('children').reset(attributes.children);
        else if(_.has(attributes, 'children') && !this.get('children'))
            this.set('children', new Groups(attributes.children));

        if(_.has(attributes, 'users') && this.get("users"))
            this.get('users').reset(attributes.users);
        else if(_.has(attributes, 'users') && !this.get('users'))
            this.set('users', new Users(attributes.users));

        if(_.has(attributes, 'managers') && this.get("managers"))
            this.get('managers').reset(attributes.managers);
        else if(_.has(attributes, 'managers') && !this.get('managers'))
            this.set('managers', new Users(attributes.managers));

        if(_.has(attributes, 'computers') && this.get("computers"))
            this.get('computers').reset(attributes.computers);
        else if(_.has(attributes, 'computers') && !this.get('computers'))
            this.set('computers', new Computers(attributes.computers));

        if(_.has(attributes, 'oses') && this.get("oses"))
            this.get('oses').reset(attributes.oses);
        else if(_.has(attributes, 'oses') && !this.get('oses'))
            this.set('oses', new Backbone.Collection(attributes.oses));

        if(_.has(attributes, 'softwares') && this.get("softwares"))
            this.get('softwares').reset(attributes.softwares);
        else if(_.has(attributes, 'softwares') && !this.get('softwares'))
            this.set('softwares', new Backbone.Collection(attributes.softwares));

        delete attributes.children;
        delete attributes.users;
        delete attributes.managers;
        delete attributes.computers;
        delete attributes.oses;
        delete attributes.softwares;
*/
        return Backbone.Model.prototype.set.call(this, attributes, options);
    },

    adoptAll:function() {
        this.children.each(this.adoptOne, this);
    },

    adoptOne:function(model) {
        model.set('parent', this);
    },

    toJSON: function(){
        if(this._isSerializing) return this.id || this.cid;
        this._isSerializing = true;
        var json = _.extend(_.clone(this.attributes), {
            //children: this.children.toJSON(),         // TOFIX ! Recursive groups !
            users: this.users.toJSON(),
            managers: this.managers.toJSON(),
            computers: this.computers.toJSON(),
            oses: this.oses.toJSON(),
            softwares: this.softwares.toJSON()
        });
        this._isSerializing = false;
        return json;
    },

    /*
    parse: function(response) {
        if(!Groups){ Groups = require('collections/Groups'); }
        if(_.has(response, 'children'))
            if(!_.has(this,'children')) this.children = new Groups(response.children);
            else this.children.reset(response.children);
        if(_.has(response, 'users'))
            if(!_.has(this,'users')) this.users = new Users(response.users);
            else this.users.reset(response.users);

        this.children.url = '/groups/' + response.id + '/groups'; 
        this.users.url = '/groups/' + response.id + '/groups'; 
        delete response.children;
        delete response.users;
        return response;
    },*/
    /*
    parse: function(data){
        if(!Groups){ Groups = require('collections/Groups'); }

        this.get('children').reset(data.children);
        this.get('users').reset(data.users);
        this.get('managers').reset(data.managers);
        this.get('computers').reset(data.computers);
        this.get('oses').reset(data.oses);
        this.get('softwares').reset(data.softwares);
        return data;
    },*/
    toggle: function() {
      return this.set('toggled', !this.get('toggled'));
    }
  });

});

