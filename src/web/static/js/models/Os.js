define(['backbone','lib/backbone-localStorage'],
function(Backbone){
  'use strict';

  return Backbone.Model.extend({

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    urlRoot: '/oses',

    defaults : {
        name : 'Platform'
    },
    
    initialize: function(){

    }

  });

});

