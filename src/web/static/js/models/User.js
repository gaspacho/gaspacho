define(['backbone','lib/backbone-localStorage'],
function(Backbone){
  'use strict';

  return Backbone.Model.extend({

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    urlRoot: '/users',

    defaults : {
        type : 'user',
        activate : false,
        name : 'username'
    },
    
    initialize: function(){

    }

  });

});

