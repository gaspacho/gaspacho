define(['backbone','lib/backbone-localStorage'],
function(Backbone){
  'use strict';

  return Backbone.Model.extend({

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    urlRoot: '/rules',

    defaults : {
        name : 'Rule Name'
    },
    
    initialize: function(){
    },

    // parse: function(response){
    //   response.name = response.name.fr;
    //   return response;
    // }

  });

});

