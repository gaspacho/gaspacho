define(['backbone','collections/Tags','lib/backbone-localStorage'],
function(Backbone,Tags){
  'use strict';

  return Backbone.Model.extend({

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    //idAttribute: 'name',

    urlRoot: '/categories',

    defaults : {
        name : 'Category Name'
    },
    
    initialize: function(){        
        this.tags = new Tags();
        this.on('change:tags',function() { this.tags.reset(this.get('tags')); }, this);
        this.tags.on('reset',this.adoptAll, this);
        this.tags.on('add',this.adoptOne, this);
        this.tags.reset(this.get('tags'));
    },

    adoptAll:function() {
        this.tags.each(this.adoptOne, this);
    },

    adoptOne:function(model) {
        model.set({
          'category'   : this,
          'categoryid' : this.id
        });
    },

    toJSON: function(){
        if(this._isSerializing) return this.id || this.cid;
        this._isSerializing = true;
        var json = _.extend(_.clone(this.attributes), {
            tags: this.tags.toJSON()
        });
        this._isSerializing = false;
        return json;
    }

  });

});

