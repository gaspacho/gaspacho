define(['backbone','lib/backbone-localStorage'],
function(Backbone){
  'use strict';

  return Backbone.Model.extend({

    //localStorage: new Backbone.LocalStorage('gaspacho-groups'),

    urlRoot: '/computers',

    defaults : {
        name : 'Domain Name',
        type : 'dns'
    },
    
    initialize: function(){

    }

  });

});

