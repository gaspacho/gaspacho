define(['marionette','templates','vent','collections/Groups','views/Header','views/Body','views/Footer'],
function (Marionette,templates,vent, Groups, Header, Body, Footer){
  "use strict";

  _.extend(Marionette.View.prototype, {
      serializeData: function(){
        var data;
        if (this.model) 
          data = this.model.toJSON(); 
        else if (this.collection)
          data = { items: this.collection.toJSON() };
        data = this.mixinTemplateHelpers(data);
        data.model = data;
        data.templates = templates;
        return data;
      }
  });

  window.app = new Marionette.Application();

  app.groups  = new Groups();

  app.addRegions({
    header : '#header',
    body   : '#body',
      sidebar : '#sidebar',
      main   : '#main',
    footer : '#footer'
  });

  app.addInitializer(function(){
    var options = { 
      groups: app.groups 
    };   
    app.header.show(new Header(options));
    app.body.show(new Body(options));
    app.footer.show(new Footer(options));

    app.groups.fetch();

  });

  app.flash = function(msg, type){
    $('#main').prepend(
      $('<div class="alert alert-'+type+'">')
      .append($('<button type="button" class="close" data-dismiss="alert">&times;</button>'))
      .append($('<i class="icon-'+(
         type == 'success' ? 'ok' :
         type == 'error' ? 'remove' : type	 
       )+'">'))
      .append(" ")
      .append(msg)
    );
  }

  return app;

});


