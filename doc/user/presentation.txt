Présentation
============

La génèse de Gaspacho
---------------------

L'idée de créer :term:`Gaspacho` est venu dans dans le cadre professionnel.

Le projet libre `EOLE <http://eole.orion.education.fr/>`_ utilise le logiciel
libre `Esu <http://dev-eole.ac-dijon.fr/projects/esu>`_.
Mais ce projet ne convient pas aux environnements hétérogènes et ne permet pas
l'intégration des postes GNU/Linux.

Le groupe d'utilisateurs de logiciel libre en Côte d'Or, `COAGUL
<http://www.coagul.org>`_, a décidé de se réunir pour partager les compétences
des différents membres pour démarrer le projet.

Même si le projet est suivi par certains membres de `COAGUL
<http://www.coagul.org>`_, il est maintenant développé de façon autonome.

Le produit
----------

:term:`Gaspacho` est un projet libre (GNU GPL v3) destiné à configurer les postes 
de travail d'une entité.

Le but du projet est de configurer indifféremment un ensemble de logiciels 
et de systèmes avec des choix déterminés par l'administrateur.
Mais ce n'est pas un logiciel de sécurisation du poste.

Un exemple simple de configuration : le proxy.

Si l'on veut configurer le proxy sur un ensemble de logiciels et de systèmes,
cela peut devenir assez rapidement rébarbatif.
Il faut le faire souvent dans plusieurs applications.

Avec :term:`Gaspacho`, il est possible de définir une seule fois la
configuration du proxy pour que l'ensemble des logiciels compatibles soient 
correctement paramétrés.

Pour cela, les configurations des applications sont divisées en différentes
règles regroupées par catégories et étiquettes. Une règle peut être appliquée à
différents logiciels (Mozilla Firefox et Konqueror) et à différents types 
de système (GNU/Linux et Microsoft Windows).

Les choix de l'administrateur sont regroupés dans des groupes de machine 
auxquels on associe des utilisateurs ou des groupes d'utilisateurs. Ainsi,
le bureau de l'utilisateur sera systématiquement construit en fonction du lieu
de connexion, du système et des besoins exprimés.

Enfin, l'administration pourra être délégué à un ou plusieurs managers 
pour un groupe déterminé.

Pour facilité la configuration de l'ensemble du parc, un système d'héritage de
choix et de template de choix permet de centraliser des configurations des
différents groupes.

:term:`Gaspacho` est composé de 2 parties logicielles :

* un serveur de configuration ;
* un agent sur les postes clients.

Les projets proches
-------------------

.. related projects

Des projets proches, sur le principe, existent. S'ils sont utiles dans leur 
domaine de compétence, ils ne peuvent pas remplacer :term:`Gaspacho`.

* privateur : GPO de Microsoft. Ce projet ne s'intéresse qu'à la base de
  registre et ne permet pas de configurer plusieurs logiciels avec une seule
  règle ;

* libres :

  * Esu : correspond aux besoins des règles mais pas multi-plateforme,

  * Puppet, CfEngine, ... : pas de notion d'utilisateur, centré sur la notion de 
    fichier (donc pas de possibilité de personnalisation de l'utilisateur) et
    l'administrateur doit connaître la structure des fichiers de configuration.

Les choix technologique
-----------------------

* `Python <http://www.python.org/>`_ : language de scripting avancé, robuste et reconnu ;
* `Twisted <http://twistedmatrix.com/>`_ : boîte à outils réseau utilisée pour le serveur web ;
* `Elixir <http://elixir.ematia.de/>`_ : boîte à outils pour la gestion de la base de données ;
* `Ext-JS <http://www.sencha.com/products/js/>`_ : bibliothèque javascript permettant de créer des applications web avec des widgets graphiques.

